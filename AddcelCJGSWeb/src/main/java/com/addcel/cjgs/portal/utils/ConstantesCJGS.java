package com.addcel.cjgs.portal.utils;

import java.util.HashMap;


public class ConstantesCJGS {
	
	public static final String PARAMETER_NAME_JSON = "json=";
	
	//variables PROD o QA
//	public static final String URL_AUTH_PROSA = "http://localhost:8080/ProsaWeb/ProsaAuth?";
//	public static final String URL_MAIL_SENDER = "http://localhost:8080/MailSenderAddcel/enviaCorreoAddcel";
	public static final String URL_DEVO_PROSA = "https://localhost:8443/ProsaWeb/ProsaDevolution?";
	
	//variables locales
//	public static String URL_AUTH_PROSA = "http://50.57.192.214:8080/ProsaWeb/ProsaAuth";
	public static String URL_MAIL_SENDER = "http://50.57.192.214:8080/MailSenderAddcel/enviaCorreoAddcel";
	
	public static final String URL_FOLDER_ARCHIVOS = "/usr/java/resources/files/AddcelEuropaWeb/";
	
	public static final String ERROR_USUARIO_STATUS_INACTIVO = "User Inactive or Terminated. Consult with the Administrator.";
	
	public static final String NOM_CAMPO_ID_ERROR = "idError";
	public static final String NOM_CAMPO_MENJ_ERROR = "mensajeError";
	
	public static final int ID_NO_SESSION = 1000;
	public static final String ERROR_NO_SESSION = "An active session does not exist.";
	
	
	public static HashMap<Integer, String> CODE_ERROR_BANK = null;
	
	static{
		CODE_ERROR_BANK = new HashMap<Integer, String>();
		CODE_ERROR_BANK.put(0, "Approved Card (if balances are not present)");
		CODE_ERROR_BANK.put(1, "Contact the bank");
		CODE_ERROR_BANK.put(2, "Refer to special conditions for card issuer");
		CODE_ERROR_BANK.put(3, "Invalid business");
		CODE_ERROR_BANK.put(4, "Pick-up card");
		CODE_ERROR_BANK.put(5, "Credit not available");
		CODE_ERROR_BANK.put(6, "Error");
		CODE_ERROR_BANK.put(7, "Pick-up card, special condition");
		CODE_ERROR_BANK.put(8, "Honor with identification");
		CODE_ERROR_BANK.put(9, "Transaction in Progress");
		CODE_ERROR_BANK.put(11, "Approved Card (VIP)");
		CODE_ERROR_BANK.put(12, "Transaction invalidate");
		CODE_ERROR_BANK.put(13, "Amount invalidate");
		CODE_ERROR_BANK.put(14, "Invalid Card Number");
		CODE_ERROR_BANK.put(15, "No such issuer");
		CODE_ERROR_BANK.put(30, "Format Error");
		CODE_ERROR_BANK.put(31, "Bank or Card Not Supported");
		CODE_ERROR_BANK.put(33, "Card Expired");
		CODE_ERROR_BANK.put(34, "Card locked");
		CODE_ERROR_BANK.put(35, "Card acceptor contact is required");
		CODE_ERROR_BANK.put(36, "Restricted Card");
		CODE_ERROR_BANK.put(37, "Card acceptor acquirer call security");
		CODE_ERROR_BANK.put(38, "Allowable PIN tries exceeded");
		CODE_ERROR_BANK.put(39, "No credit account");
		CODE_ERROR_BANK.put(41, "The Missing Card");
		CODE_ERROR_BANK.put(43, "Reported Robbed Card");
		CODE_ERROR_BANK.put(51, "Insufficient Funds");
		CODE_ERROR_BANK.put(54, "Card Expired");
		CODE_ERROR_BANK.put(55, "Incorrect personal identification number");
		CODE_ERROR_BANK.put(56, "No card record");
		CODE_ERROR_BANK.put(57, "Transaction Disallowed");
		CODE_ERROR_BANK.put(58, "Transaction not permitted to terminal");
		CODE_ERROR_BANK.put(61, "Withdrawal Limit Exceeded");
		CODE_ERROR_BANK.put(62, "Restricted Card");
		CODE_ERROR_BANK.put(65, "Exceeds withdrawal frequency limit");
		CODE_ERROR_BANK.put(68, "Response received too late");
		CODE_ERROR_BANK.put(75, "Allowable number of PIN tries exceeded");
		CODE_ERROR_BANK.put(76, "Error (76)");
		CODE_ERROR_BANK.put(77, "Error (77)");
		CODE_ERROR_BANK.put(78, "Error (78)");
		CODE_ERROR_BANK.put(79, "Error (79)");
		CODE_ERROR_BANK.put(80, "Error (80)");
		CODE_ERROR_BANK.put(81, "Error (81)");
		CODE_ERROR_BANK.put(82, "Card Rejected");
		CODE_ERROR_BANK.put(83, "Error (83)");
		CODE_ERROR_BANK.put(84, "Error (84)");
		CODE_ERROR_BANK.put(85, "Error (85)");
		CODE_ERROR_BANK.put(86, "Error (86)");
		CODE_ERROR_BANK.put(87, "Error (87)");
		CODE_ERROR_BANK.put(88, "Error (88)");
		CODE_ERROR_BANK.put(89, "Error (89)");
		CODE_ERROR_BANK.put(90, "Cutoff is in process, a switch is ending business for a day and starting the next (transaction can b");
		CODE_ERROR_BANK.put(91, "Issuer or switch is inoperative 92 Financial institution or intermediate network facility can not be");
		CODE_ERROR_BANK.put(94, "Duplicate transmission");
		CODE_ERROR_BANK.put(95, "Call your Bank");
		CODE_ERROR_BANK.put(96, "System malfunction");

	}
}
