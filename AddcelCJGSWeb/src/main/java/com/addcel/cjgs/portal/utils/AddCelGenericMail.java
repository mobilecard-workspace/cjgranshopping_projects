package com.addcel.cjgs.portal.utils;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.addcel.cjgs.portal.model.mapper.PortalCJGSMapper;
import com.addcel.cjgs.portal.model.vo.AbstractVO;
import com.addcel.cjgs.portal.model.vo.CorreoVO;
import com.addcel.cjgs.portal.model.vo.PaymentData;
import com.addcel.cjgs.portal.model.vo.SmtpVO;
import com.addcel.utils.Utilerias;

@Service
public class AddCelGenericMail {
	private static final Logger logger = LoggerFactory.getLogger(AddCelGenericMail.class);
	
	@Autowired
	private PortalCJGSMapper portalMapper;
	
	@Autowired
	private UtilsService utilService;
	
	public AbstractVO generatedMail(PaymentData paymentData, int module){
		CorreoVO correo = new CorreoVO();
		SmtpVO smtp = null;
		AbstractVO resp = null;
		List<String> from = null;
		try{
			
			smtp = portalMapper.obtenDatosMail(38, module, 38);
			from = portalMapper.obtenDatosFrom(38);
			
			if(smtp == null){
				resp = new AbstractVO(20, "Does not exist e-mail settings");
			}else if(from == null || from.size() == 0){
					resp = new AbstractVO(21, "Does not exist recipient (email) configured");
			}else{
				String body = smtp.getBody();
				body = body.replaceFirst("<#NOMBRE#>", paymentData.getFullName() != null? paymentData.getFullName(): "Usuario");
				body = body.replaceFirst("<#PRODUCTO#>", paymentData.getDescProduct());
				body = body.replaceFirst("<#MONTO#>", Utilerias.formatoImporteMon(paymentData.getTotal()));
				body = body.replaceFirst("<#DATE#>", paymentData.getFecha());
				body = body.replaceFirst("<#AUTORIZACION#>", paymentData.getNumAutorizacion());
				body = body.replaceFirst("<#REFERENCIA#>", String.valueOf(paymentData.getIdBitacora()));
				body = body.replaceFirst("<#CARD#>", paymentData.getCardNumber());
				body = body.replaceFirst("<#NUMBEROCONTROL#>", paymentData.getNumeroControl());
				
				body = body.replaceAll("<#ACOUNT#>", smtp.getBcc());
				
				String[] cid = {
						"/usr/java/resources/images/Addcel/MobileCard_header.PNG"
						};
		
				correo.setBcc(new String[]{});
				correo.setCc(smtp.getBcc().split(","));
				correo.setCid(cid);
				correo.setBody(body);
				correo.setFrom(smtp.getCorreo());
				correo.setSubject(smtp.getSubject().replaceFirst("<#COMPANY#>", "CJ Gran Shopping").replaceFirst("<#REFERENCIA#>", String.valueOf(paymentData.getIdBitacora())));
				correo.setTo(from.toArray(new String[from.size()]));
				
				sendMail(utilService.objectToJson(correo));
			}
		}catch(Exception e){
			resp = new AbstractVO(21, "An error has occurred when trying to send mail"); 
			logger.error("Ocurrio un error al enviar el email ", e);
		}
		return resp;
	}
	

	public void sendMail(String data) {
		String line = null;
		StringBuilder sb = new StringBuilder();
		try {
			logger.info("Iniciando proceso de envio email. ");
			//logger.debug("Mail Data: {}", data);

			URL url = new URL(ConstantesCJGS.URL_MAIL_SENDER);
			logger.info("Conectando con " + ConstantesCJGS.URL_MAIL_SENDER);
			HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

			urlConnection.setDoOutput(true);
			urlConnection.setRequestProperty("Content-Type", "application/json");
			urlConnection.setRequestProperty("Accept", "application/json");
			urlConnection.setRequestMethod("POST");

			OutputStreamWriter writter = new OutputStreamWriter(urlConnection.getOutputStream());
			writter.write(data);
			writter.flush();

//			logger.info("Datos enviados, esperando respuesta");

			BufferedReader reader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));

			while ((line = reader.readLine()) != null) {
				sb.append(line);
			}

//			logger.info("Respuesta del servidor: " + sb.toString());
		} catch (Exception ex) {
			logger.error("Error en: sendMail, al enviar el email ", ex);
		}
	}
}
