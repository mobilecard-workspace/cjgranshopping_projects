package com.addcel.cjgs.portal.utils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.security.GeneralSecurityException;
import java.util.HashMap;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSession;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.addcel.utils.AddcelCrypto;

@Component
public class UtilsService {
	private static final Logger logger = LoggerFactory.getLogger(UtilsService.class);
	
	@Autowired
	private ObjectMapper mapperJk;	
	
	public <T> String objectToJson(T object){
		String json=null;
		try {
			json = mapperJk.writeValueAsString(object);
//			logger.info("Encoding DEFAULT: {}",json);
			json = new String(json.getBytes(),"UTF-8");
//			logger.info("Encoding ISO-8859-1: {}",json);
		} catch (JsonGenerationException e) {
			logger.error("1.- Error al generar json: {}",e);
		} catch (JsonMappingException e) {
			logger.error("2.- Error al mapperar objeccto: {}",e);
		} catch (IOException e) {
			logger.error("3.- Error al generar json: {}",e);
		}
		return json;
	}

	public <T> Object jsonToObject(String json, Class<T> clase) {		
		Object obj = null;
		try {
			obj = mapperJk.readValue(json, clase);
		} catch (JsonParseException e) {
			logger.error("Error al parsear cadena json 1: {}", e);
		} catch (JsonMappingException e) {
			logger.error("Error al parsear cadena json 2: {}", e);
		} catch (IOException e) {
			logger.error("Error al parsear cadena json 3: {}", e);
		}
		return obj;
	}
	
	public HashMap<String, Object> getRespuestaBusquedas(int idError, String msgExito, String nombre, Object obj){
		HashMap<String, Object> resp = new HashMap<String, Object>();
		resp.put(ConstantesCJGS.NOM_CAMPO_ID_ERROR, idError);
		if(msgExito != null){
			resp.put(ConstantesCJGS.NOM_CAMPO_MENJ_ERROR, msgExito);
		}
		if(nombre != null){
			resp.put(nombre, obj);
		}
		
		
		return resp;
	}
	
	private static final String base = "0123456789abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	
	public String generatePassword(int longitud){
		StringBuffer contrasena = new StringBuffer();
		int numero = 0;
		for(int i = 0; i < longitud; i++){ //1
		    numero = (int)(Math.random()*base.length()); //2
		    contrasena.append(base.substring(numero, numero+1)); //4
		}
		return contrasena.toString();
	}
	
	private static final String MASCARA = "XXXX XXXX XXXX ";
	public String obtenerMascara(String carnet, boolean addX){
		carnet = AddcelCrypto.decryptTarjeta(carnet);
		if(addX){
			carnet = MASCARA + carnet.substring(carnet.length() -4 );
		}
		
		return carnet;
	}
	
	public String notificacionURLComercio(String urlServicio, String parametros) {
		if(urlServicio.startsWith("https")){
			logger.info("Mandando notificacion de compra por https");
			return notificacionHttpsURLComercio(urlServicio, parametros, true);
		} else {
			logger.info("Mandando notificacion de compra por http...");
			return notificacionHttpURLComercio(urlServicio, parametros, true);
		}
	}
	
	public String notificacionURLComercio(String urlServicio, String parametros, boolean method) {
		if(urlServicio.startsWith("https")){
			logger.info("Mandando notificacion de compra por https");
			return notificacionHttpsURLComercio(urlServicio, parametros, method);
		} else {
			logger.info("Mandando notificacion de compra por http...");
			return notificacionHttpURLComercio(urlServicio, parametros, method);
		}
	}
	
	public String notificacionHttpURLComercio(String urlServicio, String parametros, boolean method) {
        InputStream is = null;
        OutputStream os = null;
        InputStreamReader isr = null;
        BufferedWriter writer = null;
        BufferedReader br = null;
        HttpURLConnection conn = null;
        URL url = null;
        String inputLine = null;
        StringBuffer sbf = new StringBuffer();
        try {
        	logger.debug("URL Notificacion: {}", urlServicio);
        	logger.debug("Param Notificacion: {}", parametros);
            url = new URL(urlServicio);
            conn = (HttpURLConnection) url.openConnection();
            if (conn != null) {
                conn.setConnectTimeout(20000);
                conn.setRequestMethod(method? "POST": "GET");
                conn.setDoInput(true);
                if (parametros != null) {
                    conn.setDoOutput(true);
                    os = conn.getOutputStream();
                    writer = new BufferedWriter(new OutputStreamWriter(os));
                    writer.write(parametros);
                    writer.close();
                    os.close();
                    conn.connect();
                }
                is = conn.getInputStream();
                isr = new InputStreamReader(is);
                br = new BufferedReader(isr);
                while ((inputLine = br.readLine()) != null) {
                    sbf.append(inputLine);
                }
                logger.debug("Respuesta notificacion: {}", sbf);
            }
        } catch (Exception e) {
         logger.error("Error al leer la url: {},    {} ", url, e);
        } finally {
            if (conn != null) {
                try {
                    conn.disconnect();
                } catch (Exception e) {
                 logger.error("Exception : ", e);
                }
            }
            if (br != null) {
                try {
                    br.close();
                } catch (Exception e) {
                 logger.error("Exception : ", e);
                }
            }

            if (isr != null) {
                try {
                    isr.close();
                } catch (Exception e) {
                 logger.error("Exception : ", e);
                }
            }
            if (is != null) {
                try {
                    is.close();
                } catch (Exception e) {
                 logger.error("Exception : ", e);
                }
            }
        }
        return sbf.toString();
    }
	
	public String notificacionHttpsURLComercio(String urlServicio, String parametros, boolean method) {
		InputStream is = null;
        OutputStream os = null;
        InputStreamReader isr = null;
        BufferedWriter writer = null;
        BufferedReader br = null;
        HttpURLConnection conn = null;
		URL url = null;
		String inputLine = null;
        StringBuffer sbf = new StringBuffer();
        
		HostnameVerifier hv = new HostnameVerifier() {
			public boolean verify(String urlHostName, SSLSession session) {
				logger.debug("Warning: URL Host: " + urlHostName + " vs. " + session.getPeerHost());
				return true;
			}
		};	        
	    
        try {
        	trustAllHttpsCertificates();
    	    HttpsURLConnection.setDefaultHostnameVerifier(hv);
    	    
        	logger.debug("URL Notificacion: {}", urlServicio);
        	logger.debug("Param Notificacion: {}", parametros);

            url = new URL(urlServicio);
            conn = (HttpURLConnection) url.openConnection();
            if (conn != null) {
                conn.setConnectTimeout(20000);
                conn.setRequestMethod(method? "POST": "GET");
                conn.setDoInput(true);
                if (parametros != null) {
                    conn.setDoOutput(true);
                    os = conn.getOutputStream();
                    writer = new BufferedWriter(new OutputStreamWriter(os));
                    writer.write(parametros);
                    writer.close();
                    os.close();
                    conn.connect();
                }
                is = conn.getInputStream();
                isr = new InputStreamReader(is);
                br = new BufferedReader(isr);
                while ((inputLine = br.readLine()) != null) {
                    sbf.append(inputLine);
                }
                logger.debug("Respuesta notificacion: {}", sbf);
            }
        } catch (GeneralSecurityException e) {
        	logger.error("Error Al generar certificados: {}" , e);
        }catch (MalformedURLException e) {
        	logger.error("error al conectar con la URL: {},    {}", urlServicio, e);
        }catch (ProtocolException e) {
        	logger.error("Error al enviar datos: {}" , e);
		} catch (IOException e) {
			logger.error("Error: {}" , e);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
            if (br != null) {
                try {
                    br.close();
                } catch (Exception e) {
                 logger.error("Exception : ", e);
                }
            }

            if (isr != null) {
                try {
                    isr.close();
                } catch (Exception e) {
                 logger.error("Exception : ", e);
                }
            }
            if (is != null) {
                try {
                    is.close();
                } catch (Exception e) {
                 logger.error("Exception : ", e);
                }
            }
            if (conn != null) {
                try {
                    conn.disconnect();
                } catch (Exception e) {
                 logger.error("Exception : ", e);
                }
            }
        }
        return sbf.toString(); 
	}
	
	public static class miTM implements javax.net.ssl.TrustManager,
			javax.net.ssl.X509TrustManager {
		public java.security.cert.X509Certificate[] getAcceptedIssuers() {
			return null;
		}

		public boolean isServerTrusted(
				java.security.cert.X509Certificate[] certs) {
			return true;
		}

		public boolean isClientTrusted(
				java.security.cert.X509Certificate[] certs) {
			return true;
		}

		public void checkServerTrusted(
				java.security.cert.X509Certificate[] certs, String authType)
				throws java.security.cert.CertificateException {
			return;
		}

		public void checkClientTrusted(
				java.security.cert.X509Certificate[] certs, String authType)
				throws java.security.cert.CertificateException {
			return;
		}
	}

	private static void trustAllHttpsCertificates() throws Exception {
		javax.net.ssl.TrustManager[] trustAllCerts =
		new javax.net.ssl.TrustManager[1];
		javax.net.ssl.TrustManager tm = new miTM();
		trustAllCerts[0] = tm;
		javax.net.ssl.SSLContext sc =
		javax.net.ssl.SSLContext.getInstance("SSL");
		sc.init(null, trustAllCerts, null);
		javax.net.ssl.HttpsURLConnection.setDefaultSSLSocketFactory(
		sc.getSocketFactory());
	}

	
}
