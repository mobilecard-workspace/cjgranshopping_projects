package com.addcel.cjgs.portal.model.vo;

import java.util.List;

public class PaymentDataResponse {	
	private int totalRecords;
	private int curPage;
	private List<PaymentData> data;
	public int getTotalRecords() {
		return totalRecords;
	}
	public void setTotalRecords(int totalRecords) {
		this.totalRecords = totalRecords;
	}
	public int getCurPage() {
		return curPage;
	}
	public void setCurPage(int curPage) {
		this.curPage = curPage;
	}
	public List<PaymentData> getData() {
		return data;
	}
	public void setData(List<PaymentData> data) {
		this.data = data;
	}
	
}
