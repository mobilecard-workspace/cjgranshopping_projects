package com.addcel.cjgs.portal.model.vo;

import java.util.HashMap;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class PaymentData {

	private int idBitacora;
	private long idUsuario;
	private String canal;
	private int idCanal;
	private String modoPago;
	private int idModoPago;
	private String descProduct;
	private String fullName;
	private String email;
	private String cardNumber;
	private int cardType;
	private String msi;
	private int plazo;
	private String total;
	private String comicion;
	private String numeroControl;
	private String referenciaPay;
	private String vigencia;
	private int idStatus;
	private String status;
	private String numAutorizacion;
	private String numRefund;
	private String transactionIdn;
	private String claveError;
	private String descError;
	private String fecha;
	
	private String afiliacion;
	
	private String refunded = "";
	private String refundedNot = "";
	private String chargeBack = "";
	private String chargeBackNot = "";
	
	public PaymentData(){
	}
	
	public PaymentData(HashMap<String, Object> data){
		if(data != null){
			this.idBitacora = Integer.parseInt(data.get("idBitacora") + "");
			this.idUsuario = (Long) data.get("idUsuario");
			this.numeroControl = (String) data.get("numeroControl");
			this.canal = (String) data.get("canal");
			this.modoPago = (String) data.get("modoPago");
			this.descProduct = (String) data.get("descProduct");
			this.fullName = (String) data.get("fullName");
			this.email = (String) data.get("email");
			this.cardNumber = (String) data.get("cardNumber");
			this.cardType = (Integer) data.get("tipoTarjeta");
			this.total = String.valueOf(data.get("total"));
			this.comicion = String.valueOf(data.get("comicion"));
			this.idStatus = (Integer) data.get("idStatus");
			this.status = (String) data.get("status");
			this.afiliacion = (String) data.get("afiliacion");
			this.numAutorizacion = (String) data.get("numAutorizacion");
			this.transactionIdn = (String) data.get("transactionIdn");
//			this.claveError = (String) data.get("claveError");
//			this.descError = (String) data.get("descError");
			this.fecha = (String) data.get("fecha");
			this.referenciaPay = (String) data.get("referenciaPaynet");
			this.vigencia = (String) data.get("vigencia");
			this.msi = (String) data.get("msi");
			this.plazo = (Integer) data.get("plazo");
		}
	}
	
	public int getIdBitacora() {
		return idBitacora;
	}

	public void setIdBitacora(int idBitacora) {
		this.idBitacora = idBitacora;
	}

	public long getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(long idUsuario) {
		this.idUsuario = idUsuario;
	}

	public String getCanal() {
		return canal;
	}

	public void setCanal(String canal) {
		this.canal = canal;
	}

	public String getModoPago() {
		return modoPago;
	}

	public void setModoPago(String modoPago) {
		this.modoPago = modoPago;
	}

	public String getDescProduct() {
		return descProduct;
	}

	public void setDescProduct(String descProduct) {
		this.descProduct = descProduct;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public int getCardType() {
		return cardType;
	}

	public void setCardType(int cardType) {
		this.cardType = cardType;
	}

	public String getMsi() {
		return msi;
	}

	public void setMsi(String msi) {
		this.msi = msi;
	}

	public int getPlazo() {
		return plazo;
	}

	public void setPlazo(int plazo) {
		this.plazo = plazo;
	}

	public String getTotal() {
		return total;
	}

	public void setTotal(String total) {
		this.total = total;
	}

	public String getComicion() {
		return comicion;
	}

	public void setComicion(String comicion) {
		this.comicion = comicion;
	}

	public String getNumeroControl() {
		return numeroControl;
	}

	public void setNumeroControl(String numeroControl) {
		this.numeroControl = numeroControl;
	}

	public int getIdStatus() {
		return idStatus;
	}

	public void setIdStatus(int idStatus) {
		this.idStatus = idStatus;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getNumAutorizacion() {
		return numAutorizacion;
	}

	public void setNumAutorizacion(String numAutorizacion) {
		this.numAutorizacion = numAutorizacion;
	}

	public String getNumRefund() {
		return numRefund;
	}

	public void setNumRefund(String numRefund) {
		this.numRefund = numRefund;
	}

	public String getClaveError() {
		return claveError;
	}

	public void setClaveError(String claveError) {
		this.claveError = claveError;
	}

	public String getDescError() {
		return descError;
	}

	public void setDescError(String descError) {
		this.descError = descError;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public String getAfiliacion() {
		return afiliacion;
	}

	public void setAfiliacion(String afiliacion) {
		this.afiliacion = afiliacion;
	}

	public String getChargeBack() {
		return chargeBack;
	}

	public void setChargeBack(String chargeBack) {
		this.chargeBack = chargeBack;
	}

	public String getChargeBackNot() {
		return chargeBackNot;
	}

	public void setChargeBackNot(String chargeBackNot) {
		this.chargeBackNot = chargeBackNot;
	}

	public String getReferenciaPay() {
		return referenciaPay;
	}

	public void setReferenciaPay(String referenciaPay) {
		this.referenciaPay = referenciaPay;
	}

	public String getVigencia() {
		return vigencia;
	}

	public void setVigencia(String vigencia) {
		this.vigencia = vigencia;
	}

	public String getRefunded() {
		return refunded;
	}

	public void setRefunded(String refunded) {
		this.refunded = refunded;
	}

	public int getIdCanal() {
		return idCanal;
	}

	public void setIdCanal(int idCanal) {
		this.idCanal = idCanal;
	}

	public int getIdModoPago() {
		return idModoPago;
	}

	public void setIdModoPago(int idModoPago) {
		this.idModoPago = idModoPago;
	}

	public String getRefundedNot() {
		return refundedNot;
	}

	public void setRefundedNot(String refundedNot) {
		this.refundedNot = refundedNot;
	}

	public String getTransactionIdn() {
		return transactionIdn;
	}

	public void setTransactionIdn(String transactionIdn) {
		this.transactionIdn = transactionIdn;
	}

			
}
