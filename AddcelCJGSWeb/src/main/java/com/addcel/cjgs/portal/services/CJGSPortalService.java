package com.addcel.cjgs.portal.services;

import java.io.IOException;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.ModelAndView;

import com.addcel.cjgs.portal.model.mapper.PortalCJGSMapper;
import com.addcel.cjgs.portal.model.vo.AbstractVO;
import com.addcel.cjgs.portal.model.vo.LoginVO;
import com.addcel.cjgs.portal.model.vo.PaymentData;
import com.addcel.cjgs.portal.model.vo.PaymentDataRequest;
import com.addcel.cjgs.portal.model.vo.PaymentDataResponse;
import com.addcel.cjgs.portal.model.vo.PaymentDataResponseVO;
import com.addcel.cjgs.portal.model.vo.ProsaPagoVO;
import com.addcel.cjgs.portal.model.vo.UsuarioVO;
import com.addcel.cjgs.portal.utils.AddCelGenericMail;
import com.addcel.cjgs.portal.utils.ConstantesCJGS;
import com.addcel.cjgs.portal.utils.UtilsService;
import com.addcel.utils.AddcelCrypto;

@Service
public class CJGSPortalService {
	private static final Logger logger = LoggerFactory.getLogger(CJGSPortalService.class);
	private static final String patronCom = "yyyy-MM-dd";
	private static final SimpleDateFormat formatoCom = new SimpleDateFormat(patronCom);
	
	@Autowired
	private PortalCJGSMapper portalMapper;
	
	@Autowired
	private UtilsService utilService;
	
	@Autowired
	private AddCelGenericMail mailsender;
	
	public UsuarioVO login(LoginVO login) {
		UsuarioVO usuario = new UsuarioVO();
		List<UsuarioVO> usuarioOriginal = null;
		try {
			usuario.setLogin(login.getLogin());
			usuarioOriginal = portalMapper.validarUsuario(usuario);
			
			login.setPassword(AddcelCrypto.encryptPassword(login.getPassword()));
			
			if(usuarioOriginal == null || (usuarioOriginal != null && usuarioOriginal.size() == 0)){
				usuario = new UsuarioVO(107, "Login or password incorrect.");
			}else if(!usuarioOriginal.get(0).getPassword().equals(login.getPassword())){
				usuario = new UsuarioVO(107, "Login or password incorrect.");
			}else if(usuarioOriginal.get(0).getStatus() != 1){
				usuario = new UsuarioVO(105, ConstantesCJGS.ERROR_USUARIO_STATUS_INACTIVO);
			}else{
				usuario = usuarioOriginal.get(0);
				usuario.setPassword("");
			}
		}catch(Exception e){
			usuario = new UsuarioVO(2, "An error occurred at login.");
			logger.error("Error en el login: {}", e.getMessage());
			
		}
		return usuario;		
	}	
	
	public String busquedaTransaccion(PaymentDataRequest filtroTransaccion, UsuarioVO usuario){				
		PaymentDataResponse resp = new PaymentDataResponse();
		String json = null;
		try {
			if(validarSession(usuario) == null){
				if(usuario.getRol() != 1){
					filtroTransaccion.setIdCompany(usuario.getIdCompania());
					filtroTransaccion.setStartDate(null);
					filtroTransaccion.setEndDate(null);
					filtroTransaccion.setTipoBusqueda(filtroTransaccion.getTipoBusqueda() >= 1 && filtroTransaccion.getTipoBusqueda() <= 5? filtroTransaccion.getTipoBusqueda(): 1);
				}else{
					if((filtroTransaccion.getStartDate() == null || "".equals(filtroTransaccion.getStartDate())) &&
						(filtroTransaccion.getEndDate() == null || "".equals(filtroTransaccion.getEndDate()))){
						filtroTransaccion.setTipoBusqueda( 1);
					}
				}
				
				resp.setData(portalMapper.buscarPagos(filtroTransaccion));
				
				if(resp.getData() != null && resp.getData().size() > 0){
					resp.setTotalRecords(resp.getData().size() +1);
					resp.setCurPage(1);
					
					if(usuario.getRol() == 1){
						for(PaymentData data: resp.getData()){
							if(!"3".equals(data.getModoPago())){
								if(data.getIdStatus() == 1){
									data.setRefunded("Notif Cancellation");
									data.setChargeBack("Notif Dispute");
								}else if(data.getIdStatus() == 2){
									data.setRefunded("Finish Cancellation");
									data.setRefundedNot("Back Cancellation");
								}else if(data.getIdStatus() == 4){
									data.setChargeBack("Finish ChargeBack");
									data.setChargeBackNot("Cancel Dispute");
								}
							}
						}
					}else if(usuario.getRol() == 2){
						for(PaymentData data: resp.getData()){
							if(!"3".equals(data.getModoPago())){
								if(data.getIdStatus() == 1){
									data.setRefunded("Notif Cancellation");
								}else if(data.getIdStatus() == 2){
//									data.setRefunded("Finish Cancellation");
									data.setRefundedNot("Back Cancellation");
								}
							}
						}
					}
				}else{
					resp.setTotalRecords(resp.getData().size() +1);
					resp.setCurPage(1);
					resp.setData(new ArrayList<PaymentData>());
				}
			}else{
				resp.setTotalRecords(0);
				resp.setCurPage(1);
				resp.setData(new ArrayList<PaymentData>());
				resp.getData().add(new PaymentData());
				resp.getData().get(0).setIdBitacora(-1);
				resp.getData().get(0).setStatus("");
				resp.getData().get(0).setNumeroControl(ConstantesCJGS.ERROR_NO_SESSION);
			}
			
		}catch (Exception pe) {
			logger.error("Ocurrio un error general al buscar tiendas desde el portal: {}", pe.getMessage());
		}finally{
			json = utilService.objectToJson(resp);
		}
		return json;
	}
	

	public String actulizarRegistro(PaymentData paymentData, UsuarioVO usuario){
		PaymentDataResponseVO respuesta = null;
		AbstractVO resp = new AbstractVO();
		List<HashMap<String, Object>> pagoBD = null;
		String json = null;
		try {
			resp = validarSession(usuario);
			if(resp == null){
				if(usuario.getRol() != 1 && usuario.getRol() != 2){
					resp = new AbstractVO(10, "The user does not have permissions to make this operation.");
				}else{
					pagoBD = portalMapper.validarPago(paymentData);
					
					if(pagoBD == null || pagoBD.size() == 0){
						resp = new AbstractVO(11, "The reference number does not exist, ReferenceMC: " + paymentData.getIdBitacora());
					}else if((Integer)pagoBD.get(0).get("idStatusTienda") != 1){
						resp = new AbstractVO(12, "The provider is disabled.");
					}else if(!((Integer)pagoBD.get(0).get("idStatus") == 1 && paymentData.getIdStatus() == 2 ||
							 (Integer)pagoBD.get(0).get("idStatus") == 2 && paymentData.getIdStatus() == 3 ||
							 (Integer)pagoBD.get(0).get("idStatus") == 2 && paymentData.getIdStatus() == 1 ||
							 (Integer)pagoBD.get(0).get("idStatus") == 1 && paymentData.getIdStatus() == 4 ||
							 (Integer)pagoBD.get(0).get("idStatus") == 4 && paymentData.getIdStatus() == 1 ||
							 (Integer)pagoBD.get(0).get("idStatus") == 4 && paymentData.getIdStatus() == 5
							)){
						resp = new AbstractVO(13, "The new status is not valid.");
					}else{
						try{
							portalMapper.insertBitacoraOperaciones((Integer)pagoBD.get(0).get("idBitacora"), usuario.getIdUsuario(), paymentData.getIdStatus(), 1);
						}catch(Exception e){
							logger.error("Ocurrio un error general al inserta en bitacora operaciones registro: {}", e);
						}
						
						if(portalMapper.updateCJGSDetallePData(paymentData) == 1){
							if(paymentData.getIdStatus() == 1 && (Integer)pagoBD.get(0).get("idStatus") == 4){
								resp = new AbstractVO(0, "Was canceled the chargeback, ReferenceMC: " + paymentData.getIdBitacora());
								
							}else if(paymentData.getIdStatus() == 1 && (Integer)pagoBD.get(0).get("idStatus") == 2){
									resp = new AbstractVO(0, "Was back the cancelation, ReferenceMC: " + paymentData.getIdBitacora());
									
							}else if(paymentData.getIdStatus() == 2){
								if(((Integer)pagoBD.get(0).get("idModoPago")) == 3){
									resp = new AbstractVO(0, "Invalid operation for this transaction, ReferenceMC: " + paymentData.getIdBitacora());
									paymentData.setIdStatus((Integer)pagoBD.get(0).get("idStatus"));
									portalMapper.updateCJGSDetallePData(paymentData);
									
								}else if(((Integer)pagoBD.get(0).get("idCanal")) != 3 && ((Integer)pagoBD.get(0).get("idModoPago")) != 3){
									resp = mailsender.generatedMail(new PaymentData(pagoBD.get(0)), 3);
									if(resp == null){
										resp = new AbstractVO(0, "Sent a notification of Cancelation to support Addcel, ReferenceMC: " + paymentData.getIdBitacora());
										
									}else{
										paymentData.setIdStatus((Integer)pagoBD.get(0).get("idStatus"));
										portalMapper.updateCJGSDetallePData(paymentData);
									}
									
								}else{
									
									if((Integer)pagoBD.get(0).get("tipoTarjeta") != 3){
										respuesta = enviarCancelacionPagoVisa((Integer)pagoBD.get(0).get("idBitacora"), (Integer)pagoBD.get(0).get("transactionIdn"));
										
										if(respuesta.getStatus() == 3){
											resp = new AbstractVO(0, respuesta.getMensajeError());
										}else{
											resp = new AbstractVO(1, respuesta.getMensajeError());
										}
										
									}else{
										if(portalMapper.updateTransaccionAMEX((String)pagoBD.get(0).get("transactionIdn")) == 0){
											updateBitacoras((Integer)pagoBD.get(0).get("idBitacora"), null, (String)pagoBD.get(0).get("transactionIdn"), 3);
											
											resp = new AbstractVO(0, "The cancellation was done successfully");
										}else{
											resp = new AbstractVO(1, "An error was obtained when trying to cancel the payment.");
											updateBitacoras((Integer)pagoBD.get(0).get("idBitacora"), null, (String)pagoBD.get(0).get("transactionIdn"), 1);
										}
									}
								}
								
							}else if(paymentData.getIdStatus() == 3){
								resp = mailsender.generatedMail(new PaymentData(pagoBD.get(0)), 4);
								if(resp == null){
									resp = new AbstractVO(0, "Sent a notification of Cancelation to provider, ReferenceMC: " + paymentData.getIdBitacora());
								}else{
									paymentData.setIdStatus((Integer)pagoBD.get(0).get("idStatus"));
									portalMapper.updateCJGSDetallePData(paymentData);
								}
								
							}else if(paymentData.getIdStatus() == 4){
								resp = mailsender.generatedMail(new PaymentData(pagoBD.get(0)), 1);
								if(resp == null){
									resp = new AbstractVO(0, "Sent a notification of chargeback to provider, ReferenceMC: " + paymentData.getIdBitacora());
								}else{
									paymentData.setIdStatus((Integer)pagoBD.get(0).get("idStatus"));
									portalMapper.updateCJGSDetallePData(paymentData);
								}
								
							}else if(paymentData.getIdStatus() == 5){
								resp = mailsender.generatedMail(new PaymentData(pagoBD.get(0)), 2);
								if(resp == null){
									resp = new AbstractVO(0, "Status was changed to chargeback, successfully, ReferenceMC: " + paymentData.getIdBitacora());
								}else{
									paymentData.setIdStatus((Integer)pagoBD.get(0).get("idStatus"));
									portalMapper.updateCJGSDetallePData(paymentData);
								}
							}
						}else{
							resp = new AbstractVO(11, "The reference number does not exist, ReferenceMC: " + paymentData.getIdBitacora());
						}
					}
				}
			}
			
		}catch (Exception pe) {
			logger.error("Ocurrio un error general al actualizar registro: {}", pe);
			resp = new AbstractVO(14, "An error has occurred to updated record: " + pe.getMessage());
			
			if(pagoBD != null && pagoBD.get(0) != null && pagoBD.get(0).get("idStatus") != null){
				paymentData.setIdStatus((Integer)pagoBD.get(0).get("idStatus"));
				portalMapper.updateCJGSDetallePData(paymentData);
			}
		}finally{
			json = utilService.objectToJson(resp);
		}
		return json;
	}
	
	public PaymentDataResponseVO enviarCancelacionPagoVisa(int idBitacora, int transactionIdn){
		PaymentDataResponseVO respuesta = null;
		ProsaPagoVO prosaPagoVO = null;
		try{
			StringBuffer data = new StringBuffer() 
					.append( "transactionIdn="      ).append( URLEncoder.encode(transactionIdn+"", "UTF-8") );
					
			logger.info("Envío de datos Cancelacion CJGS VISA: {}", data);
			
			String cadena = utilService.notificacionURLComercio(ConstantesCJGS.URL_DEVO_PROSA, data.toString());
			
//			logger.info("Respuesta de PROSA CANCELACION: " + cadena);
			
			prosaPagoVO = (ProsaPagoVO) utilService.jsonToObject(cadena, ProsaPagoVO.class);
			
			if(prosaPagoVO != null){
				respuesta = new PaymentDataResponseVO();
				respuesta.setIdBitacora(idBitacora);
				respuesta.setTransactionId(prosaPagoVO.getTransactionId());
//				respuesta.setAuthorizationNumber(prosaPagoVO.getAutorizacion());
				respuesta.setAuthorizationNumber(prosaPagoVO.getClaveOperacion());
				respuesta.setIdError(Integer.parseInt(
						prosaPagoVO.getClaveRechazo()!= null && !"".equals(prosaPagoVO.getClaveRechazo())? prosaPagoVO.getClaveRechazo(): "0"));
				respuesta.setMensajeError(prosaPagoVO.getDescripcionRechazo());
				respuesta.setDate(formatoCom.format(new Date()));
				
				
				if(prosaPagoVO.isAuthorized()){
					respuesta.setStatus(3);
					respuesta.setMensajeError("The cancellation was done successfully.");
					
				}else if(prosaPagoVO.isRejected()){
					respuesta.setStatus(1);
					respuesta.setIdError(Integer.parseInt(
						prosaPagoVO.getClaveRechazo()!= null && !"".equals(prosaPagoVO.getClaveRechazo())? prosaPagoVO.getClaveRechazo(): "1"));
					respuesta.setMensajeError(ConstantesCJGS.CODE_ERROR_BANK.get(respuesta.getIdError()));
					
				}else if(prosaPagoVO.isProsaError()){
					respuesta.setStatus(1);
					respuesta.setIdError(Integer.parseInt(
							prosaPagoVO.getClaveRechazo()!= null && !"".equals(prosaPagoVO.getClaveRechazo())? prosaPagoVO.getClaveRechazo(): "1"));
					respuesta.setMensajeError(prosaPagoVO.getMsg());
				}
				
				updateBitacoras(respuesta.getIdBitacora(), respuesta.getAuthorizationNumber(), respuesta.getTransactionId(), respuesta.getStatus());
				
			}
		}catch(Exception e){
			respuesta = new PaymentDataResponseVO();
			logger.error("An error occurred during the call to the bank: {}", e);
			respuesta.setStatus(200);
			respuesta.setMensajeError("An error occurred during the call to the bank.");
			updateBitacoras(respuesta.getIdBitacora(), respuesta.getAuthorizationNumber(), respuesta.getTransactionId(), respuesta.getStatus());
		}
		return respuesta;
	}
	
	private void updateBitacoras(int idBitacora, String authorizationNumber, String transactionId, int status){
		try{
			if(status == 3){
				logger.debug("Insert bitacora_reverso, IdBitacora: " + idBitacora);
				portalMapper.insertBitacoraReverso(idBitacora);
			}
			
			logger.debug("Update t_bitacora, IdBitacora: " + idBitacora);
			portalMapper.updateTBitacora(idBitacora, status);
			
			logger.debug("Actualizando CJGS_detalle...");
			portalMapper.updateCJGSDetalle(idBitacora, authorizationNumber, transactionId, status);
		}catch(Exception e){
			logger.error("Error general al actualizar en bitacoras: {}", e);
		}
	}
	
	public void exportarBusquedaTXT(PaymentDataRequest filtroTransaccion, UsuarioVO usuario, HttpServletRequest request, HttpServletResponse response){				
		List<PaymentData> data = null;
		StringBuffer cadena = null;
		OutputStream outStream = null;
		try {
			if(validarSession(usuario) == null){
				if(usuario.getRol() != 1){
					filtroTransaccion.setIdCompany(usuario.getIdCompania());
					filtroTransaccion.setStartDate(null);
					filtroTransaccion.setEndDate(null);
					filtroTransaccion.setTipoBusqueda(filtroTransaccion.getTipoBusqueda() >= 1 && filtroTransaccion.getTipoBusqueda() <= 5? filtroTransaccion.getTipoBusqueda(): 1);
				}else{
					if((filtroTransaccion.getStartDate() == null || "".equals(filtroTransaccion.getStartDate())) &&
						(filtroTransaccion.getEndDate() == null || "".equals(filtroTransaccion.getEndDate()))){
						filtroTransaccion.setTipoBusqueda( 1);
					}
				}
				
				data = portalMapper.buscarPagos(filtroTransaccion);
				
				cadena = new StringBuffer();
				cadena.append("MC Reference").append("\t");
				cadena.append("Control Number").append("\t");
				cadena.append("ID Status").append("\t");
				cadena.append("Status").append("\t");
				cadena.append("ID Canal").append("\t");
				cadena.append("Canal").append("\t");
				cadena.append("ID Pay Mode").append("\t");
				cadena.append("Pay Mode").append("\t");
				cadena.append("Date").append("\t");
				cadena.append("Customer Name").append("\t");
				cadena.append("Desc Product").append("\t");
				cadena.append("Reference Payment").append("\t");
				cadena.append("Reference Validity").append("\t");
				cadena.append("MSI").append("\t");
				cadena.append("Months").append("\t");
				cadena.append("Card").append("\t");
				cadena.append("Card Type").append("\t");
				cadena.append("Total").append("\t");
				cadena.append("Num Autho").append("\t");
				cadena.append("\n");
				
				if(data != null && data.size() > 0){
					for(PaymentData payment: data){
						cadena.append( payment.getIdBitacora()).append("\t");
						cadena.append( payment.getNumeroControl()).append("\t");
						cadena.append( payment.getIdStatus()).append("\t");
						cadena.append( payment.getStatus()).append("\t");
						cadena.append( payment.getIdCanal()).append("\t");
						cadena.append( payment.getCanal()).append("\t");
						cadena.append( payment.getIdModoPago()).append("\t");
						cadena.append( payment.getModoPago()).append("\t");
						cadena.append( payment.getFecha()).append("\t");
						cadena.append( payment.getFullName()).append("\t");
						cadena.append( payment.getDescProduct()).append("\t");
						cadena.append( payment.getReferenciaPay()).append("\t");
						cadena.append( payment.getVigencia()).append("\t");
						cadena.append( payment.getMsi()).append("\t");
						cadena.append( payment.getPlazo()).append("\t");
						cadena.append( payment.getCardNumber()).append("\t");
						cadena.append( payment.getCardType()).append("\t");
						cadena.append( payment.getTotal() + payment.getComicion() ).append("\t");
						cadena.append( payment.getNumAutorizacion()).append("\n");
					}
				}
				
				try {
					// set content attributes for the response
			        response.setContentType("text/plain");
			        response.setContentLength(cadena.length());
			 
			        // set headers for the response
			        response.setHeader("Content-Disposition", String.format("attachment; filename=\"%s\"", "FileSearch.txt"));
			 
			        // get output stream of the response
			        outStream = response.getOutputStream();
			 
			        // write bytes read from the input stream into the output stream
			        outStream.write(cadena.toString().getBytes());
					
				} catch (IOException e) {
					logger.error("Ocurrio un error general guardar el archivo: {}", e.getMessage());
				}
			}else{
				 response.setContentType("text/html");
				 response.getWriter().print("Escribiendo en el Html");
//				 request.getRequestDispatcher("/portal/logout").forward(request, response);
				 response.sendRedirect("/AddcelCJGSWeb/portal/logout");
			}
			
		}catch (Exception pe) {
			logger.error("Ocurrio un error general al buscar tiendas desde el portal: {}", pe.getMessage());
		}
	}
	
	private AbstractVO validarSession (UsuarioVO usuario){
		AbstractVO resp = null;
		if(usuario == null){
			resp = new AbstractVO(ConstantesCJGS.ID_NO_SESSION, ConstantesCJGS.ERROR_NO_SESSION);
		}
		return resp;
	}
	
	public List<HashMap<String, String>> buscarCompanias (){
		return portalMapper.buscarCompanias();
	}
	
	public ModelAndView validarSessionController (UsuarioVO usuario){
		logger.info("Dentro del servicio: /portal/welcome");
		ModelAndView mav = null;
		try{
			if(usuario == null){
				logger.debug("Se termino la session, portal/login." );
				mav = new ModelAndView("portal/login", "usuario", new LoginVO());
				mav.addObject("mensaje", ConstantesCJGS.ERROR_NO_SESSION);
			}
		}catch(Exception e){
			mav = new ModelAndView("error");
			logger.error("Ocurrio un error general al validar session: {}", e );
		}
		return mav;	
	}
	
}
