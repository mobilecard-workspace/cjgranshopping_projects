package com.addcel.cjgs.portal.model.vo;

import java.util.HashMap;

public class PaymentDataResponseVO extends AbstractVO{
	
	private int idCompany;
	private int idBitacora;
	private String authorizationNumber;
	private String date;
	private String transactionId;
	private int status; 
	private String token;
	
	public PaymentDataResponseVO(){
		
	}
	
	public PaymentDataResponseVO(String msgError){
		super(msgError);
	}
	
	public PaymentDataResponseVO(int idError, String msgError){
		super(idError, msgError);
	}

	public HashMap<String, Object> toHashMap(){
		HashMap<String, Object> toHash = new HashMap<String, Object>();
		toHash.put("idError", getIdError());
		toHash.put("msgError", getMensajeError());
		toHash.put("reference", idBitacora);
		toHash.put("authorizationNumber", authorizationNumber);
		return toHash;
	}
	
	public int getIdBitacora() {
		return idBitacora;
	}
	public void setIdBitacora(int idBitacora) {
		this.idBitacora = idBitacora;
	}
	public String getAuthorizationNumber() {
		return authorizationNumber;
	}
	public void setAuthorizationNumber(String authorizationNumber) {
		this.authorizationNumber = authorizationNumber;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public int getIdCompany() {
		return idCompany;
	}

	public void setIdCompany(int idCompany) {
		this.idCompany = idCompany;
	}
		
}
