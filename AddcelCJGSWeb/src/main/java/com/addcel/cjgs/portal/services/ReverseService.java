package com.addcel.cjgs.portal.services;

import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.addcel.cjgs.portal.model.mapper.PortalCJGSMapper;
import com.addcel.cjgs.portal.model.vo.PaymentDataResponseVO;
import com.addcel.cjgs.portal.model.vo.ProsaPagoVO;
import com.addcel.cjgs.portal.utils.ConstantesCJGS;
import com.addcel.cjgs.portal.utils.UtilsService;

@Service
public class ReverseService {
	private static final Logger logger = LoggerFactory.getLogger(ReverseService.class);
	
	private static final String patronCom = "yyyy-MM-dd";
	private static final SimpleDateFormat formatoCom = new SimpleDateFormat(patronCom);

	@Autowired
	private PortalCJGSMapper portalMapper;
	
	@Autowired
	private UtilsService utilService;
	
	public PaymentDataResponseVO enviarCancelacionPagoVisa(int idBitacora, String transactionIdn){
		PaymentDataResponseVO respuesta = null;
		ProsaPagoVO prosaPagoVO = null;
		try{
			StringBuffer data = new StringBuffer() 
					.append( "transactionIdn="      ).append( URLEncoder.encode(transactionIdn, "UTF-8") );
					
			logger.info("Envío de datos Cancelacion CJGS VISA: {}", data);
			
			String sb = utilService.notificacionURLComercio(ConstantesCJGS.URL_DEVO_PROSA, data.toString());
			
			logger.info("Respuesta de PROSA CANCELACION: " + sb);
			
			prosaPagoVO = (ProsaPagoVO) utilService.jsonToObject(sb, ProsaPagoVO.class);
			
			if(prosaPagoVO != null){
				respuesta = new PaymentDataResponseVO();
				respuesta.setIdBitacora(idBitacora);
				respuesta.setTransactionId(prosaPagoVO.getTransactionId());
//				respuesta.setAuthorizationNumber(prosaPagoVO.getAutorizacion());
				respuesta.setAuthorizationNumber(prosaPagoVO.getClaveOperacion());
				respuesta.setIdError(Integer.parseInt(
						prosaPagoVO.getClaveRechazo()!= null && !"".equals(prosaPagoVO.getClaveRechazo())? prosaPagoVO.getClaveRechazo(): "0"));
				respuesta.setMensajeError(prosaPagoVO.getDescripcionRechazo());
				respuesta.setDate(formatoCom.format(new Date()));
				
				
				if(prosaPagoVO.isAuthorized()){
					respuesta.setStatus(3);
					respuesta.setMensajeError("The cancellation was done successfully.");
					
				}else if(prosaPagoVO.isRejected()){
					respuesta.setStatus(1);
					respuesta.setIdError(Integer.parseInt(
						prosaPagoVO.getClaveRechazo()!= null && !"".equals(prosaPagoVO.getClaveRechazo())? prosaPagoVO.getClaveRechazo(): "1"));
					respuesta.setMensajeError(ConstantesCJGS.CODE_ERROR_BANK.get(respuesta.getIdError()));
					
				}else if(prosaPagoVO.isProsaError()){
					respuesta.setStatus(1);
					respuesta.setIdError(Integer.parseInt(
							prosaPagoVO.getClaveRechazo()!= null && !"".equals(prosaPagoVO.getClaveRechazo())? prosaPagoVO.getClaveRechazo(): "1"));
					respuesta.setMensajeError(prosaPagoVO.getMsg());
				}
				
				updateBitacoras(respuesta);
				
			}
		}catch(Exception e){
			respuesta = new PaymentDataResponseVO();
			logger.error("An error occurred during the call to the bank: {}", e);
			respuesta.setStatus(200);
			respuesta.setMensajeError("An error occurred during the call to the bank.");
			updateBitacoras(respuesta);
		}
		return respuesta;
	}
	
	private void updateBitacoras(PaymentDataResponseVO paymentDataResp){
		try{
			logger.debug("Insert bitacora_reverso, IdBitacora: " + paymentDataResp.getIdBitacora());
			portalMapper.insertBitacoraReverso(paymentDataResp.getIdBitacora());
			
//			logger.debug("Update t_bitacora, IdBitacora: " + paymentDataResp.getIdBitacora());
//			portalMapper.updateTBitacora(paymentDataResp.getIdBitacora(), paymentDataResp.getStatus());
//			
//			logger.debug("Actualizando CJGS_detalle...");
//			portalMapper.updateCJGSDetalle(paymentDataResp);
		}catch(Exception e){
			logger.error("Error general al actualizar en bitacoras: {}", e);
		}
	}
	
	
	
}
