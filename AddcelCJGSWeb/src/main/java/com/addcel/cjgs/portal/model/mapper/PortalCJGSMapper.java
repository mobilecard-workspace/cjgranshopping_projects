package com.addcel.cjgs.portal.model.mapper;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.addcel.cjgs.portal.model.vo.PaymentData;
import com.addcel.cjgs.portal.model.vo.PaymentDataRequest;
import com.addcel.cjgs.portal.model.vo.SmtpVO;
import com.addcel.cjgs.portal.model.vo.UsuarioVO;

public interface PortalCJGSMapper {
	
	public List<UsuarioVO> validarUsuario(UsuarioVO usuario);
	
	public List<HashMap<String, Object>> validarPago(PaymentData paymentData);
	
	public int updateCJGSDetallePData(PaymentData paymentData);
	
	public List<PaymentData> buscarPagos(PaymentDataRequest paymentData);
	
	public List<HashMap<String, String>> buscarCompanias();
	
	public List<String> obtenDatosFrom(@Param("idCompany") int idCompany);
	
	public SmtpVO obtenDatosMail(@Param("idApp") int idApp,
								 @Param("idModulo") int idModulo,
								 @Param("idCliente") int idCliente);
	
	public int updateCJGSDetalle(
			@Param("idBitacora") int idBitacora,
			@Param("authorizationNumber") String authorizationNumber,
			@Param("transactionId") String transactionId,
			@Param("status") int status) ;
	
	public int updateTBitacora(@Param("idBitacora") int idBitacora,
								@Param("bitStatus") int bitStatus) ;
	
	public void insertBitacoraOperaciones(
			@Param("idBitacora") int idBitacora,
			@Param("idUsuario") int idUsuario,
			@Param("idOperacion") int idOperacion,
			@Param("status") int status) ;
	
	public void insertBitacoraReverso(@Param("idBitacora") int idBitacora) ;
	
	public int updateTransaccionAMEX(@Param("transaccionIdn") String transaccionIdn);
}
