<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" 		  prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<style type="text/css">
    root { 
        display: block;
    }

    html
    {
    	margin: 0;
        font-family: arial;
        font-size: 12px;
        font-weight: bold;
        color: white;
        background-color: #FFFFFF;
    }
    .ui-dialog-titlebar { 
        display: none;
    }
</style>

<link href="<c:url value="/resources/css/eshopper/jquery-ui.min.css"/>" rel="stylesheet">

<!-- JavaScript -->
<script src="<c:url value="/resources/js/jquery.ui.local.js"/>"></script>
<script src="<c:url value="/resources/js/eshopper/jquery-ui.min.js"/>"></script>
<script src="<c:url value="/resources/js/pago-3D-meses.js"/>"></script>
<section>
<div class="container">
	<div class="row">
		<div class="category-tab shop-details-tab"><!--category-tab-->
			<div class="col-sm-12">
				<ul class="nav nav-tabs">
						<li class="active" ><a href="#pagodirecto" data-toggle="tab">Portal 3D Secure CJ Gran Shopping</a></li>
				</ul>
			</div>
			<div class="tab-content">
				<div class="tab-pane fade active in" id="pagodirecto" >
					<div class="register-req">
						<p> Por favor proporcione la siguiente informaci&oacute;n de la Tarjeta de Cr&eacute;dito/Debito:</p>
					</div><!--/register-req-->
				
					<div class="shopper-informations">
						<div class="col-sm-8 col-sm-offset-2" >
							<div class="login-form">
								
			    				<div class="status alert alert-danger" <c:if test="${mensajePagoMeses == null}"> style="display: none" </c:if> >${mensajePagoMeses}</div>
								<form:form id="main-form" name="main-form" method="post" 
						    		autocomplete="off" modelAttribute="pagoForm"
						    		action="${pageContext.request.contextPath}/portal/procesa-pago-meses">
						    		<input type="hidden" id="modelo" name="modelo" />
									<input type="hidden" id="software" name="software" />
									<input type="hidden" id="operacion" name="operacion" value="3dmeses" />
									
				                    <p> Nombre:</p>
				                    <form:input path="nombre" maxlength="50" required="required" />
				                    <p> N&uacute;mero de Tarjeta:</p>
				                    <form:input path="tarjeta" id="tarjeta" maxlength="16" required="required" />
				                    <p> Tipo:</p>
			                        <form:select path="tipoTarjeta" id="tipoTarjeta" required="required" >
			                            <form:option value="1" >Visa</form:option>
								        <form:option value="2" >Master Card</form:option>
			                        </form:select>
				                    <p> Fecha de vencimiento:</p>
			                        <form:select path="mes" id="mes" required="required">
										<form:option value="01">1</form:option>
			                            <form:option value="02">2</form:option>
			                            <form:option value="03">3</form:option>
			                            <form:option value="04">4</form:option>
			                            <form:option value="05">5</form:option>
			                            <form:option value="06">6</form:option>
			                            <form:option value="07">7</form:option>
			                            <form:option value="08">8</form:option>
			                            <form:option value="09">9</form:option>
			                            <form:option value="10">10</form:option>
			                            <form:option value="11">11</form:option>
			                            <form:option value="12">12</form:option>
									</form:select >
									<form:select path="anio" id="anio" required="required">
										<%
			                                java.util.Calendar C= java.util.Calendar.getInstance();
			                                int anio=C.get(java.util.Calendar.YEAR);
			                                //out.println("<form:option selected>"+anio+"</form:option>");
			                                //anio++;
			                                for(int i=0;i<10;i++)
			                                {
			                                	out.println("<option value='" + anio + "'>"+ anio++ +"</option>");
			                                }
			                            %>         
									</form:select >
									<c:if test="${soliCompra.conMSI == true}">
				                    	<p> Diferir Pago a Meses:</p> 
										<form:select path="msi" id="msi" required="required">
											<form:option value="00">Ingrese Tarjeta...</form:option>
				                            <%-- <c:if test="${soliCompra.arrayMeses != null && soliCompra.arrayMeses != ''}">
				                            	<c:forTokens items="${soliCompra.arrayMeses}" delims="," var="mes">
					                            	<form:option value="${mes}">${mes}</form:option>
					                            </c:forTokens>
				                            </c:if> --%>
										</form:select >
									</c:if>
				                   	<p>C&oacute;digo de seguridad (CVV2/CVC2):</p>
				                  	<form:input path="cvv2" id="cvv2" maxlength="3" required="required" />
				                	</BR>
				                    <input type="submit" value="Realizar Pago" class="btn-style-pay"/>
								</br>
								</form:form>
			    			</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
