<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" 		  prefix="c" %>



<script src="<c:url value="/resources/js/jquery.ui.local.js"/>"></script>
<script src="<c:url value="/resources/js/registro.js"/>"></script>

	<form id="registroForm" name="registroForm" class="mobilecard topLabel page" 
		autocomplete="off" method="post" action="${pageContext.request.contextPath}/portal/registro-end/json">

		<header id="header" class="info">
			<label class="desc" style="font-size: 16px"> Registro Usuario MobileCard </label>
			<label class="desc" style="font-size: 12px"> Despues de su registro se enviara su Contraseña al correo registrado. </label>
			</BR>
			<label class="desc" style="font-size: 12px"> Si ya cuenta con un usuario, favor de ingresar: 
			<input id="loginButton" name="loginButton" type="submit" value="Inicio sessión"  style="width: 150px; right: auto;"/> </label>
		</header>
		
		<input type="hidden" id="modelo" name="modelo" />
		<input type="hidden" id="software" name="software" />
		<input type="hidden" id="json" name="json" value="${json}"/>
		<input type="hidden" id="comercio" name="comercio" value="${comercio}" />
		<ul>
			<li id="foli0" class="complex notranslate      ">
				<label class="desc" > Nombre de Usuario 
					<span class="req">*</span>
				</label>
				<div>
					<span class="left zip">
						<input id="login" name="login" />
					</span>
				</div>
			</li>

			<li id="foli1" class="complex notranslate      ">
				<label class="desc" > Numero Celular 
					<span class="req">*</span>
				</label>
				<div>
					<span class="left zip">
						<input id="telefono" name="telefono" maxlength="10"/>
						<label >Celular</label>
					</span>
					<span class="right country"> 
						<input id="telefonoCon" name="telefonoCon" maxlength="10"/> 
						<label >Confirmar Celuar</label>
					</span> 
				</div>
			</li>
			<li id="foli2" class="complex notranslate      ">
				<label class="desc" > Correo Electronico 
					<span class="req">*</span>
				</label>
				<div>
					<span class="left zip">
						<input id="email" name="email" maxlength="45"/>
						<label >Correo Electronico</label>
					</span>
					<span class="right country"> 
						<input id="emailCon" name="emailCon" maxlength="45"> 
						<label >Confirmar Correo Electronico</label>
					</span> 
				</div>
			</li>

			<li id="foli3" class="complex notranslate      ">
				<label class="desc"	> Nombre completo 
					<span class="req">*</span> 
				</label>
				<div>
					<span class="left zip"> 
						<input id="nombres" name="nombres" maxlength="45" /> 
						<label >Nombre (s)</label>
					</span> 
					<span class="right country"> 
						<input id="apellidoPrim" name="apellidoPrim" maxlength="45" /> 
						<label for="FieldappPaterno">Apellido Paterno</label>
					</span>
				</div>
				<div>
					<span class="left zip"> 
						<input id="apellidoSeg" name="apellidoSeg" maxlength="45"/> 
						<label for="FieldappMaterno">Apellido Materno</label>
					</span>
					<span class="right country"> 
					</span>
				</div>
			</li>
			
			<li id="foli6" class="complex notranslate      ">
				<label class="desc" > Tarjeta Credito 
					<span class="req">*</span>
				</label>
				<div>
					<span class="left zip"> 
						<input id="tarjeta" name="tarjeta" maxlength="16"/> 
						<label >Numero Tarjeta</label>
					</span> 
					<span class="right country"> 
						<input id="vigencia" name="vigencia"  readonly /> 
						<label >Vigencia (MM/YYYY)</label>
					</span>
				</div>
			</li>
			<li class="complex notranslate      ">
				<label class="desc" > Tipo de Tarjeta 
					<span class="req">*</span>
				</label>	
				<div>
					<span class="left complex"> 
						<select path="tipoTarjeta" id="tipoTarjeta"  >
			              <option value="0" >-- Seleccionar --</option>
						  <c:if test="${tproveedor.isVisa == 1}"> 
				              <option value="1" >Visa</option>
				              <option value="2" >Master Card</option>
			              </c:if>
			              <c:if test="${tproveedor.isAmex == 1}">
			              	<option value="3" >AMEX</option>
			              </c:if>
			          	</select>
					</span>
				</div>
			</li>
			<c:if test="${tproveedor.isAmex == 1}">
				<li id="liDomicilio" class="full complex notranslate      " style="display: none;">
					<label class="desc" > Domicilio de EDO. de Cuenta Tarjeta 
						<span class="req">*</span>
					</label>
					<div>
						<span class="full complex"> 
							<form:input id="domicilioAMEX" path="domicilioAMEX" maxlength="100"/> 
						</span>
					</div>
				</li>
				
				<li id="liCP" class="full complex notranslate      " style="display: none;">
					<label class="desc" > C.P. de EDO. de Cuenta Tarjeta 
						<span class="req">*</span>
					</label>
					<div>
						<span class="full complex"> 
							<form:input id="cpAMEX" path="cpAMEX" maxlength="10"/> 
						</span> 
					</div>
				</li>
			</c:if>
			<li id="foli17" class="complex notranslate      ">
				<label class="desc" > Terminos y Condiciones
					<span class="req">*</span>
				</label>
				<c:if test="${terminosCondiciones != null}">
					<div id="terminosDiv" align="left" class="buttons "  style="display: none;">
						${terminosCondiciones.label}
					</div>
					<input type="checkbox" id="terminos" name="terminos" value="${terminosCondiciones.value}" />
					<input id="terminosButton" name="terminosButton" type="submit" value="Ver Terminos y Condiciones" style="width: 200px"/>
				</c:if>
			</li>
			<li id="foli17" class="complex notranslate      ">
				<div align="center" class="buttons "  style="height: 60px;">
					<input id="saveButton" name="saveButton" type="submit" value="Registro" style="width: 150px"/>
					<!-- <input id="saveButton" name="saveButton" type="submit" value="Limpiar" style="width: 150px"/> -->
					</BR>
				</div>
			</li>
		</ul>
		
		
		
	</form>
	
	
	<%-- <form id="loginForm" name="loginForm" action="${pageContext.request.contextPath}/login">
		<input type="hidden" id="json" name="json" value="${json}" />
		<input type="hidden" id="comercio" name="comercio" value="${comercio}" /> 
		<input type="hidden" id="url" name="url" value="${url}" /> 
	</form> --%>
	
	<form id="loginForm" name="loginForm"  method="post" action="${pageContext.request.contextPath}/portal/pago-inicio">
		<input type="hidden" id="uparam" name="uparam" value="${uparam}"/>
		<input type="hidden" id="ucom" name="ucom" value="${ucom}" />
	</form>
	<!--container-->

