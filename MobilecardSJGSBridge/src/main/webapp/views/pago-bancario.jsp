<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" 		  prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<!-- JavaScript -->
<script src="<c:url value="/resources/js/jquery.ui.local.js"/>"></script>
<script src="<c:url value="/resources/js/pago-bancario.js"/>"></script>

<script>
var usuario = new Array();
usuario.telefono = '${usuario.usrTelefono}';
usuario.email = '${usuario.email}';
usuario.nombres = '${usuario.usrNombre}';
usuario.apellidoPrim = '${usuario.usrApellido}';
usuario.apellidoSeg = '${usuario.usrMaterno}';
</script>

<div id="blockPagoDiv" class="blockdiv" style="display: none;">
		<input id="unblockButton" name="unblockButton" type="submit" value="Pago con Usuario MobileCard" style="width: 250px; height: 50px;"/> </label> 
    </div>
    
    <div id="blockPagoDirectoDiv" class="blockdiv" style="display: none;">
		<input id="unblockDirectoButton" name="unblockDirectoButton" type="submit" value="Pago Directo" style="width: 250px; height: 50px;"/> </label> 
    </div>
	
	<section id="cart_items">
		<div class="container">
			<div class="breadcrumbs">
				<ol class="breadcrumb">
				  <li><a href="#">Inicio </a></li>
				  <li class="active">Resumen del Pago</li>
				</ol>
			</div>
			<div class=" cart_info">
				<table class="table ">
					<thead>
						<tr class="cart_menu">
							<th class="description">Descripcion</th>
							<th class="total">Importe</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td class="cart_description">
								<p>${soliCompra.descripcion}</p>
							</td>
							
							<td class="cart_total">
								<p class="cart_total_price">$ ${soliCompra.importeForm}</p>
							</td>
						</tr>
						<c:if test="${soliCompra.comision > 0.0}">
							<tr>
								<td class="cart_description">
									<p>Comicion</p>
								</td>
								<td class="cart_total">
									<p class="cart_total_price"> $ ${soliCompra.comisionForm}</p>
								</td>
							</tr>
						</c:if>
						<tr>
							<td class="cart_description">
								<p><B>Importe Total</B></p>
							</td>
							<td class="cart_total">
								<p class="cart_total_price">$ ${soliCompra.totalPagoForm} MXN</p>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</section> <!--/#cart_items-->
	<section>
		<div class="container">
			<div class="row">
				<div class="category-tab shop-details-tab"><!--category-tab-->
					<div class="col-sm-12">
						<ul class="nav nav-tabs">
							<c:if test="${soliCompra.modoPago != 3}"> 
								<li <c:if test="${mensajeCambioPass == null && mensajeCambioPassOK == null && mensajeReset == null && mensajeResetOK == null}"> class="active" </c:if> ><a href="#pagodirecto" data-toggle="tab">Pago con Usuario</a></li>
								<li <c:if test="${mensajeCambioPass != null || mensajeCambioPassOK != null}"> class="active" </c:if> ><a href="#usuarios" data-toggle="tab">Cambiar Contraseña</a></li>
								<li <c:if test="${mensajePagoRef != null}"> class="active" </c:if> ><a href="#nuevo" data-toggle="tab">Cambiar Datos</a></li>
							</c:if>
							<c:if test="${soliCompra.modoPago == 3}">
								<li class="active"><a href="#referen" data-toggle="tab">Pago por Referencia</a></li>
							</c:if> 
						</ul>
					</div>
					<div class="tab-content">
					<c:if test="${soliCompra.modoPago != 3}"> 
						<div class="tab-pane fade  <c:if test="${mensajeCambioPass == null && mensajeCambioPassOK == null && mensajeReset == null && mensajeResetOK == null}"> active in </c:if>" id="pagodirecto" >
							<div class="register-req">
								<h4>Bienvenido (a):  ${usuario.usrNombre}</h4>
								<p>Favor de ingresar los siguientes datos para completar su Pago:</p>
							</div>
						
							<div class="shopper-informations">
								<div class="col-sm-8 col-sm-offset-2" >
									<div class="login-form total_area ">
										<p>Informaci&oacute;n de la Tarjeta de 
											<c:if test="${soliCompra.modoPago == 1}"> Cr&eacute;dito</c:if><c:if test="${soliCompra.modoPago == 2}"> Debito</c:if>. </p>
					    				
					    				<div class="status alert alert-danger" <c:if test="${mensajePago == null}"> style="display: none" </c:if> >${mensajePago}</div>
					    				
								    	<form:form id="main-form" name="main-form" method="post" 
								    		autocomplete="off" modelAttribute="pagoUserForm"
								    		action="${pageContext.request.contextPath}/portal/procesa-pago">
								    		<input type="hidden" id="idUsuario" name="idUsuario" value="${usuario.idUsuario}"/>
								    		<input type="hidden" id="modelo" name="modelo" />
											<input type="hidden" id="software" name="software" />
											<ul style="background-color: #FFFFFF">
												<li>Nombre: <span>${usuario.usrNombre} ${usuario.usrApellido}</span></li>
												<li>Email <span>${usuario.email}</span></li>
												<li>Tarjeta <span>${usuario.usrTdcNumeroForm}</span></li>
												<li>Tipo Tarjeta <span>
													<c:if test="${usuario.idTipoTarjeta == 1}"> VISA </c:if>
													<c:if test="${usuario.idTipoTarjeta == 2}"> AMERICAN EXPRES </c:if>
													<c:if test="${usuario.idTipoTarjeta == 3}"> AMEX </c:if>
													</span></li>
												<li>Vigencia <span>${usuario.usrTdcVigencia}</span></li>
											</ul>
							                
											<%-- <c:if test="${tproveedor.isAmex == 1}">
												<div id="dataAmex" style="display: none;">
													<form:input path="domicilio"  placeholder="Dom. de EDO. Cuenta Tarjeta"  maxlength="100" />
													<form:input path="cp"  placeholder="C.P. de EDO. Cuenta Tarjeta"  maxlength="100" />
												</div> 
											</c:if> --%>
											<c:if test="${soliCompra.conMSI == true}"> 
												<form:select path="msi" required="required">
													<form:option value="00">-- Meses sin Int --</form:option>
													<form:option value="01">1</form:option>
						                            <form:option value="03">3</form:option>
						                            <form:option value="06">6</form:option>
						                            <form:option value="09">9</form:option>
						                            <form:option value="12">12</form:option>
												</form:select >
											</c:if>
							                <form:input path="cvv2"  required="required" placeholder="Codigo Seguridad" maxlength="3" />
							                
							                <button type="submit" class="btn btn-default">Realizar Pago</button>
								        </form:form>
					    			</div>
								</div>
							</div>
						</div>
						
						<div class="tab-pane fade <c:if test="${mensajeCambioPass != null || mensajeCambioPassOK != null}"> active in </c:if>" id="usuarios" >
							<div class="register-req">
								<p>Estimado <b>${usuario.usrNombre}</b> para realizar el cambio de contraseña favor de ingresar la siguiente informacion.</p>
							</div>
							<div class="row">
								<div class="col-sm-8 col-sm-offset-2">
									<div class="login-form"><!--login form-->
										<div class="status alert alert-danger" <c:if test="${mensajeCambioPass == null}"> style="display: none" </c:if> >${mensajeCambioPass}</div>
										<div class="status alert alert-info" <c:if test="${mensajeCambioPassOK == null}"> style="display: none" </c:if> >${mensajeCambioPassOK}</div>
										<form:form id="cambPass-form" name="cambPass-form" method="post" 
								    		autocomplete="off" modelAttribute="cambPass" action="${pageContext.request.contextPath}/portal/pago-cambio-password">
								    		<input type="hidden" id="idUsuario" name="idUsuario" value="${usuario.idUsuario}"/>
											<form:input path="login" placeholder="Usuario" />
											<form:input type="password" path="password" placeholder="Contraseña" />
											<form:input type="password" path="newPassword" placeholder="Nueva Contraseña" />
											<form:input type="password" path="newPasswordCon" placeholder="Confirmar Nueva Contraseña" />
											<button type="submit" class="btn btn-default">Cambiar Contraseña</button>
										</form:form>
									</div><!--/login form-->
								</div>
							</div>
						</div>
						
						<div class="tab-pane fade" id="nuevo" >
							<div class="register-req">
								<p>Favor de ingresar los siguientes datos para crear un usuario Mobilecard:</p>
							</div>
						
							<div class="shopper-informations">
								<div class="col-sm-8 col-sm-offset-2" >
									<div class="login-form">
					    				<div class="status alert alert-danger" <c:if test="${mensajePago == null}"> style="display: none" </c:if> >${mensajePago}</div>
					    				
								    	<%-- <form:form id="main-form" name="main-form" method="post">
								    		<p>Informaci&oacute;n Personal.</p>
								    		<form:input path="usuario" required="required" placeholder="Usuario" />
							                <form:input path="name"  required="required" placeholder="Nombre" />
							                <form:input type="email" path="email"  required="required" placeholder="Correo Electronico" />
							                
								            <p>Informaci&oacute;n de la Tarjeta de Cr&eacute;dito.</p>

							                <form:input path="tarjeta"  required="required" placeholder="Numero Tarjeta" />
							            	<form:select  path="tipoTarjeta" required="required">
												<form:option value="0" >-- Tipo Tarjeta --</form:option>
												<c:if test="${tproveedor.isVisa == 1}"> 
										        	<form:option value="1" >Visa</form:option>
										        	<form:option value="2" >Master Card</form:option>
									            </c:if>
									            <c:if test="${tproveedor.isAmex == 1}">
									            	<form:option value="3" >AMEX</form:option>
									            </c:if>
											</form:select >
							            	<form:select   path="mes" required="required">
												<form:option value="">-- Vence Mes --</form:option>
												<form:option value="01">1</form:option>
					                            <form:option value="02">2</form:option>
					                            <form:option value="03">3</form:option>
					                            <form:option value="04">4</form:option>
					                            <form:option value="05">5</form:option>
					                            <form:option value="06">6</form:option>
					                            <form:option value="07">7</form:option>
					                            <form:option value="08">8</form:option>
					                            <form:option value="09">9</form:option>
					                            <form:option value="10">10</form:option>
					                            <form:option value="11">11</form:option>
					                            <form:option value="12">12</form:option>
											</form:select >
											<form:select  path="anio"  required="required">
												<form:option selected="selected" value="">-- Vence Año --</form:option>
												<%
					                                java.util.Calendar C2= java.util.Calendar.getInstance();
					                                int anio2=C2.get(java.util.Calendar.YEAR);
					                                //out.println("<form:option selected>"+anio+"</form:option>");
					                                //anio++;
					                                for(int i=0;i<10;i++)
					                                {
					                                	out.println("<form:option >"+ anio2++ +"</form:option>");
					                                }
					                            %>         
											</form:select >
							                <form:input path="email"  required="required" placeholder="Codigo Seguridad" />
							                <button type="submit" class="btn btn-default">Realizar Pago</button>
								     </form:form>--%> 
					    			</div>
								</div>
							</div>
						</div>
					</c:if>
					<c:if test="${soliCompra.modoPago == 3}">
						<div class="tab-pane fade  <c:if test="${mensajePagoRef != null}"> active in </c:if>" id="referen" >
							<div class="row">
								<div class="col-sm-8 col-sm-offset-2">
									<div class="total_area login-form">
										<div class="status alert alert-danger" <c:if test="${mensajePagoRef == null}"> style="display: none" </c:if> >${mensajePagoRef}</div>
										<form:form id="main-form" name="main-form" method="post" 
								    		autocomplete="off" modelAttribute="pagoForm"
								    		action="${pageContext.request.contextPath}/portal/procesa-pago-directo">
										<ul style="background-color: #FFFFFF">
											<li>Referencia CJGS <span>${soliCompra.referenciaCJGS}</span></li>
											<li>Fecha Expiracion <span>${soliCompra.vigencia}</span></li>
											<li>Digito Verificador <span>${soliCompra.digitoVer}</span></li>
											<li>Email <span>${soliCompra.email}</span></li>
										</ul>
										<button type="submit" class="btn btn-default">Generar Referencia</button>
										</form:form>
									</div>
								</div>
							</div>
						</div>
					</c:if> 
					</div>
				</div>
			</div><!--/category-tab-->
		</div>
	</section>

	
	<!--container Cambio Datos-->
	<div id="containerCambioDatos" class="ltr" style="display: none;">


		<form id="cambioDatosForm" name="cambioDatosForm" class="mobilecard topLabel page"
			autocomplete="off" 
			action="${pageContext.request.contextPath}/cambioDatos/json">

			<header id="header" class="info">
				<label class="desc" style="font-size: 12px"> Favor de cambiar los datos que desea actualizar.. </label>
				<label class="desc" style="font-size: 12px"></label>
				
			</header>
			
			<input type="hidden" id="idUsuario" name="idUsuario" value="${usuario.idUsuario}"/>
			
			<ul>
				<li id="foliError" class="complex notranslate      " >
					<label id="lblErrorCamDato" class="error" > 
					</label>
				</li>
				<li class="complex notranslate      ">
					<label class="desc" > Usuario 
						<span class="req">*</span>
					</label>
					<div>
						<span class="left zip">
							<label > ${usuario.usrLogin} </label>
						</span>
					</div>
				</li>

				<li class="complex notranslate      ">
					<label class="desc" > Numero Telefono 
						<span class="req">*</span>
					</label>
					<div>
						<span class="left zip">
							<input id="telefono" name="telefono" />
							<label >Telefono</label>
						</span>
 						<span class="right country"> 
							<input id="telefonoCon" name="telefonoCon" /> 
							<label >Confirmar Telefono</label>
						</span>  
					</div>
				</li>
				<li id="foli2" class="complex notranslate      ">
					<label class="desc" > Email 
						<span class="req">*</span>
					</label>
					<div>
						<span class="left zip">
							<input id="email" name="email" />
							<label >Email</label>
						</span>
						<span class="right country"> 
							<input id="emailCon" name="emailCon" > 
							<label >Confirmar Email</label>
						</span>
					</div>
				</li>

				<li id="foli3" class="complex notranslate      ">
					<label class="desc"	id="title15" for="Field15"> Nombre completo <span class="req">*</span> </label>
					<div>
						<span class="left zip"> 
							<input id="nombres" name="nombres" /> 
							<label >Nombre (s)</label>
						</span> 
						<span class="right country"> 
							<input id="apellidoPrim" name="apellidoPrim" /> 
							<label >Apellido Paterno</label>
						</span>
					</div>
					<div>
						<span class="left zip"> 
							<input id="apellidoSeg" name="apellidoSeg" /> 
							<label >Apellido Materno</label>
						</span>
					</div>
				</li>
			</ul>
		</form>
		<!-- <div align="center" class="buttons "  style="height: 60px;">
			<input id="cambioDatosSubmit" name="cambioDatosSubmit" type="submit" value="Actualizar Datos" style="width: 200px"/>
			<input id="cambioDatosCanSubmit" name="cambioDatosCanSubmit" type="submit" value="Cancelar" style="width: 200px" />
		</div> -->
	</div>
	<!--container Cambio Datos-->
	
	<!--container Cambio Password-->	
 	<div id="containerCambioPassword" class="ltr" style="display: none;">

		<form id="cambioPasswordForm" name="cambioPasswordForm" class="mobilecard topLabel page"
			autocomplete="off" method="post" 
			action="${pageContext.request.contextPath}/cambioPassword/json">

			<header id="header" class="info" style="">
				<label class="desc" style="font-size:12px"> </label>
				<label class="desc" style="font-size:12px"> Para el realizar el cambio de su password, for de ingresar la siguiente informacion: </label>
				
			</header>
			
			<input type="hidden" id="idUsuario" name="idUsuario" value="${usuario.idUsuario}"/>
			<input type="hidden" id="login" name="login" value="${usuario.usrLogin}"/>

			<ul>
				<li id="foliError" class="complex notranslate      " >
					<label id="lblErrorCamPass" class="error" > 
					</label>
				</li>
				
				<li class="complex notranslate      ">
					<label class="desc"  > Password Actual
						<span class="req">*</span>
					</label>
					<div>
						<span class="left zip">
							<input type="password" id="passwordCam" name="passwordCam" maxlength="16" />
						</span>
					</div>
				</li>
				
				<li class="complex notranslate      ">
					<label class="desc"  > Nuevo Password 
						<span class="req">*</span>
					</label>
					<div>
						<span class="left zip">
							<input type="password" id="newPassword" name="newPassword" maxlength="16" />
						</span>
					</div>
				</li>
				
				<li class="complex notranslate      ">
					<label class="desc"  > Confirmar Nuevo Password
						<span class="req">*</span>
					</label>
					<div>
						<span class="left zip">
							<input type="password" id="newPasswordCon" name="newPasswordCon" maxlength="16" />
						</span>
					</div>
				</li>
			</ul>
		</form>
		<!-- <div align="center" class="buttons "  style="height: 0px;">
			<input id="cambioPasswordSubmit" name="cambioPasswordSubmit" type="submit" value="Cambiar Password" style="width: 200px" />
			<input id="cambioPasswordCanSubmit" name="cambioPasswordCanSubmit" type="submit" value="Cancelar" style="width: 200px" />
		</div> -->
	</div>
	<!--container Cambio Password-->
	
	<!--container Cambio Tarjeta-->	
	<div id="containerCambioTarjeta" class="ltr" style="display: none;">

		<form id="cambioTarjetaForm" name="cambioTarjetaForm" class="mobilecard topLabel page"
			autocomplete="off" method="post" 
			action="${pageContext.request.contextPath}/cambioTarjeta/json">

			<header id="header" class="info" style="">
				<label class="desc" style="font-size:12px"> </label>
				<label class="desc" style="font-size:12px"> Para el realizar el cambio de su Tarjeta, for de ingresar la siguiente informacion: </label>
				
			</header>
			
			<input type="hidden" id="idUsuario" name="idUsuario" value="${usuario.idUsuario}"/>
			<input type="hidden" id="login" name="login" value="${usuario.usrLogin}"/>

			<ul>
				<li id="foliError" class="complex notranslate      " >
					<label id="lblErrorCamTarj" class="error" > 
					</label>
				</li>
			
				<li class="complex notranslate      ">
					<label class="desc"  > Tarjeta Actual 
					</label>
					<div>
						<span class="left zip">
							<input id="tarjetaAct" name="tarjetaAct" readonly="readonly" value="${usuario.usrTdcNumero}"/>
						</span>
					</div>
				</li>
			
				<li class="complex notranslate      ">
					<label class="desc"  > Nueva Tarjeta 
						<span class="req">*</span>
					</label>
					<div>
						<span class="left zip">
							<input id="newTarjeta" name="newTarjeta" maxlength="16" />
						</span>
					</div>
				</li>
				
				<li class="complex notranslate      ">
					<label class="desc" > Confirmar Nueva Tarjeta
						<span class="req">*</span>
					</label>
					<div>
						<span class="left zip">
							<input id="newTarjetaCon" name="newTarjetaCon" maxlength="16" />
						</span>
					</div>
				</li>
				<li id="foli6" class="complex notranslate      ">
					<label class="desc" > Vigencia (MM/YYYY)
						<span id="req_2" class="req">*</span>
					</label>
					<div>
						<span class="left country"> 
							<input id="vigencia" name="vigencia"  readonly /> 
						</span>
					</div>
				</li>
				
				<li class="complex notranslate      ">
					<label class="desc"  > Password
						<span class="req">*</span>
					</label>
					<div>
						<span class="left zip">
							<input type="password" id="passwordTarj" name="passwordTarj" maxlength="16" />
						</span>
					</div>
				</li>
				<!-- <li class="complex notranslate      ">
					<div align="center" class="buttons "  style="height: 0px;">
						<input id="cambioTarjetaSubmit" name="cambioTarjetaSubmit" type="submit" value="Cambiar Tarjeta" style="width: 200px" />
						<input id="cambioTarjetaCanSubmit" name="cambioTarjetaCanSubmit" type="submit" value="Cancelar" style="width: 200px" />
					</div>
				</li> -->
			</ul>
		</form>
		

	</div>
	<!--container Cambio Tarjeta-->
	
	<form id="redireccionForm" name="redireccionForm"  method="post" action="${pageContext.request.contextPath}/${url}">
		<input type="hidden" id="json" name="json" value="${json}"/>
		<input type="hidden" id="comercio" name="comercio" value="${comercio}" /> 
		<input type="hidden" id="url" name="url" value="${url}" /> 
	</form>
	
