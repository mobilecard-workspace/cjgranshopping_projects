<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="HandheldFriendly" content="true"/>
    <meta name="viewport" content="width=device-width"/>

   	<div id="data" data-role="page">
   		<%-- <form method="post" name="form1" action="https://www.procom.prosa.com.mx/eMerchant/7627488_ADCL_Diestel.jsp" >  --%>
   		<form method="post" name="form1" action="https://www.procom.prosa.com.mx/eMerchant/7783587_add_tel_cj.jsp" >
        <%-- <form method="post" name="form1" action="${pageContext.request.contextPath}/portal/pagina-prosa"> --%>
            <input type="hidden" name="total" value="${procom.total}">
            <input type="hidden" name="currency" value="${procom.currency}">
            <input type="hidden" name="address" value="${procom.address}">
            <input type="hidden" name="order_id" value="${procom.orderId}">
            <input type="hidden" name="merchant" value="${procom.merchant}">
            <input type="hidden" name="store" value="${procom.store}">
            <input type="hidden" name="term" value="${procom.term}">
            <input type="hidden" name="digest" value="${procom.digest}">
            <input type="hidden" name="return_target" value="">
            <input type="hidden" name="urlBack" value="https://www.mobilecard.mx:8443/MobilecardSJGSBridge/portal/comercio-con">       
        </form>
	</div>
       <section id="cart_items">
		<div class="container">
			<div class=" cart_info">
				<table class="table ">
					<thead>
						<tr class="cart_menu">
							<th class="description">Envio de informaci&oacute;n.</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td class="cart_description">
								<p><br>Se est&aacute; enviando la informaci&oacute;n al Banco para su procesamiento, favor de esperar.<br><br></p>
							</td>
						</tr>
						<tr>
							<td class="cart_description">
								<p><img src="<c:url value="/resources/images/loading2.gif"/>" /></p>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</section>

	<script type="text/javascript">
        function sendform() {
            setTimeout (function () {
            	document.form1.submit();
            }, 1000);
        }
        sendform();            
	</script>
    
    
