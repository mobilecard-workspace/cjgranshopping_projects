<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
    
<form method="post" name="form1" autocomplete="off" action="https://eps.banorte.com/secure3d/Solucion3DSecure.htm">
	<input type="hidden" name="Reference3D" value="${prosa.Reference3D}" />
	<input type="hidden" name="MerchantId" value="${prosa.MerchantId}" />
	<input type="hidden" name="MerchantName" value="MOBILECARD" />
	<input type="hidden" name="MerchantCity" value="Mexico" />
	<input type="hidden" name="Cert3D" value="03" />
	<input type="hidden" name="ForwardPath" value="${prosa.ForwardPath}" />
	<input type="hidden" name="Expires" value="${prosa.Expires}" />
	<INPUT TYPE="hidden" NAME="Total" VALUE="${prosa.Total}"> 
	<INPUT TYPE="hidden" NAME="Card" VALUE="${prosa.Card}"> 
	<INPUT TYPE="hidden" NAME="CardType" VALUE="${prosa.CardType}">
</form>
<section id="cart_items">
	<div class="container">
		<div class=" cart_info">
			<table class="table ">
				<thead>
					<tr class="cart_menu">
						<th class="description">Envio de informaci&oacute;n.</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td class="cart_description">
							<p><br>Se est&aacute; enviando la informaci&oacute;n al Banco para validacion 3D Secure, favor de esperar.<br><br></p>
						</td>
						<tr>
							<td align="center">
								<p><img src="<c:url value="/resources/images/loading2.gif"/>" /></p>
							</td>
						</tr>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</section>
<script type="text/javascript">
	function sendform() {
	    setTimeout (function () {
	    	document.form1.submit();
	    }, 1000);
	}
	
	sendform();            
</script>

