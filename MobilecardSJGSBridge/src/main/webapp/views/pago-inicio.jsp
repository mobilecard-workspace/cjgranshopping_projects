<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" 		  prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<link href="<c:url value="/resources/css/eshopper/jquery-ui.min.css"/>" rel="stylesheet">

<!-- JavaScript -->
<script src="<c:url value="/resources/js/jquery.ui.local.js"/>"></script>
<script src="<c:url value="/resources/js/eshopper/jquery-ui.min.js"/>"></script>
<%-- <script src="<c:url value="/resources/js/pago-inicio.js"/>"></script> --%>

<style type="text/css">
	.ui-dialog-titlebar { 
        display: none;
    }
    <c:if test="${soliCompra.canal == 3}">
		select{
			padding: 10px 5px;
			margin-bottom: 10px;
			color: #696763;
			width: 100%;
			resize: none;
		}    
    </c:if>
</style>

<script type="text/javascript">
$(document).ready(function(){
	var dataSend = "";
	
	$('#modelo').val(jQuery.browser.name);
	$('#software').val(jQuery.browser.version);

	$( "#cargando" ).dialog({
		dialogClass: "no-close",
		modal: true,
		resizable: false,
		draggable: false,
		closeOnEscape: false,
		autoOpen: false
	});

	$("#cargando" ).dialog( "close" );
//	$("#cargando" ).dialog( "open" );

	$('#main-form').submit(function(){
		//$("#pagoBTN").html("Realizar Pago");
		$("#pagoBTN").attr("disabled", true);
		$("#cargando" ).dialog( "open" );
		<c:if test="${soliCompra.canal == 3 && tproveedor.isAmex == 1 }">
			if($("#tipoTarjeta").val() == 3){
				$("#msi").val(0);
			}
		</c:if>
	});
	
	<c:if test="${soliCompra.canal == 3 && tproveedor.isAmex == 1 }">
		isAMEX();
	
		function isAMEX(){
			if($("#tipoTarjeta").val() == 3){
				$("#dataAmex").show();
				$("#dataVISA").hide();
				
			}else{
				$("#dataAmex").hide();
				$("#dataVISA").show();

				if( $("#tarjeta" ).val() != null && $( "#tarjeta" ).val() != '' && 
						$( "#tarjeta" ).val().length > 6 && dataSend != $( "#tarjeta" ).val().substring(0, 6)){
					callMeses();
				}
			}
		}
		
		$("#tipoTarjeta").change(function(){
	        isAMEX();
		});
	</c:if>
	<c:if test="${soliCompra.canal != 3 && tproveedor.isAmex == 1 }">
		isAMEX();
	
		function isAMEX(){
			if($("#tipoTarjeta").val() == 3){
				$("#dataVISA").hide();
				$("#dataAmex").show();
				$("#pagoBTN").html("Realizar Pago");
	
				$("#tarjeta").attr('required', 'required');
				$("#mes").attr('required', 'required');
				$("#anio").attr('required', 'required');
				$("#cvv2").attr('required', 'required');
				
			}else{
				$("#dataVISA").show();
				$("#dataAmex").hide();
				$("#pagoBTN").html("Continuar");
	
				$("#tarjeta").removeAttr('required');
				$("#mes").removeAttr('required');
				$("#anio").removeAttr('required');
				$("#cvv2").removeAttr('required');

				if( $("#tarjeta" ).val() != null && $( "#tarjeta" ).val() != '' && 
						$( "#tarjeta" ).val().length > 6 && dataSend != $( "#tarjeta" ).val().substring(0, 6)){
					callMeses();
				}
			}
		}
		
		$("#tipoTarjeta").change(function(){
	        isAMEX();
		});
	</c:if>

	$(document).ajaxStart(function() {
		$('input[type="submit"]').attr('disabled','disabled');
		$( "#cargando" ).dialog( "open" );
	});
	
	$(document).ajaxStop(function() {
		$( "#cargando" ).dialog( "close" );
		$('input[type="submit"]').removeAttr('disabled');
	});
	

	$("#tarjeta").blur(callMeses);  

	function callMeses(){
		if( $("#tarjeta" ).val() != null && $( "#tarjeta" ).val() != '' && 
				$( "#tarjeta" ).val().length > 6 && dataSend != $( "#tarjeta" ).val().substring(0, 6)){
			$('input[type="submit"]').attr('disabled','disabled');

			/*var dataJson = {
	    		"tarjeta" : $( "#tarjeta" ).val()
	    	};*/
	    	$.ajax({
				 url: '/MobilecardSJGSBridge/portal/consultaMSI/data',
				 type: 'POST',
				 /*data: JSON.stringify(dataJson),*/
				 data: '{"tarjeta":"'+ $( "#tarjeta" ).val() + '"}',
				 dataType: 'json',
				 contentType: "application/json; charset=utf-8",
				 mimeType: 'application/json; charset=utf-8',
				 async: false,
	             cache: false,    //This will force requested pages not to be cached by the browser          
	             processData:false, //To avoid making query String instead of JSON
	             timeout: 10000,
				 statusCode: {
					 404: function() {
					 alert( "Pagina no encontrada." );
					 }
				 },
				 success: function (data) {
					dataSend = $( "#tarjeta" ).val().substring(0, 6);
					$('input[type="submit"]').removeAttr('disabled');
				 	
//				 	alert(data);
			 		if(data != null){
			 			$('#msi').empty();
			 			/*$('select option').remove();*/
			 	        //Inserta los valores recuperados en la lista desplegable
			 	        $.each(data, function(i, item) {
			 	         	$('#msi').append('<option value='+item.value+'>'+item.label+'</option>');
			 	        });
			 	        //Actualiza la lista desplegable
			 	        //$('#msi').selectmenu("refresh", true);
			 	    }
				 },
				 error: function (response, textStatus, errorThrown) {
				 	alert('An error has occured!! \nresponse: ' + response +
						 	'\ntextStatus: ' +  textStatus +
						 	'\nerrorThrown: ' + errorThrown);
				 }
			}); 
		}
	};
});
</script>
	<section id="cart_items">
		<div class="container">
			<div class="breadcrumbs">
				<ol class="breadcrumb">
				  <li><a href="#">Inicio </a></li>
				  <li class="active">Resumen del Pago</li>
				</ol>
			</div>
			<div class=" cart_info">
				<table class="table ">
					<thead>
						<tr class="cart_menu">
							<th class="description">Descripcion</th>
							<th class="total">Importe</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td class="cart_description">
								<p>${soliCompra.descripcion}</p>
							</td>
							
							<td class="cart_total">
								<p class="cart_total_price">$ ${soliCompra.importeForm}</p>
							</td>
						</tr>
						<c:if test="${soliCompra.comision > 0.0}">
							<tr>
								<td class="cart_description">
									<p>Comicion</p>
								</td>
								<td class="cart_total">
									<p class="cart_total_price"> $ ${soliCompra.comisionForm}</p>
								</td>
							</tr>
						</c:if>
						<tr>
							<td class="cart_description">
								<p><B>Importe Total</B></p>
							</td>
							<td class="cart_total">
								<p class="cart_total_price">$ ${soliCompra.totalPagoForm} MXN</p>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</section> <!--/#cart_items-->
	<section>
		<div class="container">
			<div class="row">
				<div class="category-tab shop-details-tab"><!--category-tab-->
					<div class="col-sm-12">
						<ul class="nav nav-tabs">
							<c:if test="${soliCompra.modoPago != 3}"> 
								<li <c:if test="${mensajeLogin == null && mensajePagoRef == null && mensajeReset == null && mensajeResetOK == null}"> class="active" </c:if> ><a href="#pagodirecto" data-toggle="tab">Pago Directo</a></li>
								<li <c:if test="${mensajeLogin != null || mensajeReset != null || mensajeResetOK != null}"> class="active" </c:if> ><a href="#usuarios" data-toggle="tab">Usuario MC</a></li>
								<li <c:if test="${mensajePagoRef != null}"> class="active" </c:if> ><a href="#nuevo" data-toggle="tab">Registro Usuario MC</a></li>
							</c:if>
							<c:if test="${soliCompra.modoPago == 3}">
								<li class="active"><a href="#referen" data-toggle="tab">Pago por Referencia</a></li>
							</c:if> 
						</ul>
					</div>
					<div class="tab-content">
					<c:if test="${soliCompra.modoPago != 3}"> 
						<div class="tab-pane fade  <c:if test="${mensajeLogin == null && mensajePagoRef == null && mensajeReset == null && mensajeResetOK == null}"> active in </c:if>" id="pagodirecto" >
							<div class="register-req">
								<p>Favor de ingresar los siguientes datos para completar su Pago:</p>
							</div>
						
							<div class="shopper-informations">
								<div class="col-sm-8 col-sm-offset-2" >
									<div class="login-form">
										<p>Informaci&oacute;n de la Tarjeta de 
											<c:if test="${soliCompra.modoPago == 1}"> Cr&eacute;dito</c:if><c:if test="${soliCompra.modoPago == 2}"> Debito</c:if>. </p>
					    				
					    				<div class="status alert alert-danger" <c:if test="${mensajePagoDir == null}"> style="display: none" </c:if> >${mensajePagoDir}</div>
					    				
								    	<form:form id="main-form" name="main-form" method="post" 
								    		autocomplete="off" modelAttribute="pagoForm"
								    		action="${pageContext.request.contextPath}/portal/procesa-pago-directo">
								    		<input type="hidden" id="modelo" name="modelo" />
											<input type="hidden" id="software" name="software" />
							             
							             <c:if test="${soliCompra.canal != 3}">
							             	<c:if test="${soliCompra.conMSI == true}">
								             	<span>
													<input type="checkbox" class="checkbox" name="conMSI" id="conMSI" value="true"/> 
													<strong>Con Meses Sin Intereses.</strong>
												</span>
											</c:if>
								              <form:input type="email" path="email"  required="required" placeholder="Correo Electronico"  maxlength="50" />
								              
								                <form:select path="tipoTarjeta" id="tipoTarjeta" required="required" >
												  <c:if test="${tproveedor.isVisa == 1}"> 
										              <form:option value="1" >Visa / Master Card</form:option>
									              </c:if>
									              <c:if test="${tproveedor.isAmex == 1}">
									              	<form:option value="3" >AMEX</form:option>
									              </c:if>
									          	</form:select>
								                
								            	<c:if test="${tproveedor.isAmex == 1}">
													<div id="dataAmex" style="display: none;">
													
													<form:input path="nombre" placeholder="Nombre" maxlength="50" /> 
									                <form:input path="tarjeta" id="tarjeta"  placeholder="Numero Tarjeta" maxlength="16" />
									            	
									            	<form:select path="mes" id="mes" >
														<form:option value="">-- Vence Mes --</form:option>
														<form:option value="01">1</form:option>
							                            <form:option value="02">2</form:option>
							                            <form:option value="03">3</form:option>
							                            <form:option value="04">4</form:option>
							                            <form:option value="05">5</form:option>
							                            <form:option value="06">6</form:option>
							                            <form:option value="07">7</form:option>
							                            <form:option value="08">8</form:option>
							                            <form:option value="09">9</form:option>
							                            <form:option value="10">10</form:option>
							                            <form:option value="11">11</form:option>
							                            <form:option value="12">12</form:option>
													</form:select >
													<form:select path="anio" id="anio" >
														<form:option selected="selected" value="">-- Vence Año --</form:option>
														<%
							                                java.util.Calendar C= java.util.Calendar.getInstance();
							                                int anio=C.get(java.util.Calendar.YEAR);
							                                //out.println("<form:option selected>"+anio+"</form:option>");
							                                //anio++;
							                                for(int i=0;i<10;i++)
							                                {
							                                	out.println("<option value='" + anio + "'>"+ anio++ +"</option>");
							                                }
							                            %>         
													</form:select >
													
													<form:input path="domicilio"  placeholder="Dom. de EDO. Cuenta Tarjeta"  maxlength="100" />
													<form:input path="cp"  placeholder="C.P. de EDO. Cuenta Tarjeta"  maxlength="100" />
														
													<%-- <c:if test="${soliCompra.conMSI == true}"> 
														<form:select path="msi" >
															<form:option value="00">-- Meses sin Int --</form:option>
															<form:option value="01">1</form:option>
								                            <form:option value="03">3</form:option>
								                            <form:option value="06">6</form:option>
								                            <form:option value="09">9</form:option>
								                            <form:option value="12">12</form:option>
								                            <c:forTokens items="${soliCompra.arrayMeses}" delims="," var="mes">
								                            	<form:option value="${mes}">${mes}</form:option>
								                            </c:forTokens>
														</form:select >
													</c:if> --%>
									                <form:input path="cvv2" id="cvv2"  placeholder="Codigo Seguridad" maxlength="4" />
								                </div> 
											</c:if>
										</c:if>	
										<c:if test="${soliCompra.canal == 3}">
												
											<p>Nombre </p>
							                <form:input path="nombre"  required="required" placeholder="Nombre" maxlength="50" />
							                
							                <p>Email </p>
							                <form:input type="email" path="email"  required="required" placeholder="Correo Electronico"  maxlength="50" />
											
											<form:select path="tipoTarjeta" id="tipoTarjeta" required="required" >
											  <c:if test="${tproveedor.isVisa == 1}"> 
									              <form:option value="1" >Visa</form:option>
									              <form:option value="2" >Master Card</form:option>
								              </c:if>
								              <c:if test="${tproveedor.isAmex == 1}">
								              	<form:option value="3" >AMEX</form:option>
								              </c:if>
								          	</form:select>
											
							                <p>Numero Tarjeta </p>
							                <form:input path="tarjeta" id="tarjeta"  required="required" placeholder="Numero Tarjeta" maxlength="16" />
							            	
							            	<form:select path="mes" id="mes"  required="required">
												<form:option value="">-- Vence Mes --</form:option>
												<form:option value="01">1</form:option>
					                            <form:option value="02">2</form:option>
					                            <form:option value="03">3</form:option>
					                            <form:option value="04">4</form:option>
					                            <form:option value="05">5</form:option>
					                            <form:option value="06">6</form:option>
					                            <form:option value="07">7</form:option>
					                            <form:option value="08">8</form:option>
					                            <form:option value="09">9</form:option>
					                            <form:option value="10">10</form:option>
					                            <form:option value="11">11</form:option>
					                            <form:option value="12">12</form:option>
											</form:select >
											<form:select path="anio" id="anio"  required="required">
												<form:option selected="selected" value="">-- Vence Año --</form:option>
												<%
					                                java.util.Calendar C= java.util.Calendar.getInstance();
					                                int anio=C.get(java.util.Calendar.YEAR);
					                                //out.println("<form:option selected>"+anio+"</form:option>");
					                                //anio++;
					                                for(int i=0;i<10;i++)
					                                {
					                                	out.println("<option value='" + anio + "'>"+ anio++ +"</option>");
					                                }
					                            %>         
											</form:select >
											
											<c:if test="${tproveedor.isAmex == 1}">
												<div id="dataAmex" style="display: none;">
													<p>Dom. de EDO. Cuenta Tarjeta </p>
													<form:input path="domicilio"  placeholder="Dom. de EDO. Cuenta Tarjeta"  maxlength="100" />
													<p>C.P. de EDO. Cuenta Tarjeta </p>
													<form:input path="cp"  placeholder="C.P. de EDO. Cuenta Tarjeta"  maxlength="100" />
												</div>
											</c:if>
												
											<c:if test="${soliCompra.conMSI == true}">
												<div id="dataVISA" >
													<p> Diferir Pago a Meses:</p> 
													<form:select path="msi" id="msi" required="required">
														<form:option value="00">Ingrese Tarjeta...</form:option>
							                            <%-- <c:forTokens items="${soliCompra.arrayMeses}" delims="," var="mes">
							                            	<form:option value="${mes}">${mes}</form:option>
							                            </c:forTokens> --%>
													</form:select >
												</div>
											</c:if>
											<p>Codigo Seguridad </p>
							                <form:input path="cvv2" id="cvv2"  required="required" placeholder="Codigo Seguridad" maxlength="4" />
							                
										</c:if>
							                
							                <button type="submit" id="pagoBTN" class="btn btn-default">Realizar Pago</button>
								        </form:form>
					    			</div>
								</div>
							</div>
						</div>
						
						<div class="tab-pane fade <c:if test="${mensajeLogin != null || mensajeReset != null || mensajeResetOK != null}"> active in </c:if>" id="usuarios" >
							<div class="row">
								<div class="col-sm-4 col-sm-offset-1">
									<div class="login-form"><!--login form-->
										<h2>Iniciar session</h2>
										<div class="status alert alert-danger" <c:if test="${mensajeLogin == null}"> style="display: none" </c:if> >${mensajeLogin}</div>
										<form:form id="login-form" name="main-form" method="post" 
								    		autocomplete="off" modelAttribute="login" action="${pageContext.request.contextPath}/portal/login-end">
											<form:input path="login" placeholder="Usuario" />
											<form:input type="password" path="password" placeholder="Contraseña" />
											<button type="submit" class="btn btn-default">Iniciar Session</button>
										</form:form>
									</div><!--/login form-->
								</div>
								<div class="col-sm-1">
									<h2 class="or">O</h2>
								</div>
								<div class="col-sm-4">
									<div class="signup-form"><!--sign up form-->
										<h2>Recuperar Contraseña</h2>
										<div class="status alert alert-danger" <c:if test="${mensajeReset == null}"> style="display: none" </c:if> >${mensajeReset}</div>
										<div class="status alert alert-info" <c:if test="${mensajeResetOK == null}"> style="display: none" </c:if> >${mensajeResetOK}</div>
										<form:form id="reset-form" name="main-form" method="post" 
								    		autocomplete="off" modelAttribute="reset" action="${pageContext.request.contextPath}/portal/resetPassword">
											<form:input path="login" placeholder="Usuario"/>
											<button type="submit" class="btn btn-default">Recuperar Contraseña</button>
										</form:form>
									</div><!--/sign up form-->
								</div>
							</div>
						</div>
						
						<div class="tab-pane fade" id="nuevo" >
							<div class="register-req">
								<p>Favor de ingresar los siguientes datos para crear un usuario Mobilecard:</p>
							</div><!--/register-req-->
						
							<div class="shopper-informations">
								<div class="col-sm-8 col-sm-offset-2" >
									<div class="login-form">
					    				<div class="status alert alert-danger" <c:if test="${mensajePagoDir == null}"> style="display: none" </c:if> >${mensajePagoDir}</div>
					    				
								    	<%-- <form:form id="main-form" name="main-form" method="post">
								    		<p>Informaci&oacute;n Personal.</p>
								    		<form:input path="usuario" required="required" placeholder="Usuario" />
							                <form:input path="name"  required="required" placeholder="Nombre" />
							                <form:input type="email" path="email"  required="required" placeholder="Correo Electronico" />
							                
								            <p>Informaci&oacute;n de la Tarjeta de Cr&eacute;dito.</p>

							                <form:input path="tarjeta"  required="required" placeholder="Numero Tarjeta" />
							            	<form:select  path="tipoTarjeta" required="required">
												<form:option value="0" >-- Tipo Tarjeta --</form:option>
												<c:if test="${tproveedor.isVisa == 1}"> 
										        	<form:option value="1" >Visa</form:option>
										        	<form:option value="2" >Master Card</form:option>
									            </c:if>
									            <c:if test="${tproveedor.isAmex == 1}">
									            	<form:option value="3" >AMEX</form:option>
									            </c:if>
											</form:select >
							            	<form:select   path="mes" required="required">
												<form:option value="">-- Vence Mes --</form:option>
												<form:option value="01">1</form:option>
					                            <form:option value="02">2</form:option>
					                            <form:option value="03">3</form:option>
					                            <form:option value="04">4</form:option>
					                            <form:option value="05">5</form:option>
					                            <form:option value="06">6</form:option>
					                            <form:option value="07">7</form:option>
					                            <form:option value="08">8</form:option>
					                            <form:option value="09">9</form:option>
					                            <form:option value="10">10</form:option>
					                            <form:option value="11">11</form:option>
					                            <form:option value="12">12</form:option>
											</form:select >
											<form:select  path="anio"  required="required">
												<form:option selected="selected" value="">-- Vence Año --</form:option>
												<%
					                                java.util.Calendar C2= java.util.Calendar.getInstance();
					                                int anio2=C2.get(java.util.Calendar.YEAR);
					                                //out.println("<form:option selected>"+anio+"</form:option>");
					                                //anio++;
					                                for(int i=0;i<10;i++)
					                                {
					                                	out.println("<form:option >"+ anio2++ +"</form:option>");
					                                }
					                            %>         
											</form:select >
							                <form:input path="email"  required="required" placeholder="Codigo Seguridad" />
							                <button type="submit" class="btn btn-default">Realizar Pago</button>
								     </form:form>--%> 
					    			</div>
								</div>
							</div>
						</div>
					</c:if>
					<c:if test="${soliCompra.modoPago == 3}">
						<div class="tab-pane fade active in" id="referen" >
							<div class="row">
								<div class="col-sm-8 col-sm-offset-2">
									<div class="total_area login-form">
										<div class="status alert alert-danger" <c:if test="${mensajePagoRef == null}"> style="display: none" </c:if> >${mensajePagoRef}</div>
										<form:form id="main-form" name="main-form" method="post" 
								    		autocomplete="off" modelAttribute="pagoForm"
								    		action="${pageContext.request.contextPath}/portal/procesa-pago-directo">
										<ul style="background-color: #FFFFFF">
											<li>Referencia CJGS <span>${soliCompra.referenciaCJGS}</span></li>
											<li>Fecha Expiracion <span>${soliCompra.vigencia}</span></li>
											<li>Digito Verificador <span>${soliCompra.digitoVer}</span></li>
											<%-- <li>Email <span>${soliCompra.email}</span></li> --%>
											<form:input type="email" path="email"  required="required" placeholder="Correo Electronico"  maxlength="50" />
										</ul>
										<button type="submit" class="btn btn-default">Generar Referencia</button>
										</form:form>
									</div>
								</div>
							</div>
						</div>
					</c:if> 
					</div>
				</div>
			</div>
		</div>
	</section>
