/**
 * PaynetReferenceStatusResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.ws.clientes.paynet;

public class PaynetReferenceStatusResponse  implements java.io.Serializable {
    private int respCode;

    private java.lang.String respDesc;

    private int status;

    public PaynetReferenceStatusResponse() {
    }

    public PaynetReferenceStatusResponse(
           int respCode,
           java.lang.String respDesc,
           int status) {
           this.respCode = respCode;
           this.respDesc = respDesc;
           this.status = status;
    }


    /**
     * Gets the respCode value for this PaynetReferenceStatusResponse.
     * 
     * @return respCode
     */
    public int getRespCode() {
        return respCode;
    }


    /**
     * Sets the respCode value for this PaynetReferenceStatusResponse.
     * 
     * @param respCode
     */
    public void setRespCode(int respCode) {
        this.respCode = respCode;
    }


    /**
     * Gets the respDesc value for this PaynetReferenceStatusResponse.
     * 
     * @return respDesc
     */
    public java.lang.String getRespDesc() {
        return respDesc;
    }


    /**
     * Sets the respDesc value for this PaynetReferenceStatusResponse.
     * 
     * @param respDesc
     */
    public void setRespDesc(java.lang.String respDesc) {
        this.respDesc = respDesc;
    }


    /**
     * Gets the status value for this PaynetReferenceStatusResponse.
     * 
     * @return status
     */
    public int getStatus() {
        return status;
    }


    /**
     * Sets the status value for this PaynetReferenceStatusResponse.
     * 
     * @param status
     */
    public void setStatus(int status) {
        this.status = status;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof PaynetReferenceStatusResponse)) return false;
        PaynetReferenceStatusResponse other = (PaynetReferenceStatusResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.respCode == other.getRespCode() &&
            ((this.respDesc==null && other.getRespDesc()==null) || 
             (this.respDesc!=null &&
              this.respDesc.equals(other.getRespDesc()))) &&
            this.status == other.getStatus();
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += getRespCode();
        if (getRespDesc() != null) {
            _hashCode += getRespDesc().hashCode();
        }
        _hashCode += getStatus();
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(PaynetReferenceStatusResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.paynet.com.mx/", "PaynetReferenceStatusResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("respCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.paynet.com.mx/", "RespCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("respDesc");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.paynet.com.mx/", "RespDesc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("status");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.paynet.com.mx/", "Status"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
