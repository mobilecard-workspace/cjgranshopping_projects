/**
 * GetPaynetReferenceResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.ws.clientes.paynet;

public class GetPaynetReferenceResponse  implements java.io.Serializable {
    private com.addcel.ws.clientes.paynet.PaynetReferenceResponse getPaynetReferenceResult;

    public GetPaynetReferenceResponse() {
    }

    public GetPaynetReferenceResponse(
           com.addcel.ws.clientes.paynet.PaynetReferenceResponse getPaynetReferenceResult) {
           this.getPaynetReferenceResult = getPaynetReferenceResult;
    }


    /**
     * Gets the getPaynetReferenceResult value for this GetPaynetReferenceResponse.
     * 
     * @return getPaynetReferenceResult
     */
    public com.addcel.ws.clientes.paynet.PaynetReferenceResponse getGetPaynetReferenceResult() {
        return getPaynetReferenceResult;
    }


    /**
     * Sets the getPaynetReferenceResult value for this GetPaynetReferenceResponse.
     * 
     * @param getPaynetReferenceResult
     */
    public void setGetPaynetReferenceResult(com.addcel.ws.clientes.paynet.PaynetReferenceResponse getPaynetReferenceResult) {
        this.getPaynetReferenceResult = getPaynetReferenceResult;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetPaynetReferenceResponse)) return false;
        GetPaynetReferenceResponse other = (GetPaynetReferenceResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.getPaynetReferenceResult==null && other.getGetPaynetReferenceResult()==null) || 
             (this.getPaynetReferenceResult!=null &&
              this.getPaynetReferenceResult.equals(other.getGetPaynetReferenceResult())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getGetPaynetReferenceResult() != null) {
            _hashCode += getGetPaynetReferenceResult().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetPaynetReferenceResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.paynet.com.mx/", ">GetPaynetReferenceResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("getPaynetReferenceResult");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.paynet.com.mx/", "GetPaynetReferenceResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.paynet.com.mx/", "PaynetReferenceResponse"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
