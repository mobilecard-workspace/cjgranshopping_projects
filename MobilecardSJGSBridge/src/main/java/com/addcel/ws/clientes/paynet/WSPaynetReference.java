/**
 * WSPaynetReference.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.ws.clientes.paynet;

public interface WSPaynetReference extends javax.xml.rpc.Service {
    public java.lang.String getWSPaynetReferenceSoapAddress();

    public com.addcel.ws.clientes.paynet.WSPaynetReferenceSoap getWSPaynetReferenceSoap() throws javax.xml.rpc.ServiceException;

    public com.addcel.ws.clientes.paynet.WSPaynetReferenceSoap getWSPaynetReferenceSoap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
