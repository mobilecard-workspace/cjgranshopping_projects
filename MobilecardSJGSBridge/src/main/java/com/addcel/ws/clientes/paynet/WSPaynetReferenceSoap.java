/**
 * WSPaynetReferenceSoap.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.ws.clientes.paynet;

public interface WSPaynetReferenceSoap extends java.rmi.Remote {

    /**
     * Genera un numero de referencia Paynet
     */
    public com.addcel.ws.clientes.paynet.PaynetReferenceResponse getPaynetReference(java.lang.String issuerCod, java.lang.String description, com.addcel.ws.clientes.paynet.Parameter[] params) throws java.rmi.RemoteException;

    /**
     * Regresa el estatus actual de la referencia de paynet
     */
    public com.addcel.ws.clientes.paynet.PaynetReferenceStatusResponse getPaynetReferenceStatus(java.lang.String issuerCod, java.lang.String paynetReference) throws java.rmi.RemoteException;
}
