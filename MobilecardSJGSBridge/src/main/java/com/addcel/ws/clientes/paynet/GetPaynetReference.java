/**
 * GetPaynetReference.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.ws.clientes.paynet;

public class GetPaynetReference  implements java.io.Serializable {
    private java.lang.String issuerCod;

    private java.lang.String description;

    private com.addcel.ws.clientes.paynet.Parameter[] params;

    public GetPaynetReference() {
    }

    public GetPaynetReference(
           java.lang.String issuerCod,
           java.lang.String description,
           com.addcel.ws.clientes.paynet.Parameter[] params) {
           this.issuerCod = issuerCod;
           this.description = description;
           this.params = params;
    }


    /**
     * Gets the issuerCod value for this GetPaynetReference.
     * 
     * @return issuerCod
     */
    public java.lang.String getIssuerCod() {
        return issuerCod;
    }


    /**
     * Sets the issuerCod value for this GetPaynetReference.
     * 
     * @param issuerCod
     */
    public void setIssuerCod(java.lang.String issuerCod) {
        this.issuerCod = issuerCod;
    }


    /**
     * Gets the description value for this GetPaynetReference.
     * 
     * @return description
     */
    public java.lang.String getDescription() {
        return description;
    }


    /**
     * Sets the description value for this GetPaynetReference.
     * 
     * @param description
     */
    public void setDescription(java.lang.String description) {
        this.description = description;
    }


    /**
     * Gets the params value for this GetPaynetReference.
     * 
     * @return params
     */
    public com.addcel.ws.clientes.paynet.Parameter[] getParams() {
        return params;
    }


    /**
     * Sets the params value for this GetPaynetReference.
     * 
     * @param params
     */
    public void setParams(com.addcel.ws.clientes.paynet.Parameter[] params) {
        this.params = params;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetPaynetReference)) return false;
        GetPaynetReference other = (GetPaynetReference) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.issuerCod==null && other.getIssuerCod()==null) || 
             (this.issuerCod!=null &&
              this.issuerCod.equals(other.getIssuerCod()))) &&
            ((this.description==null && other.getDescription()==null) || 
             (this.description!=null &&
              this.description.equals(other.getDescription()))) &&
            ((this.params==null && other.getParams()==null) || 
             (this.params!=null &&
              java.util.Arrays.equals(this.params, other.getParams())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getIssuerCod() != null) {
            _hashCode += getIssuerCod().hashCode();
        }
        if (getDescription() != null) {
            _hashCode += getDescription().hashCode();
        }
        if (getParams() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getParams());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getParams(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetPaynetReference.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.paynet.com.mx/", ">GetPaynetReference"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("issuerCod");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.paynet.com.mx/", "issuerCod"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("description");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.paynet.com.mx/", "description"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("params");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.paynet.com.mx/", "params"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.paynet.com.mx/", "Parameter"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://www.paynet.com.mx/", "Parameter"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
