/**
 * WSPaynetReferenceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.ws.clientes.paynet;

public class WSPaynetReferenceLocator extends org.apache.axis.client.Service implements com.addcel.ws.clientes.paynet.WSPaynetReference {

    public WSPaynetReferenceLocator() {
    }


    public WSPaynetReferenceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public WSPaynetReferenceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for WSPaynetReferenceSoap
    private java.lang.String WSPaynetReferenceSoap_address = "https://www.datalogic.com.mx/PaynetCE/WSPaynetReference.asmx";

    public java.lang.String getWSPaynetReferenceSoapAddress() {
        return WSPaynetReferenceSoap_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String WSPaynetReferenceSoapWSDDServiceName = "WSPaynetReferenceSoap";

    public java.lang.String getWSPaynetReferenceSoapWSDDServiceName() {
        return WSPaynetReferenceSoapWSDDServiceName;
    }

    public void setWSPaynetReferenceSoapWSDDServiceName(java.lang.String name) {
        WSPaynetReferenceSoapWSDDServiceName = name;
    }

    public com.addcel.ws.clientes.paynet.WSPaynetReferenceSoap getWSPaynetReferenceSoap() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(WSPaynetReferenceSoap_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getWSPaynetReferenceSoap(endpoint);
    }

    public com.addcel.ws.clientes.paynet.WSPaynetReferenceSoap getWSPaynetReferenceSoap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.addcel.ws.clientes.paynet.WSPaynetReferenceSoapStub _stub = new com.addcel.ws.clientes.paynet.WSPaynetReferenceSoapStub(portAddress, this);
            _stub.setPortName(getWSPaynetReferenceSoapWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setWSPaynetReferenceSoapEndpointAddress(java.lang.String address) {
        WSPaynetReferenceSoap_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.addcel.ws.clientes.paynet.WSPaynetReferenceSoap.class.isAssignableFrom(serviceEndpointInterface)) {
                com.addcel.ws.clientes.paynet.WSPaynetReferenceSoapStub _stub = new com.addcel.ws.clientes.paynet.WSPaynetReferenceSoapStub(new java.net.URL(WSPaynetReferenceSoap_address), this);
                _stub.setPortName(getWSPaynetReferenceSoapWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("WSPaynetReferenceSoap".equals(inputPortName)) {
            return getWSPaynetReferenceSoap();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://www.paynet.com.mx/", "WSPaynetReference");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://www.paynet.com.mx/", "WSPaynetReferenceSoap"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("WSPaynetReferenceSoap".equals(portName)) {
            setWSPaynetReferenceSoapEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
