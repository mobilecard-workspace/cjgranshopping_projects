/**
 * GetPaynetReferenceStatus.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.ws.clientes.paynet;

public class GetPaynetReferenceStatus  implements java.io.Serializable {
    private java.lang.String issuerCod;

    private java.lang.String paynetReference;

    public GetPaynetReferenceStatus() {
    }

    public GetPaynetReferenceStatus(
           java.lang.String issuerCod,
           java.lang.String paynetReference) {
           this.issuerCod = issuerCod;
           this.paynetReference = paynetReference;
    }


    /**
     * Gets the issuerCod value for this GetPaynetReferenceStatus.
     * 
     * @return issuerCod
     */
    public java.lang.String getIssuerCod() {
        return issuerCod;
    }


    /**
     * Sets the issuerCod value for this GetPaynetReferenceStatus.
     * 
     * @param issuerCod
     */
    public void setIssuerCod(java.lang.String issuerCod) {
        this.issuerCod = issuerCod;
    }


    /**
     * Gets the paynetReference value for this GetPaynetReferenceStatus.
     * 
     * @return paynetReference
     */
    public java.lang.String getPaynetReference() {
        return paynetReference;
    }


    /**
     * Sets the paynetReference value for this GetPaynetReferenceStatus.
     * 
     * @param paynetReference
     */
    public void setPaynetReference(java.lang.String paynetReference) {
        this.paynetReference = paynetReference;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetPaynetReferenceStatus)) return false;
        GetPaynetReferenceStatus other = (GetPaynetReferenceStatus) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.issuerCod==null && other.getIssuerCod()==null) || 
             (this.issuerCod!=null &&
              this.issuerCod.equals(other.getIssuerCod()))) &&
            ((this.paynetReference==null && other.getPaynetReference()==null) || 
             (this.paynetReference!=null &&
              this.paynetReference.equals(other.getPaynetReference())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getIssuerCod() != null) {
            _hashCode += getIssuerCod().hashCode();
        }
        if (getPaynetReference() != null) {
            _hashCode += getPaynetReference().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetPaynetReferenceStatus.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.paynet.com.mx/", ">GetPaynetReferenceStatus"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("issuerCod");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.paynet.com.mx/", "issuerCod"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("paynetReference");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.paynet.com.mx/", "paynetReference"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
