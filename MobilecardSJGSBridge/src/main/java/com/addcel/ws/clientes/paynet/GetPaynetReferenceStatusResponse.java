/**
 * GetPaynetReferenceStatusResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.ws.clientes.paynet;

public class GetPaynetReferenceStatusResponse  implements java.io.Serializable {
    private com.addcel.ws.clientes.paynet.PaynetReferenceStatusResponse getPaynetReferenceStatusResult;

    public GetPaynetReferenceStatusResponse() {
    }

    public GetPaynetReferenceStatusResponse(
           com.addcel.ws.clientes.paynet.PaynetReferenceStatusResponse getPaynetReferenceStatusResult) {
           this.getPaynetReferenceStatusResult = getPaynetReferenceStatusResult;
    }


    /**
     * Gets the getPaynetReferenceStatusResult value for this GetPaynetReferenceStatusResponse.
     * 
     * @return getPaynetReferenceStatusResult
     */
    public com.addcel.ws.clientes.paynet.PaynetReferenceStatusResponse getGetPaynetReferenceStatusResult() {
        return getPaynetReferenceStatusResult;
    }


    /**
     * Sets the getPaynetReferenceStatusResult value for this GetPaynetReferenceStatusResponse.
     * 
     * @param getPaynetReferenceStatusResult
     */
    public void setGetPaynetReferenceStatusResult(com.addcel.ws.clientes.paynet.PaynetReferenceStatusResponse getPaynetReferenceStatusResult) {
        this.getPaynetReferenceStatusResult = getPaynetReferenceStatusResult;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetPaynetReferenceStatusResponse)) return false;
        GetPaynetReferenceStatusResponse other = (GetPaynetReferenceStatusResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.getPaynetReferenceStatusResult==null && other.getGetPaynetReferenceStatusResult()==null) || 
             (this.getPaynetReferenceStatusResult!=null &&
              this.getPaynetReferenceStatusResult.equals(other.getGetPaynetReferenceStatusResult())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getGetPaynetReferenceStatusResult() != null) {
            _hashCode += getGetPaynetReferenceStatusResult().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetPaynetReferenceStatusResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.paynet.com.mx/", ">GetPaynetReferenceStatusResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("getPaynetReferenceStatusResult");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.paynet.com.mx/", "GetPaynetReferenceStatusResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.paynet.com.mx/", "PaynetReferenceStatusResponse"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
