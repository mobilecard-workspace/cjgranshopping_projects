/**
 * PaynetReferenceResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.ws.clientes.paynet;

public class PaynetReferenceResponse  implements java.io.Serializable {
    private int respCode;

    private java.lang.String respDesc;

    private java.lang.String paynetReference;

    public PaynetReferenceResponse() {
    }

    public PaynetReferenceResponse(
           int respCode,
           java.lang.String respDesc,
           java.lang.String paynetReference) {
           this.respCode = respCode;
           this.respDesc = respDesc;
           this.paynetReference = paynetReference;
    }


    /**
     * Gets the respCode value for this PaynetReferenceResponse.
     * 
     * @return respCode
     */
    public int getRespCode() {
        return respCode;
    }


    /**
     * Sets the respCode value for this PaynetReferenceResponse.
     * 
     * @param respCode
     */
    public void setRespCode(int respCode) {
        this.respCode = respCode;
    }


    /**
     * Gets the respDesc value for this PaynetReferenceResponse.
     * 
     * @return respDesc
     */
    public java.lang.String getRespDesc() {
        return respDesc;
    }


    /**
     * Sets the respDesc value for this PaynetReferenceResponse.
     * 
     * @param respDesc
     */
    public void setRespDesc(java.lang.String respDesc) {
        this.respDesc = respDesc;
    }


    /**
     * Gets the paynetReference value for this PaynetReferenceResponse.
     * 
     * @return paynetReference
     */
    public java.lang.String getPaynetReference() {
        return paynetReference;
    }


    /**
     * Sets the paynetReference value for this PaynetReferenceResponse.
     * 
     * @param paynetReference
     */
    public void setPaynetReference(java.lang.String paynetReference) {
        this.paynetReference = paynetReference;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof PaynetReferenceResponse)) return false;
        PaynetReferenceResponse other = (PaynetReferenceResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.respCode == other.getRespCode() &&
            ((this.respDesc==null && other.getRespDesc()==null) || 
             (this.respDesc!=null &&
              this.respDesc.equals(other.getRespDesc()))) &&
            ((this.paynetReference==null && other.getPaynetReference()==null) || 
             (this.paynetReference!=null &&
              this.paynetReference.equals(other.getPaynetReference())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += getRespCode();
        if (getRespDesc() != null) {
            _hashCode += getRespDesc().hashCode();
        }
        if (getPaynetReference() != null) {
            _hashCode += getPaynetReference().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(PaynetReferenceResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.paynet.com.mx/", "PaynetReferenceResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("respCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.paynet.com.mx/", "RespCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("respDesc");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.paynet.com.mx/", "RespDesc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("paynetReference");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.paynet.com.mx/", "PaynetReference"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
