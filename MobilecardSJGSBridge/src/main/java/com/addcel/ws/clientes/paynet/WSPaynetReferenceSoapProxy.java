package com.addcel.ws.clientes.paynet;

public class WSPaynetReferenceSoapProxy implements com.addcel.ws.clientes.paynet.WSPaynetReferenceSoap {
  private String _endpoint = null;
  private com.addcel.ws.clientes.paynet.WSPaynetReferenceSoap wSPaynetReferenceSoap = null;
  
  public WSPaynetReferenceSoapProxy() {
    _initWSPaynetReferenceSoapProxy();
  }
  
  public WSPaynetReferenceSoapProxy(String endpoint) {
    _endpoint = endpoint;
    _initWSPaynetReferenceSoapProxy();
  }
  
  private void _initWSPaynetReferenceSoapProxy() {
    try {
      wSPaynetReferenceSoap = (new com.addcel.ws.clientes.paynet.WSPaynetReferenceLocator()).getWSPaynetReferenceSoap();
      if (wSPaynetReferenceSoap != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)wSPaynetReferenceSoap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)wSPaynetReferenceSoap)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (wSPaynetReferenceSoap != null)
      ((javax.xml.rpc.Stub)wSPaynetReferenceSoap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public com.addcel.ws.clientes.paynet.WSPaynetReferenceSoap getWSPaynetReferenceSoap() {
    if (wSPaynetReferenceSoap == null)
      _initWSPaynetReferenceSoapProxy();
    return wSPaynetReferenceSoap;
  }
  
  public com.addcel.ws.clientes.paynet.PaynetReferenceResponse getPaynetReference(java.lang.String issuerCod, java.lang.String description, com.addcel.ws.clientes.paynet.Parameter[] params) throws java.rmi.RemoteException{
    if (wSPaynetReferenceSoap == null)
      _initWSPaynetReferenceSoapProxy();
    return wSPaynetReferenceSoap.getPaynetReference(issuerCod, description, params);
  }
  
  public com.addcel.ws.clientes.paynet.PaynetReferenceStatusResponse getPaynetReferenceStatus(java.lang.String issuerCod, java.lang.String paynetReference) throws java.rmi.RemoteException{
    if (wSPaynetReferenceSoap == null)
      _initWSPaynetReferenceSoapProxy();
    return wSPaynetReferenceSoap.getPaynetReferenceStatus(issuerCod, paynetReference);
  }
  
  
}