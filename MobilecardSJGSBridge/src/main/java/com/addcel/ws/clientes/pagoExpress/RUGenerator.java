package com.addcel.ws.clientes.pagoExpress;


public class RUGenerator {
	
	public String createRU(String issuer, String reference, String amount, String dueDate) {
		validateParams(issuer, reference, amount, dueDate);
		
		String resultRef = "";
		resultRef += issuer.trim();
		resultRef += reference.trim();
		resultRef += amount;    //aplicar left pad al monto en centavos, de acuerdo a la cantidad de posiciones acordadas
//		resultRef += new SimpleDateFormat ("ddMMyyyy").format(dueDate);
		resultRef += dueDate;
		return resultRef + getDB10Digit(resultRef);
	}
	
	private void validateParams(String issuer, String reference, String amount, String dueDate) throws IllegalArgumentException {
		if (issuer == null || issuer.trim().length() == 0) {
			throw new IllegalArgumentException("El parametro issuer es invalido.");
		}
		if (issuer.length() != 6 ) {
			throw new IllegalArgumentException("La longitud del issuer debe ser de 6 digitos.");
		}
		if (reference == null || reference.trim().length() == 0) {
			throw new IllegalArgumentException("El parametro reference es invalido.");
		}
		if (amount != null && Double.parseDouble(amount) <= 0) {
			throw new IllegalArgumentException("El parametro amount es invalido.");
		}
		
	}
	
	private String getDB10Digit(String ref) {
		int multiplicador = 2;
		char[] caracteres = ref.toCharArray();
		
		int suma = 0;
		for (int i=caracteres.length-1; i>=0; i--) {
			int valor = 0;
			char currChar = caracteres[i];
			
			if (currChar == 'A' || currChar=='J' || currChar=='1') {
				valor = 1;
			} else if (currChar == 'B' || currChar=='K' || currChar=='S' || currChar=='2') {
				valor = 2;
			} else if (currChar == 'C' || currChar=='L' || currChar=='T' || currChar=='3') {
				valor = 3;
			} else if (currChar == 'D' || currChar=='M' || currChar=='U' || currChar=='4') {
				valor = 4;
			}  else if (currChar == 'E' || currChar=='N' || currChar=='V' || currChar=='5') {
				valor = 5;
			}  else if (currChar == 'F' || currChar=='O' || currChar=='W' || currChar=='6') {
				valor = 6;
			} else if (currChar == 'G' || currChar=='P' || currChar=='X' || currChar=='7') {
				valor = 7;
			}  else if (currChar == 'H' || currChar=='Q' || currChar=='Y' || currChar=='8') {
				valor = 8;
			}  else if (currChar == 'I' || currChar=='R' || currChar=='Z' || currChar=='9') {
				valor = 9;
			} else if (currChar == '0') {
				valor = 0;
			}
			
			int sumando = multiplicador * valor;
			if (sumando > 9 ) {
				sumando = (int)(sumando / 10) + (sumando - 10);
			}
			
			suma += sumando;
			
			System.out.println("Valor: " + valor + " Suma: " + suma);
			
			if (multiplicador == 2) {
				multiplicador = 1;
			} else {
				multiplicador = 2;
			}
		}
		
		int result = ((int)(suma/10)+1)*10 - suma;
		
		if (result == 10) {
			result =0;
		}
		
		return Integer.toString(result);
	}
	
	
}
