package com.addcel.mobilecard.services;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.addcel.mobilecard.model.vo.PagoVO;
import com.addcel.mobilecard.model.vo.pagos.ProcomVO;
import com.addcel.mobilecard.model.vo.pagos.TransactionProcomVO;
import com.addcel.mobilecard.utils.UtilsConstants;

public class ProcomService {
	private static final Logger logger = LoggerFactory.getLogger(ProcomService.class);
	
	public ProcomVO comercioFin(PagoVO pago, String afiliacion, String numeroControl, String total) {    	        
        String varTotal = formatoMontoProsa(total);      
        //cambiar por datos correctos
        String digest = digest(
        		new StringBuilder(afiliacion)
        			.append(UtilsConstants.VAR_STORE)
        			.append(UtilsConstants.VAR_TERM)
        			.append(varTotal)
        			.append(UtilsConstants.VAR_CURRENCY)
        			.append(numeroControl).toString() );   
        logger.info("JCGS MobilecardJCGSService: {}", digest);
        
    	ProcomVO procomObj = new ProcomVO(pago, varTotal, UtilsConstants.VAR_CURRENCY, 
    			UtilsConstants.VAR_ADDRESS, numeroControl, afiliacion, 
    			UtilsConstants.VAR_STORE, UtilsConstants.VAR_TERM, digest, 
    			UtilsConstants.VAR_URL_BACK);
    	
    	logger.debug("JCGS afiliacion: {}", afiliacion );
    	logger.debug("JCGS numeroControl: {}", numeroControl );
    	logger.debug("JCGS total: {}", total ); // 2,499.00
    	
    	return procomObj;
    }
	
	private String formatoMontoProsa(String monto){
		String varTotal = "000";
		String pesos = null;
        String centavos = null;
		if(monto.contains(".")){ // 2,499.00
            pesos=monto.substring(0, monto.indexOf(".")); //2,499
            centavos=monto.substring(monto.indexOf(".")+1,monto.length()); //00
            if(centavos.length()<2){
                centavos = centavos.concat("0");
            }else{
                centavos=centavos.substring(0, 2); //00
            }            
            varTotal=pesos+centavos; //2,49900
        }else{
            varTotal=monto.concat("00");
        } 

		varTotal = varTotal.replace(",", "");
		
		logger.info("Monto a cobrar 3dSecure: {}", varTotal);
		return varTotal;		
	}
	
	private String digest(String text){
		String digest="";
		try {
			logger.info("DIGEST cadena: " + text);
			MessageDigest md = MessageDigest.getInstance("SHA-1");
			md.update(text.getBytes(),0,text.length());
			digest = convertToHex(md.digest());
		} catch (NoSuchAlgorithmException e) {
			logger.info("Error al encriptar - digest", e);
		}
		logger.info("DIGEST calculado: " + digest);
		return digest;
	}
	
	private String convertToHex(byte[] data) { 
        StringBuilder buf = new StringBuilder();
        for (int i = 0; i < data.length; i++) { 
            int halfbyte = (data[i] >>> 4) & 0x0F;
            int two_halfs = 0;
            do { 
                if ((0 <= halfbyte) && (halfbyte <= 9)) 
                    buf.append((char) ('0' + halfbyte));    
                else 
                    buf.append((char) ('a' + (halfbyte - 10)));
                halfbyte = data[i] & 0x0F;
            } while(two_halfs++ < 1);
        } 
        return buf.toString();
    } 
	
	public String digestEnvioProsa(String varMerchant,String varStore,String varTerm,String varTotal,String varCurrency,String varOrderId){
		logger.info("Inicia calculo DIGEST: ");
		logger.info("varMerchant: " + varMerchant);
		logger.info("varStore: " + varStore);
		logger.info("varTerm: " + varTerm);
		logger.info("varTotal: " + varTotal);
		logger.info("varCurrency: " + varCurrency);
		logger.info("varOrderId: " + varOrderId);
		return digest(varMerchant + varStore+varTerm + varTotal + varCurrency + varOrderId);		
	}
	
	public String digestRegresoProsa(TransactionProcomVO transactionProcomV) {
		
		StringBuffer digestCad = new StringBuffer()
//				.append(transactionProcomV.getEm_Total())
//				.append(transactionProcomV.getEm_OrderID())
//				.append(transactionProcomV.getEm_Merchant())
//				.append(transactionProcomV.getEm_Store())
//				.append(transactionProcomV.getEm_Term())
//				.append(transactionProcomV.getEm_RefNum())
//				.append("-")
//				.append(transactionProcomV.getEm_Auth())
		;
				
		return digest(digestCad.toString());		
	}
}
