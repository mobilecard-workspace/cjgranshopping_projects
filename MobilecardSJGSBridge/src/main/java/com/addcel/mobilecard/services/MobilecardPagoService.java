package com.addcel.mobilecard.services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.axis.AxisProperties;
import org.apache.axis.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;
import org.springframework.web.servlet.ModelAndView;

import com.addcel.mobilecard.model.mapper.BitacorasMapper;
import com.addcel.mobilecard.model.mapper.MobilecardWebMapper;
import com.addcel.mobilecard.model.vo.AbstractLabelValue;
import com.addcel.mobilecard.model.vo.AbstractVO;
import com.addcel.mobilecard.model.vo.CompraVO;
import com.addcel.mobilecard.model.vo.LoginRequest;
import com.addcel.mobilecard.model.vo.PagoVO;
import com.addcel.mobilecard.model.vo.SolicitudCompraRespuestaVO;
import com.addcel.mobilecard.model.vo.SolicitudCompraVO;
import com.addcel.mobilecard.model.vo.TProveedorVO;
import com.addcel.mobilecard.model.vo.UsuarioVO;
import com.addcel.mobilecard.model.vo.pagos.CatalogoBinVO;
import com.addcel.mobilecard.model.vo.pagos.ProcomVO;
import com.addcel.mobilecard.model.vo.pagos.TBitacoraProsaVO;
import com.addcel.mobilecard.model.vo.pagos.TBitacoraVO;
import com.addcel.mobilecard.utils.AddCelGenericMail;
import com.addcel.mobilecard.utils.AxisSSLSocketFactory;
import com.addcel.mobilecard.utils.UtilsConstants;
import com.addcel.mobilecard.utils.UtilsService;
import com.addcel.mobilecard.utils.UtilsValidate;
import com.addcel.utils.AddcelCrypto;
import com.addcel.utils.Utilerias;
import com.addcel.ws.clientes.paynet.Parameter;
import com.addcel.ws.clientes.paynet.PaynetReferenceResponse;
import com.addcel.ws.clientes.paynet.WSPaynetReferenceSoapProxy;

@Service
public class MobilecardPagoService {
	private static final Logger logger = LoggerFactory.getLogger(MobilecardPagoService.class);

	@Autowired
	private BitacorasMapper bitMapper;

	@Autowired
	private MobilecardWebMapper mapper;
	
	@Autowired
	private UtilsService utilService;	
	
	@Autowired
	private MobilecardOpenSwitchService soService;
	
	//Errores 81 al 100
	public ModelAndView procesaPagoUsuario(PagoVO pago, ModelMap modelo) {
		ModelAndView mav = new ModelAndView("portal/pago-bancario");
		String errores = null;
		SolicitudCompraVO solicitudCompra = null;
		UsuarioVO usuario = null;
		try {
			usuario = (UsuarioVO) modelo.get("usuario");
			solicitudCompra = (SolicitudCompraVO) modelo.get("soliCompra");
			
			if(usuario.getIdUsrStatus() == 98 || usuario.getIdUsrStatus() == 99){
				LoginRequest login = new LoginRequest();
				login.setLogin(usuario.getUsrLogin());
				usuario = mapper.consultaUsuario(login);
			}
			
//			pago.setPassword(AddcelCrypto.encryptPassword(pago.getPassword()));
			
			if(!(usuario != null && usuario.getIdUsuario() == pago.getIdUsuario())){
				logger.debug("Iniciar pantalla, error" );
				mav = new ModelAndView("portal/pago-error");
				mav.addObject("mensajeError", UtilsConstants.DESC_ERROR_FINSESION);
//			}else if(!usuario.getUsrPwd().equals(pago.getPassword())){
//				res = new SolicitudCompraRespuestaVO(50, "Contraseña incorrecta, favor de validar.");
			}else{
				pago.setIdUsuario(usuario.getIdUsuario());
				pago.setTarjeta(usuario.getUsrTdcNumero());
				pago.setVigencia(usuario.getUsrTdcVigencia());
				pago.setTipoTarjeta(usuario.getIdTipoTarjeta());
				pago.setEmail(usuario.getEmail());
				pago.setNombre(usuario.getUsrNombre() + (usuario.getUsrApellido() != null?" " + usuario.getUsrApellido():"") + 
						(usuario.getUsrMaterno() != null? " " + usuario.getUsrMaterno(): ""));
				pago.setMes(usuario.getUsrTdcVigencia().substring(0, 2));
				pago.setAnio(usuario.getUsrTdcVigencia().substring(3));
				
				errores = validateFieldsPago(pago);
				if( errores != null){
					mav.addObject("mensajePago", errores);
					
				}else{
					
					if(usuario.getIdTipoTarjeta() == 3){
						pago.setCp(usuario.getUsrCp());
						pago.setDomicilio(usuario.getUsrDomAmex());
					}
					mav = procesaPago(solicitudCompra, pago, modelo);
				}
			}
		}catch (Exception pe) {
			logger.error("Error General en el proceso de pago.: {}", pe.getMessage());
			mav.addObject("mensajePago", "Error General en el proceso de pago.");
		}finally{
			pago.setCvv2("");
			modelo.put("pagoUserForm", pago);
			mav.addObject("cambPass", new LoginRequest());
//				mav.addObject("tproveedor", modelo.get("tproveedor"));
			mav.addObject("soliCompra", solicitudCompra);
			modelo.put("tproveedor", modelo.get("tproveedor") != null? modelo.get("tproveedor"): mapper.consultaTProveedor());
		}
		return mav;
	}
	
	public ModelAndView procesaPagoMeses(PagoVO pago, ModelMap modelo) {
		ModelAndView mav = null;
//		SolicitudCompraRespuestaVO res = new SolicitudCompraRespuestaVO();
		SolicitudCompraVO solicitudCompra = null;
		String errores = null;
		UsuarioVO usuario = null;
		try {
			solicitudCompra = (SolicitudCompraVO) modelo.get("soliCompra");
			usuario = (UsuarioVO) modelo.get("usuario");
			
			if(usuario != null){
				modelo.remove("usuario");
				usuario = null;
			}
			
			if(solicitudCompra == null ){
				logger.debug("Iniciar pantalla, error" );
				mav = new ModelAndView("portal/pago-error");
				mav.addObject("mensajeError", UtilsConstants.DESC_ERROR_FINSESION);
				
			}else{
				errores = validateFieldsPagoDirecto(solicitudCompra, pago);
				if( errores != null){
					mav = new ModelAndView("portal/pagina-3DS-meses");
					mav.addObject("mensajePagoMeses", errores);
					
				}else{
					mav = procesaPago(solicitudCompra, pago, modelo);
				}
				mav.addObject("soliCompra", solicitudCompra);
				mav.addObject("pago", pago);
			}
		}catch (Exception pe) {
			logger.error("Error General en el proceso de pago.: {}", pe.getMessage());
			mav = new ModelAndView("portal/pagina-3DS-meses");
			mav.addObject("mensajePagoMeses", "Error General en el proceso de pago: " + pe.getMessage());
		}finally{
			if(mav != null){
				modelo.put("pagoForm", pago);
				mav.addObject("login", new LoginRequest());
				mav.addObject("reset", new LoginRequest());
//				mav.addObject("tproveedor", modelo.get("tproveedor"));
				mav.addObject("soliCompra", solicitudCompra);
				modelo.put("tproveedor", modelo.get("tproveedor") != null? modelo.get("tproveedor"): mapper.consultaTProveedor());
			}
		}
		return mav;
	}
	
	public ModelAndView procesaPagoDirecto(PagoVO pago, ModelMap modelo) {
		ModelAndView mav = null;
//		SolicitudCompraRespuestaVO res = new SolicitudCompraRespuestaVO();
		SolicitudCompraVO solicitudCompra = null;
		String errores = null;
		UsuarioVO usuario = null;
		try {
			solicitudCompra = (SolicitudCompraVO) modelo.get("soliCompra");
			usuario = (UsuarioVO) modelo.get("usuario");
			
			if(usuario != null){
				modelo.remove("usuario");
				usuario = null;
			}
			
			if(solicitudCompra == null ){
//				mav = new ModelAndView("portal/pago-error");
//				res.setIdError(UtilsConstants.ID_ERROR_FINSESION);
//				res.setMensajeError(UtilsConstants.DESC_ERROR_FINSESION);
				
				logger.debug("Iniciar pantalla, error" );
				mav = new ModelAndView("portal/pago-error");
				mav.addObject("mensajeError", UtilsConstants.DESC_ERROR_FINSESION);
				
			}else{
				if("true".equals(pago.getConMSI())){
					mav = reenvia3DSecureConMSI(solicitudCompra, pago, modelo);
				}else{
					if(solicitudCompra.getModoPago() != 3){
						errores = validateFieldsPagoDirecto(solicitudCompra, pago);
					}
					if( errores != null){
						mav = new ModelAndView("portal/pago-inicio");
						modelo.put("pagoForm", pago);
						mav.addObject("login", new LoginRequest());
						modelo.put("tproveedor", modelo.get("tproveedor") != null? modelo.get("tproveedor"): mapper.consultaTProveedor());
						mav.addObject("mensajePagoDir", errores);
						
					}else{
						mav = procesaPago(solicitudCompra, pago, modelo);
						
					}
				}
				mav.addObject("soliCompra", solicitudCompra);
				mav.addObject("pago", pago);
				modelo.put("tproveedor", modelo.get("tproveedor") != null? modelo.get("tproveedor"): mapper.consultaTProveedor());
			}
		}catch (Exception pe) {
			logger.error("Error General en el proceso de pago.: {}", pe.getMessage());
			mav = new ModelAndView("portal/pago-inicio");
			mav.addObject("mensajePagoDir", "Error General en el proceso de pago: " + pe.getMessage());
		}finally{
			if(mav != null){
				modelo.put("pagoForm", pago);
				mav.addObject("login", new LoginRequest());
				mav.addObject("reset", new LoginRequest());
//				mav.addObject("tproveedor", modelo.get("tproveedor"));
				mav.addObject("soliCompra", solicitudCompra);
				modelo.put("tproveedor", modelo.get("tproveedor") != null? modelo.get("tproveedor"): mapper.consultaTProveedor());
			}
		}
		return mav;
	}

	public ModelAndView procesaPago(SolicitudCompraVO solicitudCompra, PagoVO pago, ModelMap modelo){
		ModelAndView mav = new ModelAndView("portal/pago-resultado");
//		SolicitudCompraRespuestaVO res = null;
		String afiliacion = null;
		try{
			afiliacion = buscaAfiliacion(solicitudCompra.getCanal(), pago.getTipoTarjeta());
			
			if(afiliacion == null){
				mav = new ModelAndView("portal/pago-inicio");
//				res = new SolicitudCompraRespuestaVO(40, "No existe una afiliacion configurada.");
				mav.addObject("mensajePagoDir", "No existe una afiliacion configurada.");
			}else{
				if(pago.getMsi() > 1){
					afiliacion = UtilsConstants.VAR_PAYW_MERCHANT;
				}
				
				solicitudCompra.setEmail(pago.getEmail() != null && !"".equals(pago.getEmail())? pago.getEmail(): solicitudCompra.getEmail());
				
				insertaBitacoras(solicitudCompra, pago, afiliacion);
				
				if(solicitudCompra.getCanal() != 3 && solicitudCompra.getModoPago() != 3 && pago.getTipoTarjeta() != 3){		//ejecutar swith abierto para call center visa/amex y pagos amex cualquier canal
					if("3dmeses".equals(pago.getOperacion())){
						mav = procesaPago3DSecureConMSI(solicitudCompra, pago, modelo);
					}else{
						mav = procesaPago3DSecure(solicitudCompra, pago, afiliacion, modelo);
					}
				}else if(solicitudCompra.getModoPago() == 3){		//ejecutar Generacion de Referencia para pago en tiendas de conveniencia
					mav = procesaPagoReferencia(solicitudCompra, pago, modelo);
					
				}else{
					mav = procesaPagoSwithAbierto(solicitudCompra, pago, afiliacion, modelo);
					
				}
			}
//			mav.addObject("soliCompraResp", res);
//			modelo.put("soliCompraResp", res);		
		}catch(Exception e){
//			res = new SolicitudCompraRespuestaVO(100, "Ocurrio un error General al realizar el pago : <br>" + (e.getMessage()!= null && e.getMessage().length() >100? e.getMessage().substring(0,100): e.getMessage()));
			logger.error("Ocurrio un error al PagoProsaService.inicioProceso: {}" , e);
			mav = new ModelAndView("portal/pago-inicio");
			mav.addObject("mensajePagoDir", "Error General en el proceso de pago: " + e.getMessage());
		}
		return mav;
	}
	
	public ModelAndView procesaPago3DSecure(SolicitudCompraVO solicitudCompra, PagoVO pago, String afiliaciones, ModelMap modelo){
		ModelAndView mav = new ModelAndView("portal/pagina-3Dsecure");
		SolicitudCompraRespuestaVO res = null;
		ProcomVO  procom = null; 
		try{
			
			procom = new ProcomService().comercioFin(pago, afiliaciones, 
					String.valueOf(solicitudCompra.getIdBitacora()), solicitudCompra.getTotalPagoForm()); 
			mav.addObject("procom", procom);
			modelo.put("pago", pago);
						
		}catch(Exception e){
			res = new SolicitudCompraRespuestaVO(100, "Ocurrio un error General al realizar el pago : <br>" + (e.getMessage()!= null && e.getMessage().length() >100? e.getMessage().substring(0,100): e.getMessage()));
			logger.error("Ocurrio un error al PagoProsaService.inicioProceso: {}" , e);
		}
		return mav;
	}	
	
	public ModelAndView reenvia3DSecureConMSI(SolicitudCompraVO solicitudCompra, PagoVO pago, ModelMap modelo){
		ModelAndView mav = new ModelAndView("portal/pagina-3DS-meses");
		return mav;
	}
	
	public ModelAndView procesaPago3DSecureConMSI(SolicitudCompraVO solicitudCompra, PagoVO pago, ModelMap modelo) {
		ModelAndView mav = null;
//		SolicitudCompraRespuestaVO soliCompraResp = new SolicitudCompraRespuestaVO();
		HashMap<String, String> resp = new HashMap<String, String>();
		int respBD = 0;
		
		try{
			resp.put("ID_AFILIACION", UtilsConstants.VAR_PAYW_MERCHANT);
			resp.put("USUARIO", UtilsConstants.VAR_PAYW_USER);
			resp.put("CLAVE_USR", UtilsConstants.VAR_PAYW_PASS);
			resp.put("CMD_TRANS", "VENTA");
			resp.put("ID_TERMINAL", UtilsConstants.VAR_PAYW_TERMINAL_ID);
			resp.put("Reference3D", solicitudCompra.getIdBitacora() + "");
			resp.put("MerchantId", UtilsConstants.VAR_PAYW_MERCHANT);
//			resp.put("MerchantName", MerchantName);
//			resp.put("MerchantCity", MerchantCity);
//			resp.put("Cert3D", Cert3D);
			resp.put("ForwardPath", UtilsConstants.VAR_PAYW_URL_BACK);
			resp.put("Expires", pago.getMes() + "/" + pago.getAnio().substring(2, 4));
			resp.put("Total", formatoMontoPayworks(solicitudCompra.getTotalPago() + ""));
			resp.put("Card", pago.getTarjeta());
			resp.put("CardType", pago.getTipoTarjeta() == 1? "VISA": "MC");
			
			respBD = bitMapper.guardaTBitacoraPIN(solicitudCompra.getIdBitacora(), AddcelCrypto.encryptHard( pago.getCvv2(), false));
			if(respBD == 1){
				mav = new ModelAndView("payworks/comercio-send");
				mav.addObject("prosa", resp);
			/*}else{
				mav = new ModelAndView("error");
				mav.addObject("mensajePagoMeses", "Error al recuperar Datos.");
				
				modelo.put("soliCompraResp", res);
				mav.addObject("soliCompraResp", res);*/
			}
			
		}catch(Exception e){
			logger.error("Error en el proceso de pago: {}", e);
			mav = new ModelAndView("error");
			mav.addObject("mensajePagoMeses", "Ocurrio un error: " + e.getMessage());
		}
		return mav;	
	}
	
	public ModelAndView procesaRespuesta3DPayworks(String cadena, ModelMap modelo) {
		SolicitudCompraRespuestaVO soliCompraResp = null;
		SolicitudCompraVO soliCompra = null;
		PagoVO pago = null;
		ModelAndView mav = null;
		HashMap<String, Object> resp = new HashMap<String, Object>();
		String pin = null;
		String msg = null;
		try{
			soliCompra = (SolicitudCompraVO) modelo.get("soliCompra");
			pago = (PagoVO) modelo.get("pago");
			
			cadena = cadena.replace(" ", "+");
			logger.debug("Cadena procesaRespuesta3DPayworks: " + cadena);
			String[] spl = cadena.split("&");
			String[] data = null;
			for(String cad: spl){
				logger.debug("data:  " + cad);
				data = cad.split("=");
				resp.put(data[0], data.length >= 2? data[1]: "");
			}
			if(resp.containsKey("Status") && "200".equals(resp.get("Status"))){
				
				pin = bitMapper.buscarTBitacoraPIN((String) resp.get("Reference3D"));
				pin = AddcelCrypto.decryptHard(pin);
				mav=new ModelAndView("payworks/comercioPAYW2");
				mav.addObject("prosa", resp);
				
				resp.put("ID_AFILIACION", UtilsConstants.VAR_PAYW_MERCHANT);
				resp.put("USUARIO", UtilsConstants.VAR_PAYW_USER);
				resp.put("CLAVE_USR", UtilsConstants.VAR_PAYW_PASS);
				resp.put("CMD_TRANS", "VENTA");
				resp.put("ID_TERMINAL", UtilsConstants.VAR_PAYW_TERMINAL_ID);
				resp.put("Total", formatoMontoPayworks((String)resp.get("Total")));
//				resp.put("MONTO", UtilsConstants.VAR_MERCHANT);
				resp.put("MODO", "PRD");
//				resp.put("REFERENCIA", UtilsConstants.VAR_MERCHANT);
//				resp.put("NUMERO_CONTROL", UtilsConstants.VAR_PAYW_MERCHANT);
//				resp.put("REF_CLIENTE1", "");
//				resp.put("REF_CLIENTE2", "");
//				resp.put("REF_CLIENTE3", "");
//				resp.put("REF_CLIENTE4", "");
//				resp.put("REF_CLIENTE5", "");
//				resp.put("NUMERO_TARJETA", UtilsConstants.VAR_MERCHANT);
				resp.put("FECHA_EXP", ((String)resp.get("Expires")).replaceAll("%2F", ""));
//				resp.put("CODIGO_SEGURIDAD", UtilsConstants.VAR_MERCHANT);
//				resp.put("CODIGO_AUT", UtilsConstants.VAR_MERCHANT);
				resp.put("MODO_ENTRADA", "MANUAL");
//				resp.put("LOTE", UtilsConstants.VAR_MERCHANT);
				resp.put("URL_RESPUESTA", UtilsConstants.VAR_PAYW_URL_BACK_W2);
				resp.put("IDIOMA_RESPUESTA", "ES");
				if(resp.get("XID") != null){
					resp.put("XID", ((String)resp.get("XID")).replaceAll("%3D", "="));
					resp.put("CAVV", ((String)resp.get("CAVV")).replaceAll("%3D", "="));
				}else{
					resp.put("XID", null);
//					resp.put("Reference3D", soliCompra.getIdBitacora()+"");
				}
//				resp.put("ESTATUS_3D", "");
//				resp.put("ECI", "");;
				resp.put("CODIGO_SEGURIDAD", pin);
				resp.put("DIFERIMIENTO_INICIAL", "00");
				resp.put("NUMERO_PAGOS", pago.getMsi() + "");
				resp.put("TIPO_PLAN", "03");
				
			}else{
				mav=new ModelAndView("portal/pago-resultado");
				soliCompraResp = new SolicitudCompraRespuestaVO();
				
				msg = "PAGO " + (pago.getTipoTarjeta() == 1? "VISA": "MASTER") + " 3D RECHAZADA: " + resp.get("Status") ;
				
				soliCompraResp.setIdError(Integer.parseInt((String)resp.get("Status")));
				soliCompraResp.setMensajeError("El pago fue rechazado. " + 
						(resp.get("Status") != null? "Clave: " + resp.get("Status"): "") + ", Descripcion: " + UtilsConstants.errorPayW.get(resp.get("Status")) );
				
				soliCompraResp.setFechaTransaccion(Utilerias.getFullFechaActual());
				soliCompraResp.setTransaccionMC(String.valueOf(soliCompra.getIdBitacora()));
				
				updateBitacoras(soliCompra, soliCompraResp, "NI", msg);
//				soliCompraResp.setJsonNotificacion(AddcelCrypto.encryptHard(utilService.objectToJson(soliCompraResp)));
				notificacionComercio(soliCompra, soliCompraResp, pago, modelo);
//				bitMapper.borrarTBitacoraPIN((String) resp.get("Reference3D"));
				
				modelo.put("soliCompraResp", soliCompraResp);
				mav.addObject("soliCompraResp", soliCompraResp);
			}

		}catch(Exception e){
			logger.error("Error en el proceso de pago: {}", e);
			mav=new ModelAndView("error");
			mav.addObject("error", "Ocurrio un error: " + e.getMessage());
		}finally{
		}
		return mav;	
	}
	
	public ModelAndView procesaPagoSwithAbierto(SolicitudCompraVO solicitudCompra, PagoVO pago, String afiliaciones, ModelMap modelo){
		ModelAndView mav = new ModelAndView("portal/pago-resultado");
		SolicitudCompraRespuestaVO res = null;
		List<String> listaNegra = null;
		try{
			
			listaNegra = mapper.buscarListaNegra(AddcelCrypto.encryptTarjeta(pago.getTarjeta()));
			
			if(listaNegra != null && listaNegra.size() > 0){
				res = new SolicitudCompraRespuestaVO(-10, "No es posible realizar el pago, el numero de Tarjeta se encuentra en Lista Negra.");
				mav = new ModelAndView("portal/pago-error");
				mav.addObject("mensajeError", "No es posible realizar el pago, el numero de Tarjeta se encuentra en Lista Negra.");
			}else{
				if(pago.getTipoTarjeta() != 3){
					if(pago.getMsi() > 1){
						mav = reenviaPagoSwithAbiertoConMSI(solicitudCompra, pago, modelo);
					}else{
						res = soService.switchAbiertoVISA(solicitudCompra, pago, afiliaciones );
					}
					
				}else if(pago.getTipoTarjeta() == 3){
					res = soService.switchAbiertoAMEX(solicitudCompra, pago, afiliaciones );
					
				}else{
					res = new SolicitudCompraRespuestaVO(41, "El Tipo de Tarjeta no es valido.");
				}
				
				if(res != null){
					notificacionComercio(solicitudCompra, res, pago, modelo);
				}
			}
		}catch(Exception e){
			res = new SolicitudCompraRespuestaVO(100, "Ocurrio un error General al realizar el pago : <br>" + (e.getMessage()!= null && e.getMessage().length() >100? e.getMessage().substring(0,100): e.getMessage()));
			logger.error("Ocurrio un error al PagoProsaService.inicioProceso: {}" , e);
			mav = new ModelAndView("portal/pago-inicio");
			mav.addObject("mensajePagoDir", "Error General en el proceso de pago: " + e.getMessage());
		}finally{
//			pago.setTarjeta(AddcelCrypto.decryptTarjeta(pago.getTarjeta()));
			if(res != null){
				modelo.put("soliCompraResp", res);
				mav.addObject("soliCompraResp", res);
			}
			
		}
		return mav;
	}
	
	public ModelAndView reenviaPagoSwithAbiertoConMSI(SolicitudCompraVO solicitudCompra, PagoVO pago, ModelMap modelo){
		ModelAndView mav = null;
		String cadena = null;
		try{
			modelo.put("pago", pago);
			bitMapper.guardaTBitacoraPIN(solicitudCompra.getIdBitacora(), AddcelCrypto.encryptHard( pago.getCvv2(), false));
			cadena = "Status=200&MerchantId=7753116&MerchantCity=Mexico&Number="+ pago.getTarjeta() +
					"&Expires=" + pago.getMes() + pago.getAnio().substring(2, 4) + "&CardType=" + (pago.getTipoTarjeta() == 1? "VISA": "MC") + 
					"&MerchantName=MOBILECARD&Reference3D="+ solicitudCompra.getIdBitacora() +"&Total=" + formatoMontoPayworks(solicitudCompra.getTotalPago()+"");
			mav = procesaRespuesta3DPayworks(cadena, modelo);
		}catch(Exception e){
			logger.error("Ocurrio un error reenviaPagoSwithAbiertoConMSI: {}" , e);
		}
		return mav;
	}
	
	public ModelAndView procesaPagoReferencia(SolicitudCompraVO solicitudCompra, PagoVO pago, ModelMap modelo){
		ModelAndView mav = new ModelAndView("portal/pago-resultado");
		SolicitudCompraRespuestaVO res = new SolicitudCompraRespuestaVO();
		PaynetReferenceResponse respPaynet = null;
		Parameter[] params = new Parameter[3];
//		int statusExito = 1;
		String referencia = null;
		String msg = null;
		
		try{
			logger.info("Referencia CJGS: " + solicitudCompra.getReferenciaCJGS());
			params[0] = new Parameter();
			params[0].setName("REFERENCE");
			params[0].setValue(solicitudCompra.getReferenciaCJGS());
			
			params[1] = new Parameter();
			params[1].setName("MONTO");
//			params[1].setValue(utilService.formatoMontoPaynet(solicitudCompra.getTotal() + ""));
			params[1].setValue(solicitudCompra.getTotal());
			
			params[2] = new Parameter();
			params[2].setName("FechaVig");
			params[2].setValue(solicitudCompra.getVigencia().substring(0,2) + "/" +
					solicitudCompra.getVigencia().substring(2,4) + "/" +
					solicitudCompra.getVigencia().substring(4));
////			params[2].setValue("31/12/2015");
//			
////			params[3] = new Parameter();
////			params[3].setName("DigitoVerificador");
////			params[3].setValue(solicitudCompra.getDigitoVer());
			
//			AxisSSLSocketFactory.setKeystorePassword("gdfkeypass");
			AxisSSLSocketFactory.setResourcePathToKeystore("com.addcel/mobilecard/utils/Datalogickey.jks");
			AxisProperties.setProperty("axis.socketSecureFactory", "com.addcel.utils.AxisSSLSocketFactory");
			
			respPaynet = new WSPaynetReferenceSoapProxy().getPaynetReference("0b0cc28b-620e-485a-ac7b-9025627d05c1", solicitudCompra.getDescripcion(), params);
			
			res.setFechaTransaccion(Utilerias.getFullFechaActual());
			res.setTransaccionMC(solicitudCompra.getIdBitacora()+"");
			
			if(respPaynet.getRespCode() == 0){
				
				msg = "Referencia:  " + respPaynet.getPaynetReference();
				res.setReferencia(respPaynet.getPaynetReference());
				
				logger.info("Referencia Generada: " + respPaynet.getPaynetReference());
				
				AddCelGenericMail.sendMail(
						utilService.objectToJson(AddCelGenericMail.generatedMailRefencia(res, solicitudCompra, pago)));
			}else{
				msg = "Error Code:  " + respPaynet.getRespCode() + ", Desc: " + respPaynet.getRespDesc();
				res.setIdError(respPaynet.getRespCode());
				res.setMensajeError(respPaynet.getRespDesc());
			}
			
//			referencia = new RUGenerator().createRU("000001", solicitudCompra.getReferenciaCJGS(),
//					utilService.formatoMontoPaynet(solicitudCompra.getTotal()+""), solicitudCompra.getVigencia());
//	
//			res.setFechaTransaccion(Utilerias.getFullFechaActual());
//			res.setTransaccionMC(solicitudCompra.getIdBitacora()+"");
//			if(referencia != null && referencia.length() > 0){
//				res.setReferencia(referencia);
//				msg = "Referencia:  " + referencia;
//			AddCelGenericMail.sendMail(
//					utilService.objectToJson(AddCelGenericMail.generatedMailRefencia(res, solicitudCompra, pago)));
//			}
			
			notificacionComercio(solicitudCompra, res, null, modelo);
			updateBitacoras(solicitudCompra, res, null, msg);
			
//			res.setReferencia(generateReferencia(35));
//			updateBitacoras(solicitudCompra, res, null, msg);
		}catch(IllegalArgumentException ix){
			mav = new ModelAndView("portal/pago-inicio");
			mav.addObject("mensajePagoRef", ix.getMessage());
		
		}catch(Exception e){
			logger.error("Ocurrio un error durante la generacion de referencia: {}", e);
			res.setIdError(1);
			res.setMensajeError(e.getMessage().length() > 50?e.getMessage().substring(0,49): e.getMessage() );
			mav = new ModelAndView("portal/pago-inicio");
			mav.addObject("mensajePagoRef", e.getMessage());
		}finally{
			mav.addObject("soliCompraResp", res);
		}
		return mav;
	}	
	
	public ModelAndView procesaRespuestaPago3DSecure(String EM_Response,
			String EM_Total, String EM_OrderID,
			String EM_Merchant, String EM_Store,
			String EM_Term, String EM_RefNum,
			String EM_Auth, String EM_Digest, 
			String cc_number, ModelMap modelo){
			
		ModelAndView mav = new ModelAndView("portal/pago-resultado");
		SolicitudCompraRespuestaVO soliCompraResp = new SolicitudCompraRespuestaVO();
		SolicitudCompraVO soliCompra = null;
		PagoVO pago = null;
		UsuarioVO usuario = null;
		String tipoTDC = "NI";
		String msg = null;

		try{
			usuario = (UsuarioVO) modelo.get("usuario");
			soliCompra = (SolicitudCompraVO) modelo.get("soliCompra");
			pago = (PagoVO) modelo.get("pago");
			soliCompraResp = new SolicitudCompraRespuestaVO();
			
			tipoTDC = tipoTDC(pago.getTarjeta());
		
			logger.debug("EM_Response: " + EM_Response);
			logger.debug("EM_Total: " + EM_Total);
			logger.debug("EM_OrderID: " + EM_OrderID);
			logger.debug("EM_Merchant: " + EM_Merchant);
//			logger.debug("EM_Store: " + EM_Store);
//			logger.debug("EM_Term: " + EM_Term);
			logger.debug("EM_RefNum: " + EM_RefNum);
			logger.debug("EM_Auth: " + EM_Auth);
			logger.debug("cc_number: " + cc_number);
//			logger.debug("EM_Digest: " + EM_Digest);
			
			if (!EM_Auth.equals("000000") ){
//				pago.setStatus(1);
//				pago.setMsg("EXITO PAGO BotonPagos VISA AUTORIZADA");
				
				msg = "EXITO PAGO " + (pago.getTipoTarjeta() == 1? "VISA": pago.getTipoTarjeta() == 2? "MASTER": "VISA") + " 3D AUTORIZADA";
				soliCompraResp.setFechaTransaccion(Utilerias.getFullFechaActual());
				soliCompraResp.setAutorizacionBancaria(EM_Auth);
				soliCompraResp.setImporte(soliCompra.getTotal());
				soliCompraResp.setComision(soliCompra.getComision());
				soliCompraResp.setTotalPago(soliCompra.getTotalPago());
				soliCompraResp.setStatus(1);
				
				if(cc_number == null || cc_number.length() == 0){
					soliCompraResp.setTarjeta("XXXXXX XXXXXX XXXX");
				}else if(cc_number.length() == 4){
					soliCompraResp.setTarjeta("XXXXXX XXXXXX " + cc_number);
				}else{
					soliCompraResp.setTarjeta("XXXXXX XXXXXX " + cc_number.substring(cc_number.length() - 4));
				}
				
				AddCelGenericMail.sendMail(
						utilService.objectToJson(AddCelGenericMail.generatedMail(soliCompraResp, soliCompra, pago)));
			}else{
				msg = "PAGO " + (pago.getTipoTarjeta() == 1? "VISA": pago.getTipoTarjeta() == 2? "MASTER": "AMEX") + " 3D RECHAZADA: " + EM_RefNum ;
				
				soliCompraResp.setStatus(0);
				soliCompraResp.setIdError(2);
				soliCompraResp.setMensajeError("El pago fue rechazado. " + (EM_Response != null? "Clave: " +EM_Response: "") + ", Descripcion: " +EM_RefNum  );
			}
			soliCompraResp.setFechaTransaccion(Utilerias.getFullFechaActual());
			soliCompraResp.setTransaccionMC(String.valueOf(soliCompra.getIdBitacora()));
			
			updateBitacoras(soliCompra, soliCompraResp, tipoTDC, msg);
//			soliCompraResp.setJsonNotificacion(AddcelCrypto.encryptHard(utilService.objectToJson(soliCompraResp)));
			notificacionComercio(soliCompra, soliCompraResp, pago, modelo);
			
		}catch(Exception e){
			soliCompraResp = new SolicitudCompraRespuestaVO(100, "Error General al realizar el pago : " + (e.getMessage()!= null && e.getMessage().length() >100? e.getMessage().substring(0,100): e.getMessage()));
			logger.error("Ocurrio un error al PagoProsaService.inicioProceso: {}" , e);
			
			soliCompraResp.setStatus(0);
			soliCompraResp.setIdError(5);
			soliCompraResp.setMensajeError("El pago fue rechazado. Error durante el procesamieto.");
			updateBitacoras(soliCompra, soliCompraResp, tipoTDC, msg);
		}finally{
			modelo.put("soliCompraResp", soliCompraResp);
			mav.addObject("soliCompraResp", soliCompraResp);
		}
		return mav;
	}
	
	public ModelAndView payworks2Respuesta(String NUMERO_CONTROL,
			String REFERENCIA, String FECHA_RSP_CTE,
			String TEXTO, String RESULTADO_PAYW,
			String FECHA_REQ_CTE, String CODIGO_AUT,
			String CODIGO_PAYW,
			String ID_AFILIACION, ModelMap modelo) {
		
		ModelAndView mav = new ModelAndView("portal/pago-resultado");
		SolicitudCompraRespuestaVO soliCompraResp = new SolicitudCompraRespuestaVO();
		SolicitudCompraVO soliCompra = null;
		PagoVO pago = null;
		Double comision = null;
		String tipoTDC = "NI";
		String msg = null;
//		HashMap<String, String> resp = new HashMap<String, String>();
		
		try{
			logger.debug("NUMERO_CONTROL: " + NUMERO_CONTROL);
			logger.debug("REFERENCIA: " + REFERENCIA);
			logger.debug("FECHA_RSP_CTE: " + FECHA_RSP_CTE);
			logger.debug("TEXTO: " + TEXTO);
			logger.debug("RESULTADO_PAYW: " + RESULTADO_PAYW);
			logger.debug("FECHA_REQ_CTE: " + FECHA_REQ_CTE);
			logger.debug("CODIGO_PAYW: " + CODIGO_PAYW);
			logger.debug("CODIGO_AUT: " + CODIGO_AUT);
			logger.debug("ID_AFILIACION: " + ID_AFILIACION);
			
			soliCompra = (SolicitudCompraVO) modelo.get("soliCompra");
			pago = (PagoVO) modelo.get("pago");
			soliCompraResp = new SolicitudCompraRespuestaVO();
			
			tipoTDC = tipoTDC(pago.getTarjeta());

			bitMapper.borrarTBitacoraPIN(NUMERO_CONTROL);
			
			if (RESULTADO_PAYW != null && "A".equals(RESULTADO_PAYW)) {
				
				msg = "EXITO PAGO " + (pago.getTipoTarjeta() == 1? "VISA": pago.getTipoTarjeta() == 2? "MASTER": "VISA") + " PAYW AUTORIZADA";
				soliCompraResp.setFechaTransaccion(Utilerias.getFullFechaActual());
				soliCompraResp.setAutorizacionBancaria(CODIGO_AUT);
				soliCompraResp.setImporte(soliCompra.getTotal());
				soliCompraResp.setComision(soliCompra.getComision());
				soliCompraResp.setTotalPago(soliCompra.getTotalPago());
				soliCompraResp.setStatus(1);
				
				if(soliCompra.getCanal() == 3){
					soliCompraResp.setTarjeta(pago.getTarjeta().substring(0, 6) + " XXXXXX " + pago.getTarjeta().substring(pago.getTarjeta().length() - 4));
				}else{
					soliCompraResp.setTarjeta("XXXXXX XXXXXX " + pago.getTarjeta().substring(pago.getTarjeta().length() - 4));
				}
				
				AddCelGenericMail.sendMail(
						utilService.objectToJson(AddCelGenericMail.generatedMail(soliCompraResp, soliCompra, pago)));
				
			}else{
				
				msg = "PAGO " + (pago.getTipoTarjeta() == 1? "VISA": "MASTER") + " 3D " + 
						("D".equalsIgnoreCase(RESULTADO_PAYW)? "DECLINADA": 
						"R".equalsIgnoreCase(RESULTADO_PAYW)? "RECHAZADA":
						"T".equalsIgnoreCase(RESULTADO_PAYW)? "Sin respuesta del autorizador":"Repuesta no Valida")
						+ ": " + TEXTO ;
				
				soliCompraResp.setStatus(0);
				soliCompraResp.setIdError(CODIGO_PAYW != null? Integer.parseInt(CODIGO_PAYW): 2);
				soliCompraResp.setMensajeError("El pago fue rechazado. " + (CODIGO_PAYW != null? "Clave: " +CODIGO_PAYW: "") + ", Descripcion: " + TEXTO  );
			}
			soliCompraResp.setFechaTransaccion(Utilerias.getFullFechaActual());
			soliCompraResp.setTransaccionMC(String.valueOf(soliCompra.getIdBitacora()));
			
			updateBitacoras(soliCompra, soliCompraResp, tipoTDC, msg);
//			soliCompraResp.setJsonNotificacion(AddcelCrypto.encryptHard(utilService.objectToJson(soliCompraResp)));
			notificacionComercio(soliCompra, soliCompraResp, pago, modelo);
		}catch(Exception e){
			logger.error("Error en el proceso de pago: {}", e);
			soliCompraResp = new SolicitudCompraRespuestaVO(100, "Error General al realizar el pago : " + (e.getMessage()!= null && e.getMessage().length() >100? e.getMessage().substring(0,100): e.getMessage()));
			logger.error("Ocurrio un error al PagoProsaService.inicioProceso: {}" , e);
		}finally{
			modelo.put("soliCompraResp", soliCompraResp);
			mav.addObject("soliCompraResp", soliCompraResp);
		}
		return mav;	
	}	
	
	public ModelAndView pagoResultado(ModelMap modelo) {
		ModelAndView mav = new ModelAndView("portal/pago-resultado");
		SolicitudCompraRespuestaVO soliCompraResp = new SolicitudCompraRespuestaVO();
		SolicitudCompraVO soliCompra = null;
		UsuarioVO usuario = null;
		try {
			usuario = (UsuarioVO) modelo.get("usuario");
			soliCompra = (SolicitudCompraVO) modelo.get("soliCompra");
			soliCompraResp = (SolicitudCompraRespuestaVO) modelo.get("soliCompraResp");

			if(soliCompra == null){
				logger.error("Dentro de login, se termino la session");
				mav = new ModelAndView("portal/pago-error");
				mav.addObject("mensajeError", UtilsConstants.DESC_ERROR_FINSESION);
			}else{
				if(soliCompra.getUrlBack() == null || "".equals(soliCompra.getUrlBack())){
					soliCompraResp.setUrlConfirmacion("https://www.mobilecard.mx");
				}else{
					soliCompraResp.setUrlConfirmacion(soliCompra.getUrlBack());
				}
				
				mav.addObject("usuario", usuario);
				mav.addObject("soliCompra", soliCompra);
				mav.addObject("soliCompraResp", soliCompraResp);
				
			}
		}catch(Exception e){
			logger.error("Ocurrio un error general en el servicio /loginFinal: {}", e );
			mav = new ModelAndView("portal/pago-error");
			mav.addObject("mensajeError", "Ocurrio un error general: " + e.getMessage());
		} finally{
//			mav.addObject("nameCompany", "promarine");
		}
		return mav;	
	}
	
	public String consultaMSI(String tarjeta, ModelMap modelo){
		List<AbstractLabelValue> lstMeses = null;
		List<AbstractLabelValue> lstMesesTemp = null;
		SolicitudCompraVO solicitudCompra = null;
		String resp = null;
		String[] bancos = null;
		try{
			solicitudCompra = (SolicitudCompraVO) modelo.get("soliCompra");
			if(tarjeta != null && tarjeta.length() >= 6 ){
				tarjeta = tarjeta.replace("{\"tarjeta\":\"", "");
				tarjeta = tarjeta.replace("\"}", "");
				lstMeses = mapper.consultaMSI(tarjeta.substring(0, 6));
				
				if(lstMeses == null || lstMeses.size() == 0 ){
					lstMeses = new ArrayList<AbstractLabelValue>();
					lstMeses.add(new AbstractLabelValue("00", "No hay meses disponibles..."));
				}
				
				if(solicitudCompra.getCanal() == 3){
					if(lstMeses != null && lstMeses.size() > 0 && !"00".equals(lstMeses.get(0).getValue())){
						bancos = lstMeses.get(0).getLabel().split("-");
						lstMeses.add(0, new AbstractLabelValue("1", bancos[0].trim() + " - 1 (contado)"));
					}else{
						lstMeses.add(0, new AbstractLabelValue("1", "GENERICO - 1 (contado)"));
					}
				}
				
				if(lstMeses != null && lstMeses.size() > 0 
						&& solicitudCompra.getMeses() != null && solicitudCompra.getMeses().length > 0){
					lstMesesTemp = new ArrayList<AbstractLabelValue>();
					for(AbstractLabelValue data: lstMeses){
						for(String dataMes: solicitudCompra.getMeses()){
							if(data.getValue().equals(dataMes.trim())){
								lstMesesTemp.add(data);
								break;
							}
						}
					}
					if(lstMesesTemp.size() > 0){
						lstMeses = lstMesesTemp;
					}
				}
				
			}else{
				lstMeses = new ArrayList<AbstractLabelValue>();
				lstMeses.add(new AbstractLabelValue("00", "No hay meses disponibles..."));
			}
		}catch(Exception e){
			lstMeses = new ArrayList<AbstractLabelValue>();
			logger.error("Ocurrio un error al PagoProsaService.inicioProceso: {}" , e);
		}finally{
			resp = utilService.objectToJson(lstMeses);
		}
		return resp;
	}	
	
	private void insertaBitacoras(SolicitudCompraVO solicitudCompra, PagoVO pago, String afiliaciones ) throws Exception{
		TBitacoraVO tb = new TBitacoraVO();
//		String tarj = null;
		try {
//			tarj = pago.getTarjeta();
//			pago.setTarjeta(pago.getTarjeta()!= null  && !"".equals(pago.getTarjeta())? AddcelCrypto.encryptTarjeta(pago.getTarjeta()): "");
			tb = new TBitacoraVO(
					pago.getIdUsuario() + "", "PAGO BotonPagos " + (pago.getTipoTarjeta() ==3?"AMEX":"VISA"), 
					(pago.getSoftware() != null? pago.getSoftware() + " " + pago.getModelo(): "Pago "), 
					"Numero Control: " + solicitudCompra.getNumeroControl(), 
					pago.getTarjeta()!= null && !"".equals(pago.getTarjeta())? AddcelCrypto.encryptTarjeta(pago.getTarjeta()): "", "WEB", 
					(pago.getSoftware() != null? pago.getSoftware(): "Pago "), (pago.getModelo() != null? pago.getModelo(): "Pago "), "", 
					String.valueOf(solicitudCompra.getTotalPago()) );
			bitMapper.addBitacora(tb);
			
			solicitudCompra.setIdBitacora(Long.parseLong(tb.getIdBitacora()));
			
			TBitacoraProsaVO tbProsa = new TBitacoraProsaVO(tb.getIdBitacora(),
					pago.getIdUsuario() + "", null, null, null, "PAGO BotonPagos " + (pago.getTipoTarjeta() ==3?"AMEX":"VISA"),
					String.valueOf(solicitudCompra.getTotal()), solicitudCompra.getComision() + "", "0", "0");
			
			bitMapper.addBitacoraProsa(tbProsa);
			bitMapper.addJCGSDetalle(solicitudCompra.getIdBitacora(), 0L, solicitudCompra.getCanal(), solicitudCompra.getModoPago(), 
					solicitudCompra.getNumeroControl(), solicitudCompra.getEmail(), solicitudCompra.getDescripcion(), pago != null? pago.getNombre():"",
							pago.getTarjeta() != null && !"".equals(pago.getTarjeta())? pago.getTarjeta().substring(0,6) + " XXXXXX " + pago.getTarjeta().substring(12): "",
					pago.getTipoTarjeta(), solicitudCompra.isConMSI()? 1: 0, pago.getMsi() != 0? pago.getMsi(): 1, 
					solicitudCompra.getTotal(), solicitudCompra.getComision(), afiliaciones, solicitudCompra.getReferenciaCJGS(), 
					solicitudCompra.getVigencia(), solicitudCompra.getDigitoVer());
			
//			if(pago.getTarjeta() != null && !"".equals(pago.getTarjeta())){
//				pago.setTarjeta(AddcelCrypto.decryptTarjeta(pago.getTarjeta()));
//			}
		} catch (Exception e) {
			logger.error("Error TenenciaServices.consume3DSecure,  id_bitacora: {}", tb.getIdBitacora(), e);
			throw new Exception(e);
		}
	}
	
	private void updateBitacoras(SolicitudCompraVO solicitudCompra, SolicitudCompraRespuestaVO soliCompraResp, String tipoTDC, String msg){
		TBitacoraProsaVO tbitacoraProsaVO = new TBitacoraProsaVO();
		TBitacoraVO tbitacoraVO = new TBitacoraVO();
		try{
			bitMapper.updateJCGSDetalle(solicitudCompra.getIdBitacora(), soliCompraResp.getReferencia(), 
					solicitudCompra.getModoPago() == 3? -1:
					soliCompraResp.getIdError() == 0? 1: 0, 
					tipoTDC, soliCompraResp.getAutorizacionBancaria(), 0, soliCompraResp.getTarjeta());
			
        	logger.info("Inicio Update TBitacora.");
        	tbitacoraVO.setIdBitacora(solicitudCompra.getIdBitacora() + "");
        	tbitacoraVO.setBitConcepto(msg);
        	tbitacoraVO.setBitTicket(msg);
        	tbitacoraVO.setBitNoAutorizacion(soliCompraResp.getAutorizacionBancaria());
        	tbitacoraVO.setBitStatus(solicitudCompra.getModoPago() != 3? soliCompraResp.getStatus(): -1);
        	tbitacoraVO.setBitCodigoError(soliCompraResp.getIdError() != 0? soliCompraResp.getIdError(): 1);
        	tbitacoraVO.setDestino("Id Transaccion: " + solicitudCompra.getNumeroControl() + "-" + tipoTDC);
        	
        	bitMapper.updateBitacora(tbitacoraVO);
        	logger.info("Fin Update TBitacora.");
        }catch(Exception e){
        	logger.error("Error durante el update en TBitacora: ", e);
        }
        
        try{
        	logger.info("Inicio Update TBitacoraProsa.");
        	tbitacoraProsaVO.setIdBitacora(solicitudCompra.getIdBitacora() + "");
        	tbitacoraProsaVO.setConcepto(msg);
        	tbitacoraProsaVO.setAutorizacion(soliCompraResp.getAutorizacionBancaria());
        	
        	bitMapper.updateBitacoraProsa(tbitacoraProsaVO);
        	
        	logger.info("Fin Update TBitacoraProsa.");
        }catch(Exception e){
        	logger.error("Error durante el update en TBitacoraProsa: ", e);
        }
	}
	
	public String tipoTDC(String tdc){
		String tipoTDC = "NI";
		List<CatalogoBinVO> bines = null;		
		try{
			if(tdc != null && tdc.length() > 10){
				bines = mapper.selectTipoTarjeta(
						tdc.substring(0, 6),
						tdc.substring(0, 7),
						tdc.substring(0, 8),
						tdc.substring(0, 9),
						tdc.substring(0, 10));
				
				if(bines!=null && bines.size()>0){
					tipoTDC = bines.get(0).getTipo();					
				}
			}else{
				logger.error("Tarjeta vacia: " + tdc );
			}
		} catch(Exception e){
			logger.error("No se pudo obtener el tipo de TDC: {}",e.getMessage());
		}
		return tipoTDC;
	}

	private String notificacionComercio(SolicitudCompraVO solicitudCompra, SolicitudCompraRespuestaVO res, PagoVO pago, ModelMap modelo){
		String notifiacionJson = null;
		StringBuilder data = new StringBuilder();
		TProveedorVO tproveedor = null;
		try{
			
			data.append("{\"idError\":").append(res.getIdError())
			.append(",\"mensajeError\":\"").append(res.getMensajeError())
			.append("\",\"numeroControl\":\"").append(solicitudCompra.getNumeroControl())
			.append("\",\"fechaTransaccion\":\"").append(res.getFechaTransaccion())
			.append("\",\"autorizacion\":").append(res.getAutorizacionBancaria()!= null? "\"" +res.getAutorizacionBancaria() + "\"":null)
			.append(",\"transaccionMC\":").append(solicitudCompra.getIdBitacora())
			.append(",\"referencia\":\"").append(res.getReferencia())
			.append("\",\"tarjeta\":\"").append(res.getTarjeta() != null? res.getTarjeta():
					pago != null && pago.getTarjeta() != null && pago.getTarjeta().length() >15 ? pago.getTarjeta().substring(0, 6) + " XXXXXX " + pago.getTarjeta().substring(12): 
						pago != null && pago.getTarjeta() != null && pago.getTarjeta().length() == 15 ? pago.getTarjeta().substring(0, 6) + " XXXXX " + pago.getTarjeta().substring(11): "" )
			.append("\",\"plazo\":").append(pago != null? pago.getMsi(): "1")
			.append(",\"pago\":").append(false)
			.append(",\"canal\":").append(solicitudCompra.getCanal())
			.append(",\"modoPago\":").append(solicitudCompra.getModoPago())
			.append("}");
			
//			tproveedor = mapper.consultaTProveedor();
			tproveedor = modelo.get("tproveedor") != null? (TProveedorVO) modelo.get("tproveedor"): mapper.consultaTProveedor();
			notifiacionJson = AddcelCrypto.encryptTripleDes(tproveedor.getClave3Desc(), data.toString(), true);
//			utilService.notificacionURLComercio(solicitudCompra.getUrlBack() + "?param=" + notifiacionJson, null);
			res.setJsonNotificacion(notifiacionJson);
			
		}catch(Exception e){
			logger.error("Error al genaral al notificar al Comercio: {}", e);
		}
		return notifiacionJson;
	}

	private String validateFieldsPago(PagoVO pago){
		String respuesta = null;
		
		try{
			respuesta = UtilsValidate.validateFields(pago.getIdUsuario()+"","IdUsuario", new String[]{UtilsValidate.REQUIRED,UtilsValidate.MINLENGTH + "12",UtilsValidate.MAXLENGTH + "15"});
//			respuesta += UtilsValidate.validateFields(pago.getPassword(),"Password", new String[]{UtilsValidate.REQUIRED,UtilsValidate.MAXLENGTH + "50"});
			respuesta += UtilsValidate.validateFields(pago.getCvv2(),"CVV2", new String[]{UtilsValidate.REQUIRED,UtilsValidate.MINLENGTH + "3",UtilsValidate.MAXLENGTH + "4"});
			
			if(StringUtils.isEmpty(respuesta)){
				respuesta = null;
			}
			
		}catch(Exception e){
			logger.error("Ocurrio un error con la validacion de los campos." , e.getMessage());
			respuesta = "Ocurrio un error con la validacion de los campos.";
		}
		
		return respuesta;
	}
	
	private String validateFieldsPagoDirecto(SolicitudCompraVO solicitudCompra, PagoVO pago){
		String respuesta = null;
		
		try{
			if(solicitudCompra.getCanal() == 3 || pago.getTipoTarjeta() == 3 || "3dmeses".equals(pago.getOperacion())){
				respuesta = UtilsValidate.validateFields(pago.getNombre(),"Nombre", new String[]{UtilsValidate.REQUIRED,UtilsValidate.MAXLENGTH + "50"});
				if(!"3dmeses".equals(pago.getOperacion())){
					respuesta += UtilsValidate.validateFields(pago.getEmail(),"Correo", new String[]{UtilsValidate.REQUIRED,UtilsValidate.MAXLENGTH + "50"});
				}
				respuesta += UtilsValidate.validateFields(pago.getTarjeta(),"Tarjeta", new String[]{UtilsValidate.REQUIRED,UtilsValidate.DIGITS,UtilsValidate.MINLENGTH + "15",UtilsValidate.MAXLENGTH + "16"});
//				respuesta += UtilsValidate.validateFields(pago.getVigencia(),"Vigencia", new String[]{UtilsValidate.REQUIRED,UtilsValidate.MINLENGTH + "5",UtilsValidate.MAXLENGTH + "10"});
				respuesta += UtilsValidate.validateFields(pago.getMes(),"Vigencia Mes", new String[]{UtilsValidate.REQUIRED,UtilsValidate.DIGITS,UtilsValidate.MINLENGTH + "2",UtilsValidate.MAXLENGTH + "2"});
				respuesta += UtilsValidate.validateFields(pago.getAnio(),"Vigencia Año", new String[]{UtilsValidate.REQUIRED,UtilsValidate.DIGITS,UtilsValidate.MINLENGTH + "4",UtilsValidate.MAXLENGTH + "4"});
				respuesta += UtilsValidate.validateFields(pago.getCvv2(),"Codigo Seguridad", new String[]{UtilsValidate.REQUIRED,UtilsValidate.DIGITS,UtilsValidate.MINLENGTH + "3",UtilsValidate.MAXLENGTH + "4"});
				respuesta += UtilsValidate.validateFields(pago.getTipoTarjeta()+"","Tipo Tarjeta", new String[]{UtilsValidate.REQUIRED,UtilsValidate.DIGITS,});
				
				if(pago.getTipoTarjeta() != 3 && ("3dmeses".equals(pago.getOperacion()) || (solicitudCompra.getCanal() == 3 && solicitudCompra.isConMSI()))){
//					respuesta = respuesta != null? respuesta: "" + 
					if(	!"".equals(UtilsValidate.validateFields(pago.getMsi()+"","Meses", new String[]{UtilsValidate.REQUIRED,UtilsValidate.DIGITS,UtilsValidate.MIN + "1"}))  ){
						respuesta = respuesta != null && !"".equals(respuesta)? respuesta: "" + "Favor de seleccionar un Mes para diferir el pago.";
					}
				}

			}else{
				respuesta = UtilsValidate.validateFields(pago.getTipoTarjeta()+"","Tipo Tarjeta", new String[]{UtilsValidate.REQUIRED,UtilsValidate.DIGITS,});
			}
			
			if(pago.getTipoTarjeta() <= 0){
				respuesta = (respuesta != null? respuesta:"" ) + "Por favor, seleccione un Tipo de Tarjeta."; 
			}
			
			if(StringUtils.isEmpty(respuesta)){
				respuesta = null;
			}
			
		}catch(Exception e){
			logger.error("Ocurrio un error con la validacion de los campos." , e.getMessage());
			respuesta = "Ocurrio un error con la validacion de los campos.";
		}
		
		return respuesta;
	}
	
	public String terminar(CompraVO compraVO, SolicitudCompraRespuestaVO transaccion, String jsonNotificacion) {
		String json = null;
		AbstractVO res = new AbstractVO();
		HashMap<String, String> data = null;
		try {
//			if(compraVO.getUrlConfirmacion() != null && !"".equals(compraVO.getUrlConfirmacion())){
//				data = new HashMap<String, String>();
//				data.put("idAplicacion", String.valueOf(compraVO.getIdAplicacion()));
//				data.put("respuesta", "");
//				mapper.jsontransaccion(data);
//				
//				String[] datos = data.get("respuesta").split(";");
//				if(!datos[0].equals("0")){
//					logger.error("Error al obtener claves no se puede notificar al Comercio: {}", datos[1]);
//				} else {
//					utilService.notificacionURLComercio(compraVO.getUrlComercio(), "json=" + jsonNotificacion);
//				}
//			}
		}catch (Exception pe) {
			res.setIdError(91);
			res.setMensajeError("Error General en el proceso de pago.");
			logger.error("Error General en el proceso de pago.: {}", pe.getMessage());
			
		} finally{
			json = utilService.objectToJson(res);
		}
		return json;
	}


	public String buscaAfiliacion(int canal, int tipoTarjeta){
		String afiliaciones = null;
		String[] tipoAfil = null;
		String[] afil = null;
		String respAf = null;
		try{
			afiliaciones = mapper.selectAfiliacion();
			if(afiliaciones != null){
				tipoAfil = afiliaciones.split(":");
				for(String cad: tipoAfil){
					afil = cad.split(",");
					if(String.valueOf(canal).equals(afil[0])){
						if(tipoTarjeta != 3){
							respAf = afil[1];
						}else if(tipoTarjeta == 3){
							respAf = afil[2];
						}
					}
				}
			}
		}catch(Exception e){
			logger.error("Error al buscar afiliaciones: {}", e.getMessage());
		}
		return respAf;
	}
	
	private static final String base = "0123456789";
	
	public static String generateReferencia(int longitud){
		StringBuffer contrasena = new StringBuffer();
		int numero = 0;
		for(int i = 0; i < longitud; i++){ //1
		    numero = (int)(Math.random()*(base.length())); //2
		    contrasena.append(base.substring(numero, numero+1)); //4
		}
		return contrasena.toString();
	}
	
	public String formatoMontoPayworks(String monto){
		String varTotal = "000";
		String pesos = null;
        String centavos = null;
		if(monto.contains(".")){
            pesos=monto.substring(0, monto.indexOf("."));
            centavos=monto.substring(monto.indexOf(".")+1,monto.length());
            if(centavos.length()<2){
                centavos = centavos.concat("0");
            }else{
                centavos=centavos.substring(0, 2);
            }            
            varTotal=pesos+ "." + centavos;
        }else{
            varTotal=monto.concat(".00");
        } 
		logger.info("Monto a cobrar 3dSecure: "+varTotal);
		return varTotal;		
	}
	
}
