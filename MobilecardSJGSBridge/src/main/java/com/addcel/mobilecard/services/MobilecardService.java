package com.addcel.mobilecard.services;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.servlet.http.HttpSession;

import org.apache.axis.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.servlet.ModelAndView;

import com.addcel.mobilecard.model.mapper.BitacorasMapper;
import com.addcel.mobilecard.model.mapper.MobilecardWebMapper;
import com.addcel.mobilecard.model.vo.AbstractVO;
import com.addcel.mobilecard.model.vo.CambioTarjeta;
import com.addcel.mobilecard.model.vo.CompraVO;
import com.addcel.mobilecard.model.vo.LoginRequest;
import com.addcel.mobilecard.model.vo.PagoVO;
import com.addcel.mobilecard.model.vo.PaymentData;
import com.addcel.mobilecard.model.vo.SolicitudCompraRespuestaVO;
import com.addcel.mobilecard.model.vo.SolicitudCompraVO;
import com.addcel.mobilecard.model.vo.TProveedorVO;
import com.addcel.mobilecard.model.vo.TokenVO;
import com.addcel.mobilecard.model.vo.UsuarioRequest;
import com.addcel.mobilecard.model.vo.UsuarioVO;
import com.addcel.mobilecard.model.vo.addcelBridge.Respuesta;
import com.addcel.mobilecard.utils.UtilsConstants;
import com.addcel.mobilecard.utils.UtilsService;
import com.addcel.mobilecard.utils.UtilsValidate;
import com.addcel.utils.AddcelCrypto;
import com.addcel.utils.Utilerias;

@Service
public class MobilecardService {
	private static final Logger logger = LoggerFactory.getLogger(MobilecardService.class);
	private static final String patron = "ddhhmmssSSS";
	private static final SimpleDateFormat formato = new SimpleDateFormat(patron);
	private static final String patronComp = "yyyyMMddhhmmss";
	private static final SimpleDateFormat formatoComp = new SimpleDateFormat(patronComp);

	@Autowired
	private MobilecardWebMapper mapper;
	
	@Autowired
	private BitacorasMapper bitMapper;
	
	@Autowired
	private UtilsService utilService;	
	
	public String getToken(String json) {	
		TProveedorVO tproveedor = null;
		TokenVO token = null;
		String respuesta = null;
		try {
			tproveedor = mapper.consultaTProveedor();
		}catch (Exception pe) {
			respuesta = "{\"idError\":11,\"mensajeError\":\"No existe configuracion para ver esta pagina.\"}";
			logger.error("Error al consultar la funcion jsontransaccion: {}", pe);
		}	
		
		try{
			if(tproveedor != null ){
				if(tproveedor.getStatus() != 1){
					respuesta = "{\"idError\":12,\"mensajeError\":\"Comercio no habilitado para realizar transacciones.\"}";
					logger.error("Comercio no habilitado para realizar transaccion: {}");
				}else{
					try{
//						logger.debug("Llave para descifrar json.: {}", tproveedor.getClave3Desc());
//						logger.debug("descifrar json.: {}", json);
						
						json = AddcelCrypto.decryptTripleDes(tproveedor.getClave3Desc(), json);
						
						if(json == null){
							respuesta = "{\"idError\":16,\"mensajeError\":\"Comercio no habilitado para realizar transacciones.\"}";
							logger.error("Error al descifrar JSON. ");
						}else{
							token = (TokenVO) utilService.jsonToObject(json, TokenVO.class);
							
							if(!tproveedor.getUsuario().equals(token.getUsuario()) || !tproveedor.getPassword().equals(token.getPassword())){
								respuesta = "{\"idError\":15,\"mensajeError\":\"Comercio no habilitado para realizar transacciones.\"}";
								logger.error("Usuario no valido: {}, base {}", token.getUsuario(), tproveedor.getUsuario());
								logger.error("Password no valido: {}, base {}", token.getPassword(), tproveedor.getPassword());
								
							}else{
//								respuesta="{\"token\":\""+AddcelCrypto.encrypt(UtilsConstants.KEY_TOKEN, mapper.getFechaActual(), false) +"\",\"idError\":0,\"mensajeError\":\"\"}";
								respuesta="{\"token\":\""+AddcelCrypto.encrypt(UtilsConstants.KEY_TOKEN, Utilerias.getFullFechaActual(), false) +
										"\",\"idError\":0,\"mensajeError\":\"\"}";
							}
						}
					}catch(Exception e){
						respuesta = "{\"idError\":17,\"mensajeError\":\"Comercio no habilitado para realizar transacciones.\"}";
						logger.error("Error al convertir json a objeto CompraVO.: {}",e);
					}
				}
				respuesta = AddcelCrypto.encryptTripleDes(tproveedor.getClave3Desc(), respuesta, false);
			}else{
				respuesta = "{\"idError\":18,\"mensajeError\":\"Comercio no habilitado para realizar transacciones.\"}";
				logger.error("Comercio no habilitado para realizar transaccion: {}");
			}
		}catch (Exception pe) {
			respuesta = "{\"idError\":19,\"mensajeError\":\"Comercio no habilitado para realizar transacciones.\"}";
			logger.error("Error al insertar usuario: {}", pe);
		}finally{
			logger.debug("Toekn respuesta: " + respuesta);
		}
		return respuesta;	
	}
	
	public ModelAndView pagoInicio(String jsonEnc, ModelMap modelo, HttpSession session) {	
		ModelAndView mav = null;
		SolicitudCompraVO soliCompra = null;
		PagoVO pago = null;
		List<PaymentData> pagoList = null;
		PaymentData dataPay = null;
		SolicitudCompraRespuestaVO res = null;
		
		try{
//			soliCompra = (SolicitudCompraVO) modelo.get("soliCompra"); 
//			if(soliCompra != null && soliCompra.getIdError() == 0){
//				logger.debug("Iniciar pantalla, login" );
//				mav = new ModelAndView("portal/pago-inicio");
////				mav.addObject("tproveedor", mapper.consultaTProveedor(soliCompra.getIdComercio() +""));
//			}else{
				session.invalidate();
				modelo.clear();
				soliCompra = getSolicitudCompra(jsonEnc, modelo);
				
				if(soliCompra != null && soliCompra.getIdError() == 0){
					
					pagoList = bitMapper.buscarPagosJCGSDetalle(0, soliCompra.getNumeroControl(), 0, 0);
					
					if(pagoList != null){
						for(PaymentData data: pagoList){
							if(soliCompra.getModoPago() == 3){
								if(data.getIdStatus() == -1){
									dataPay = data;
									res = new SolicitudCompraRespuestaVO();
									res.setMensajeError("La referencia fue generada anteriormente.");
									res.setFechaTransaccion(data.getFecha());
									res.setTransaccionMC(data.getIdBitacora() + "");
									res.setReferencia(data.getReferenciaPay());
								}
							}else{
								if(data.getIdStatus() == 1){
									dataPay = data;
									res = new SolicitudCompraRespuestaVO();
									res.setMensajeError("El Numero de Control ya fue Pagado.");
									res.setFechaTransaccion(data.getFecha());
									res.setTransaccionMC(data.getIdBitacora() + "");
									res.setAutorizacionBancaria(data.getNumAutorizacion());
									break;
								}
							}
						}
					}
						
					if(res != null){
						notificacionComercio(soliCompra, res, dataPay, modelo);
						mav = new ModelAndView("portal/pago-resultado");
						logger.debug("Iniciar pantalla, pago-resultado" );
						modelo.put("soliCompraResp", res);
						mav.addObject("soliCompraResp", res);
					}else{
						pago = new PagoVO();
						pago.setEmail(soliCompra.getEmail());

						logger.debug("Iniciar pantalla, inicio-pago" );
						mav = new ModelAndView("portal/pago-inicio");
						mav.addObject("tproveedor", mapper.consultaTProveedor());
						
						mav.addObject("pagoForm", pago);
						modelo.put("soliCompra", soliCompra);
						modelo.addAttribute("soliCompra", soliCompra);
					}
				}else{
					logger.debug("Iniciar pantalla, error" );
					mav = new ModelAndView("portal/pago-error");
					mav.addObject("mensajeError", soliCompra.getMensajeError());
				}
//			}
		}catch(Exception e){
			logger.debug("Iniciar pantalla, error" );
			logger.debug("error: " + e.getMessage() );
			mav = new ModelAndView("portal/pago-error");
			mav.addObject("mensajeError", "Ocurrio un error general: " + e.getMessage());
		} finally{
			mav.addObject("soliCompra", soliCompra);
			mav.addObject("login", new LoginRequest());
			mav.addObject("reset", new LoginRequest());
			mav.addObject("uparam", jsonEnc);
//			pago = new PagoVO();
//			pago.setEmail(soliCompra.getEmail());
//			modelo.put("pagoForm", pago);
			modelo.put("tproveedor", modelo.get("tproveedor") != null? modelo.get("tproveedor"): mapper.consultaTProveedor());
			mav.addObject("tproveedor", modelo.get("tproveedor"));
		}
		
		return mav;	
	}
	
	private String notificacionComercio(SolicitudCompraVO solicitudCompra, SolicitudCompraRespuestaVO res, PaymentData dataPay, ModelMap modelo){
		String notifiacionJson = null;
		StringBuilder data = new StringBuilder();
		TProveedorVO tproveedor = null;
		try{
			
			data.append("{\"idError\":").append(res.getIdError())
			.append(",\"mensajeError\":\"").append(res.getMensajeError())
			.append("\",\"numeroControl\":\"").append(solicitudCompra.getNumeroControl())
			.append("\",\"fechaTransaccion\":\"").append(res.getFechaTransaccion())
			.append("\",\"autorizacion\":\"").append(res.getAutorizacionBancaria())
			.append("\",\"transaccionMC\":").append(solicitudCompra.getIdBitacora())
			.append(",\"referencia\":\"").append(res.getReferencia())
			.append("\",\"tarjeta\":\"").append(dataPay.getCardNumber() )
			.append("\",\"plazo\":").append(dataPay.getPlazo())
			.append(",\"pago\":").append(false)
			.append(",\"canal\":").append(solicitudCompra.getCanal())
			.append(",\"modoPago\":").append(solicitudCompra.getModoPago())
			.append("}");
			
			tproveedor = modelo.get("tproveedor") != null? (TProveedorVO)modelo.get("tproveedor"): mapper.consultaTProveedor();
			notifiacionJson = AddcelCrypto.encryptTripleDes(tproveedor.getClave3Desc(), data.toString());
//			utilService.notificacionURLComercio(solicitudCompra.getUrlBack() + "?param=" + notifiacionJson, null);
			res.setJsonNotificacion(notifiacionJson);
			
		}catch(Exception e){
			logger.error("Error al genaral al notificar al Comercio: {}", e);
		}
		return notifiacionJson;
	}
	
	
	
	public ModelAndView loginEnd(@ModelAttribute("login") LoginRequest login, ModelMap modelo) {
		ModelAndView mav = new ModelAndView("portal/pago-inicio");
		UsuarioVO usuario = null;
		SolicitudCompraVO soliCompra = null;
		PagoVO pago = null;
			
		try{
			soliCompra = (SolicitudCompraVO) modelo.get("soliCompra");
			
//			if(soliCompra == null){
////				compraVO = addcelService.getCompraVO(jsonEnc, comercio);
//			}
			if(soliCompra == null){
				logger.error("Dentro de login, se termino la session");
				mav = new ModelAndView("portal/pago-error");
				mav.addObject("mensajeError", UtilsConstants.DESC_ERROR_FINSESION);
			}else{
				logger.info("Iniciar session, login: {}", login.getLogin() );
				mav.addObject("pago", new PagoVO());
				mav.addObject("soliCompra", soliCompra);
				mav.addObject("login", login);
				
//				login.setPassword(AddcelCrypto.encryptPassword(login.getPassword()));
				
				usuario = mapper.consultaUsuario(login);
				
				if(usuario == null){
					mav.addObject("mensajeLogin", "Usuario o Contraseña incorrectos, favor de validar.");
				}else if(usuario.getIdUsrStatus() == 0){
					mav.addObject("mensajeLogin", "Usuario Inactivo. Consulte con el Administrador.");
//	            }else if(usuario.getIdUsrStatus() == 99){
//	            	mav.addObject("mensajeLogin", "Modifique su Informacion por seguridad.");
//	            }else if(usuario.getIdUsrStatus() == 98){
//	            	mav.addObject("mensajeLogin", "Modifique su clave de acceso por seguridad.");
//					logger.error("Modifique su Informacion por seguridad, id_user: " + usuario.getIdUsuario());
	            }else if(usuario.getIdUsrStatus() != 1 && usuario.getIdUsrStatus() != 98 && usuario.getIdUsrStatus() != 99){
	            	mav.addObject("mensajeLogin", "Estatus de Usuario Invalido. Consulte con el Administrador.");
					logger.error("Estatus de Usuario Invalido, id_user: {}  , status:  {}", usuario.getIdUsuario(), usuario.getIdUsrStatus());
				}else if(!usuario.getUsrPwd().equals(AddcelCrypto.encryptPassword(login.getPassword()))){
					mav.addObject("mensajeLogin", "Usuario o Contraseña incorrectos, favor de validar.");
					logger.error("El password es incorrecto, id_user: " + login.getLogin());
				}else{
					//Se sube a session el objeto Usuario
					modelo.put("usuario", usuario);
					mav.addObject("usuario", usuario);
					usuario.setUsrTdcNumeroForm("XXXX XXXX XXXX " + AddcelCrypto.decryptTarjeta(usuario.getUsrTdcNumero()).substring(12));
					
					if(usuario.getIdUsrStatus() == 1){
						logger.debug("Status del usuario correcto, se procede con la pantalla de pago.");
						mav = new ModelAndView("portal/pago-bancario");
						mav.addObject("pagoUserForm", new PagoVO());
//						mav.addObject("catalogoGenero", commonService.getCatalogoGenero());
//						mav.addObject("catalogoPais", commonService.getCatalogoPais());
//						mav.addObject("catalogoOperador", commonService.getCatalogoOperador());
//						mav.addObject("catalogoCuotas", commonService.getCuotas());
						
					}else if(usuario.getIdUsrStatus() == 99 || usuario.getIdUsrStatus() == 98){
						LoginRequest usuer = new LoginRequest();
						usuer.setLogin(usuario.getUsrLogin());
						logger.debug("Status del usuario Cambio de password.");
						mav = new ModelAndView("portal/pago-cambio-pass", "cambioPassword", usuer);
					}
				}
			}
			
		}catch(Exception e){
			logger.error("Ocurrio un error general en el servicio /loginFinal: {}", e );
			mav = new ModelAndView("portal/pago-error");
			mav.addObject("mensajeError", "Ocurrio un error general: " + e.getMessage());
		} finally{
			pago = new PagoVO();
			pago.setEmail(soliCompra.getEmail());
			mav.addObject("tproveedor", mapper.consultaTProveedor());
			mav.addObject("pagoForm", pago);
			mav.addObject("cambPass", new LoginRequest());
			mav.addObject("reset", new LoginRequest());
			login.setPassword("");
		}
		return mav;	
	}
	
	public ModelAndView registro(String jsonEnc, String comercio, ModelMap modelo) {	
		ModelAndView mav = null;
		SolicitudCompraVO soliCompra = null;
		
		try{
			soliCompra = (SolicitudCompraVO) modelo.get("soliCompra");
			if(soliCompra != null){
				mav = new ModelAndView("portal/pago-registro");
				mav.addObject("tproveedor", mapper.consultaTProveedor());
				try{
					mav.addObject("terminosCondiciones", mapper.consultaTerminos());
				}catch(Exception e){
					logger.error("Ocurrio un error al cargar catalogos: {}", e );
				}
				
			}else{
				logger.debug(UtilsConstants.DESC_ERROR_FINSESION );
				mav = new ModelAndView("portal/pago-error");
				mav.addObject("mensajeError", UtilsConstants.DESC_ERROR_FINSESION);
			}
			
		}catch(Exception e){
			mav = new ModelAndView("portal/pago-error");
			mav.addObject("mensajeError", "Ocurrio un error general: " + e.getMessage());
			logger.error("Ocurrio un error general: {}", e );
		} finally{
			mav.addObject("nameCompany", "promarine");
			mav.addObject("uparam", jsonEnc);
			mav.addObject("ucom", comercio);
			mav.addObject("tipoPago", "1");
		}
		
		return mav;	
	}
	
	public String registroFinal(UsuarioRequest usuario, ModelMap modelo ) {
		AbstractVO res = new AbstractVO();
		String key = null;
		String json = null;
		SolicitudCompraVO soliCompra = null;
		
		try{
			soliCompra = (SolicitudCompraVO) modelo.get("soliCompra");
			res.setMensajeError(validateFields(usuario, true));
			if( res.getMensajeError() != null){
				res.setIdError(10);
			}else{
				usuario.setProveedor(soliCompra.getIdComercio());
				usuario.setTipo("WEB-" + usuario.getModelo() + "-" +usuario.getSoftware() );
				usuario.setImei(usuario.getModelo() + "-" +usuario.getSoftware() + "-" + formatoComp.format(new Date()));
				
				key = formato.format(new Date());
				json =  AddcelCrypto.encryptSensitive(key, utilService.objectToJson(usuario));
				json = utilService.notificacionHttpsURLComercio(UtilsConstants.URL_INSERT_USER, "json=" + json, true);
				json =  AddcelCrypto.decryptJson(key, json);
				
				json = json.replaceAll("resultado", "idError");
				json = json.replaceAll("mensaje", "mensajeError");
				logger.info("Respuesta AddcelBridge.guardarUsuario: {}", json);
				
				res = (AbstractVO) utilService.jsonToObject(json, AbstractVO.class);
				if(res.getIdError() == 1){
					res.setIdError(0);
				}
			}
			
		}catch (Exception pe) {
			res.setIdError(10);
			res.setMensajeError("Error al registrar usuario");
			logger.error("Error al insertar usuario: {}", pe.getMessage());
		}finally{
			json = utilService.objectToJson(res);
			//logger.info("Cadena Respuesta: {}", json);
		}
		return json;
	}
	
	
	public ModelAndView resetPassword( LoginRequest cambioPassword, ModelMap modelo) {
		ModelAndView mav = new ModelAndView("portal/pago-inicio");
		SolicitudCompraVO soliCompra = null;
		String resServicio = null;
		String[] data = null;
		PagoVO pago = null;
		try {
			soliCompra = (SolicitudCompraVO) modelo.get("soliCompra");
			
			resServicio = utilService.notificacionURLComercio(UtilsConstants.URL_RECOVERY_PASS + "?json=" +
					AddcelCrypto.encrypt(UtilsConstants.KEY_ADDCELBRIDGE, "{\"cadena\":\""+ (cambioPassword.getLogin() != null? cambioPassword.getLogin(): cambioPassword.getEmail() ) +"\"}", false), null
					);
			resServicio = AddcelCrypto.decrypt(UtilsConstants.KEY_ADDCELBRIDGE, resServicio, false);
			logger.debug("Respuesta Servicio adc_RecoveryUP: "+ resServicio);
			
			if(resServicio != null && !"".equals(resServicio.trim())){
				data = resServicio.split("\\|");
				if("0".equals(data[0])){
					mav.addObject("mensajeResetOK", "La nueva contraseña se a enviado al correo registrado.");
				}else{
					mav.addObject("mensajeReset", data[1]);
				}
			}else{
				mav.addObject("mensajeReset", "Ocurrio un error al recuperar la contraseña, favor de intentar mas tarde.");
			}
			
		}catch (Exception pe) {
			logger.error("Error General en el cambio de password json: {}", pe.getMessage());
			mav.addObject("mensajeReset", "Error General al recuperar de contraseña, favor de intentar mas tarde.");
		} finally{
			pago = new PagoVO();
			pago.setEmail(soliCompra.getEmail());
			mav.addObject("tproveedor", mapper.consultaTProveedor());
			mav.addObject("pagoForm", pago);
			mav.addObject("login", new LoginRequest());
			mav.addObject("reset", new LoginRequest());
		}
		return mav;
	}
	
	public ModelAndView cambioPassword(LoginRequest cambioPassword, ModelMap modelo) {
		ModelAndView mav = new ModelAndView("portal/pago-cambio-pass");
		UsuarioVO usuarioVO = null;
		SolicitudCompraVO soliCompra = null;
		AbstractVO data = new AbstractVO();
		
		try {
			usuarioVO = (UsuarioVO) modelo.get("usuario");
			soliCompra = (SolicitudCompraVO) modelo.get("soliCompra");
			if(usuarioVO.getIdUsuario() == cambioPassword.getIdUsuario()){
				data = cambioPassword(cambioPassword, usuarioVO, soliCompra);
				
				if(data.getIdError() == 0){
					mav = new ModelAndView("portal/pago-bancario");
					mav.addObject("pagoUserForm", new PagoVO());
					mav.addObject("cambPass", new LoginRequest());
					mav.addObject("soliCompra", soliCompra);
					
				}else{
					mav.addObject("mensajeCambPass", data.getMensajeError());
				}
				mav.addObject("usuario", usuarioVO);
			}
			
		}catch (Exception pe) {
			mav.addObject("mensajeError", "Error General en el cambio de password.");
			logger.error("Error General en el cambio de password: {}", pe.getMessage());
		}finally{
			mav.addObject("pago", new PagoVO());
			mav.addObject("soliCompra", soliCompra);
			mav.addObject("cambioPassword", cambioPassword);
			cambioPassword.setPassword("");
			cambioPassword.setNewPassword("");
			cambioPassword.setNewPasswordCon("");
		}
		return mav;
	}
	
	public ModelAndView cambioPasswordFinal(LoginRequest cambioPassword, ModelMap modelo) {
		ModelAndView mav = new ModelAndView("portal/pago-bancario");
		SolicitudCompraVO solicitudCompra = null;
		UsuarioVO usuarioVO = null;
		AbstractVO res = new AbstractVO();
		
		try {
			usuarioVO = (UsuarioVO) modelo.get("usuario");
			solicitudCompra = (SolicitudCompraVO) modelo.get("soliCompra");
			
			res = cambioPassword(cambioPassword, usuarioVO, solicitudCompra);
			
			if(res.getIdError() == 0){
				mav.addObject("mensajeCambioPassOK", res.getMensajeError());
			}else{
				mav.addObject("mensajeCambioPass", res.getMensajeError());
			}
				
		}catch (Exception pe) {
			logger.error("Error General en el cambio de password json: {}", pe.getMessage());
			mav.addObject("mensajeCambioPass", "Error General en el cambio de password: " + pe.getMessage());
		}finally{
			modelo.put("pagoUserForm", new PagoVO());
			mav.addObject("cambPass", new LoginRequest());
//				mav.addObject("tproveedor", modelo.get("tproveedor"));
			mav.addObject("soliCompra", solicitudCompra);
			mav.addObject("tproveedor", mapper.consultaTProveedor());
		}
		return mav;	
	}
	
	public AbstractVO cambioPassword(LoginRequest cambioPassword, UsuarioVO usuarioVO, SolicitudCompraVO soliCompra) {
		AbstractVO res = new AbstractVO();
		String resServicio = null;
		Respuesta data = null;
		
		try {
			if(usuarioVO.getIdUsuario() == cambioPassword.getIdUsuario()){
				if(usuarioVO.getIdUsrStatus() == 99){
					resServicio = utilService.notificacionURLComercio(UtilsConstants.URL_UPDATE_PASS_MAIL + "?json=" +
							AddcelCrypto.encryptSensitive(cambioPassword.getPassword(),//UtilsConstants.KEY_ADDCELBRIDGE_SEN, 
									"{\"login\":\""+ cambioPassword.getLogin( ) +
									"\", \"password\":\""+ cambioPassword.getPassword() +
									"\", \"newPassword\":\""+ cambioPassword.getNewPassword() +
									"\"}"), null
							);
				}else{
					resServicio = utilService.notificacionURLComercio(UtilsConstants.URL_UPDATE_PASS + "?json=" +
							AddcelCrypto.encryptSensitive(cambioPassword.getPassword(),
									"{\"login\":\""+ cambioPassword.getLogin( ) +
									"\", \"password\":\""+ cambioPassword.getPassword() +
									"\", \"newPassword\":\""+ cambioPassword.getNewPassword() +
									"\", \"mail\":\""+ cambioPassword.getEmail() +
									"\"}"), null
							);
				}
				
//				logger.debug("Respuesta Servicio adc_RecoveryUP: "+ resServicio);
				resServicio = AddcelCrypto.decryptJson(cambioPassword.getPassword(), resServicio);
				if(resServicio != null){
					
					data = (Respuesta) utilService.jsonToObject(resServicio, Respuesta.class);
					
//					if(data.getResultado() == 1){
						res = new AbstractVO(0, "La contraseña a sido actualizada correctamente.");
//					}else{
//						res = new AbstractVO(data.getResultado(), data.getMensaje());
//					}
					
				}else{
					res = new AbstractVO(50, "Error General en el cambio de password.");
					logger.error("Error General en el cambio de password: al notificar a AddcelBridge");
				}
			}
		}catch (Exception pe) {
			res = new AbstractVO(50, "Error General en el cambio de password.");
			logger.error("Error General en el cambio de password: {}", pe.getMessage());
		}
		return res;
	}
	
	public String random() {
        Random rand = new Random();
        int x = rand.nextInt(99999999);
        return "" + x;
    }
	
	//Errores 61 al 80
	public String cambioTarjetaJson(UsuarioVO usuarioVO, Map<String, String> formInputs) {
		AbstractVO res = new AbstractVO();
		String json = null;
		CambioTarjeta cambioTarjeta = null;
		try {
			json =  utilService.objectToJson(formInputs);
			
			logger.debug("cambioPassword Json: {}", json);
			cambioTarjeta = (CambioTarjeta) utilService.jsonToObject(json, CambioTarjeta.class);
			
			if(!(usuarioVO != null && usuarioVO.getIdUsuario() == cambioTarjeta.getIdUsuario())){
				res.setIdError(UtilsConstants.ID_ERROR_FINSESION);
				res.setMensajeError("La sesión se ha terminado.");
				
			}else{
			
				res.setMensajeError(validateFieldsCambioTarjeta(cambioTarjeta));
				if( res.getMensajeError() != null){
					res.setIdError(10);
				}else{
					
					cambioTarjeta.setEmail(usuarioVO.getEmail());
//					cambioTarjeta.setNombres(usuarioVO.getNombres());
					
					json =  AddcelCrypto.encryptSensitive(formato.format(new Date()), utilService.objectToJson(cambioTarjeta));
//					json = bridgeWSProxy.cambiarTarjeta(json);
					json =  AddcelCrypto.decryptSensitive(json);
					
					logger.info("Respuesta AddcelColombiaBridge.cambiarTarjeta: {}", json);
					res = (AbstractVO) utilService.jsonToObject(json, AbstractVO.class);
				}
			}
			
		}catch (Exception pe) {
			res.setIdError(61);
			res.setMensajeError("Error General en el cambio de tarjeta.");
			logger.error("Error General en el cambio de tarjeta: {}", pe.getMessage());
		}finally{
			json =  utilService.objectToJson(res);
		}
		return json;
	}
	
	//Errores 81 al 100
	public String cambioDatosJson(UsuarioVO usuarioVO, Map<String, String> formInputs) {
		AbstractVO res = new AbstractVO();
		String json = null;
		try {
			json =  utilService.objectToJson(formInputs);
			
			logger.debug("UsuarioRequest: {}", json);
			UsuarioRequest usuario = (UsuarioRequest) utilService.jsonToObject(json, UsuarioRequest.class);
			
//			if(!(usuarioVO != null && usuarioVO.getIdUsuario() == usuario.getIdUsuario())){
//				res.setIdError(UtilsConstants.ID_ERROR_FINSESION);
//				res.setMensajeError("La sesión se ha terminado.");
//				
//			}else{
////				res.setMensajeError(validateFields(usuario, false));
//				if( res.getMensajeError() != null){
//					res.setIdError(10);
//				}else{
//					json =  AddcelCrypto.encryptSensitive(formato.format(new Date()), utilService.objectToJson(usuario));
////					json = bridgeWSProxy.actualizarUsuario(json);
//					json =  AddcelCrypto.decryptSensitive(json);
//					
//					logger.info("Respuesta AddcelColombiaBridge.actualizarUsuario: {}", json);
//					res = (AbstractVO) utilService.jsonToObject(json, AbstractVO.class);
//				}
//			}
		}catch (Exception pe) {
			res.setIdError(91);
			res.setMensajeError("Error General en el cambio de datos.");
			logger.error("Error General en el cambio de datos: {}", pe.getMessage());
		}finally{
			json =  utilService.objectToJson(res);
		}
		return json;
	}
	
	
	//Errores el 20 al 40
	//error 30, 31 usados en el controller
	public SolicitudCompraVO getSolicitudCompra(String json, ModelMap modelo) {
		SolicitudCompraVO soliCompra = null;
		TProveedorVO tproveedor = null;
		String certificado = null;
		String respuesta = null;
		Integer diferencia = null;
		
		try {
			tproveedor = mapper.consultaTProveedor();
			
		}catch (Exception pe) {
			soliCompra = new SolicitudCompraVO(21, "No tiene permisos para ver esta pagina, Error 21.");
			logger.error("Error al consultar la funcion jsontransaccion: {}", pe);
		}	
		
		try{
			if(tproveedor == null ){
				soliCompra = new SolicitudCompraVO(20, "El Comercio no esta configurado para realizar transacciones.");
			}else{
				
				if(tproveedor.getStatus() != 1){
					soliCompra = new SolicitudCompraVO(20, "Comercio no habilitado para realizar transacciones.");
//						logger.error("Comercio no habilitado para realizar transaccion: {}", idComercio);
				}else{
					try{
						modelo.put("tproveedor", tproveedor);
//						logger.debug("Llave para descifrar json.: {}", tproveedor.getClave3Desc());
//						logger.debug("descifrar json.: {}", json);
						
						json = AddcelCrypto.decryptTripleDes(tproveedor.getClave3Desc(), json, true);
						
						if(json != null){
							soliCompra = (SolicitudCompraVO) utilService.jsonToObject(json, SolicitudCompraVO.class);
							
							completarSolicitudCompra(soliCompra);
							
							if(tproveedor.getTipoComision() == 1){
								soliCompra.setComision(soliCompra.getTotal() * (tproveedor.getComiPorc()/100));
								soliCompra.setTotalPago(soliCompra.getTotal() + soliCompra.getComision());
							}else if(tproveedor.getTipoComision() == 2){
								soliCompra.setComision(tproveedor.getComiFija());
								soliCompra.setTotalPago(soliCompra.getTotal() + tproveedor.getComiFija());
							}else{
								soliCompra.setTotalPago(soliCompra.getTotal());
							}
							
						}else{
							soliCompra = new SolicitudCompraVO(23, "No tiene permisos para ver esta pagina, Error 23.");
							logger.debug("Error al descifrar JSON. ");
						}
						
					}catch(Exception e){
						soliCompra = new SolicitudCompraVO(24, "No tiene permisos para ver esta pagina, Error 24.");
						logger.error("Error al convertir json a objeto CompraVO.: {}",e);
					}
					
					if(soliCompra.getIdError() == 0 && soliCompra != null ){
						
						respuesta = validateFieldsCompra(soliCompra);
						
						certificado = UtilsValidate.getCertificadoMD5(soliCompra, tproveedor.getSecureCode());
						
//						diferencia = mapper.difFechaMin(soliCompra.getToken());
						
						diferencia = utilService.difFechasSegundos(soliCompra.getToken(), Utilerias.getFullFechaActual());
						
						if(diferencia == null || diferencia > 40){
							soliCompra = new SolicitudCompraVO(25, "La transaccion ya no es valida. <br><br>" +
									"Regrese al sitio previo a esta pagina (sin utilizar el botón Atrás del navegador) y comience nuevamente su compra.");
						}else 
							if(!certificado.equals(soliCompra.getCertificado())){
							soliCompra = new SolicitudCompraVO(26, "No tiene permisos para ver esta pagina, Error 26.");
							logger.error("Certificado no valido: {}, base {}",  soliCompra.getCertificado(), certificado);
						}else 
							if(soliCompra.getTotalPago() < tproveedor.getMontoMin()){
							soliCompra = new SolicitudCompraVO(28, "Total a Pagar es menor al Monto Minimo permitido <br><br> Total Pagar: " + soliCompra.getTotalPagoForm() +
									"<br>Monto Minimo: " + Utilerias.formatoImporteMon(tproveedor.getMontoMin()));
							logger.error("Total a Pagar es menor al monto minimo permitido Total Pagar: {}, Monto Minimo: {}",
									soliCompra.getTotalPagoForm(), tproveedor.getMontoMin());
							
						}else if(soliCompra.getTotalPago() > tproveedor.getMontoMax()){
							soliCompra = new SolicitudCompraVO(28, "Total a Pagar es mayor al Monto Maximo permitido <br><br> Total Pagar: " + soliCompra.getTotalPagoForm() +
									"<br>Monto Maximo: " + Utilerias.formatoImporteMon(tproveedor.getMontoMax()));
							logger.error("Total a Pagar es mayor al Monto Maximo permitido Total Pagar: {}, Monto Maximo: {}",
									soliCompra.getTotalPagoForm(), tproveedor.getMontoMax());
//						}else if(soliCompra.getModoPago() == 3 && soliCompra.getTotalPago() > 9999.99){
//							soliCompra = new SolicitudCompraVO(28, "Total a Pagar es mayor al Monto Maximo permitido <br><br> Total Pagar: " + soliCompra.getTotalPagoForm() +
//									"<br>Monto Maximo: 9999.99" );
//							logger.error("Total a Pagar es mayor al Monto Maximo permitido Total Pagar: {}, Monto Maximo: 99999.99",
//									soliCompra.getTotalPagoForm());
						}else if(respuesta != null){
							soliCompra = new SolicitudCompraVO(27, "No tiene permisos para ver esta pagina, Error 27.");
							logger.error("Parametros del objeto CompraVO no valido: {}", respuesta);
						}
					}else{
						logger.error("compraVO: null");
					}
				}
			}
		}catch (Exception pe) {
			soliCompra = new SolicitudCompraVO(23, "No tiene permisos para ver esta pagina, Error 23.");
			soliCompra.setIdError(26);
			soliCompra.setMensajeError("Ocurrio un error general, Validacion 26.");
			logger.error("Error al insertar usuario: {}", pe);
		}
		return soliCompra;
	}
	
	

	private String validateFieldsLogin(LoginRequest login, boolean tipo){
		String respuesta = null;
		
		try{
			respuesta = UtilsValidate.validateFields(login.getLogin(),"Usuario", new String[]{UtilsValidate.REQUIRED,UtilsValidate.MAXLENGTH + "15",UtilsValidate.MINLENGTH + "3"});
			respuesta += UtilsValidate.validateFields(login.getPassword(),"Password", new String[]{UtilsValidate.REQUIRED,UtilsValidate.MAXLENGTH + "16"});
			
			if(StringUtils.isEmpty(respuesta)){
				respuesta = null;
			}
			
		}catch(Exception e){
			logger.error("Ocurrio un error con la validacion de los campos." , e.getMessage());
			respuesta = "Ocurrio un error con la validacion de los campos.";
		}
		
		return respuesta;
	}
	
	private String validateFields(UsuarioRequest usuario, boolean tipo){
		String respuesta = null;
		
		try{
			respuesta = UtilsValidate.validateFields(usuario.getLogin(),"Usuario", new String[]{UtilsValidate.REQUIRED,UtilsValidate.MAXLENGTH + "15",UtilsValidate.MINLENGTH + "3"});
			
			respuesta += UtilsValidate.validateFields(usuario.getTelefono(),"Telefono", new String[]{UtilsValidate.REQUIRED,UtilsValidate.DIGITS,UtilsValidate.MAXLENGTH + "10"});
//			respuesta += UtilsValidate.validateFields(usuario.getTelefonoCon(),"Confirmar Telefono", new String[]{UtilsValidate.REQUIRED,UtilsValidate.DIGITS,UtilsValidate.MAXLENGTH + "10"});
//			respuesta += UtilsValidate.validateFields(usuario.getTelefono(),"Telefono", new String[]{UtilsValidate.EQUALTO + usuario.getTelefonoCon() + ":Confirmar Celuar"});
			
			respuesta += UtilsValidate.validateFields(usuario.getMail(),"Email", new String[]{UtilsValidate.REQUIRED,UtilsValidate.EMAIL,UtilsValidate.MAXLENGTH + "45"});
//			respuesta += UtilsValidate.validateFields(usuario.getEmailCon(),"Confirmar Email", new String[]{UtilsValidate.REQUIRED,UtilsValidate.EMAIL,UtilsValidate.MAXLENGTH + "45"});
//			respuesta += UtilsValidate.validateFields(usuario.getEmail(),"Email", new String[]{UtilsValidate.EQUALTO + usuario.getEmailCon() + ":Confirmar Email"});
			
			respuesta += UtilsValidate.validateFields(usuario.getNombre(),"Nombre(s)", new String[]{UtilsValidate.REQUIRED,UtilsValidate.MAXLENGTH + "45"});
			respuesta += UtilsValidate.validateFields(usuario.getApellido(),"Appellido Paterno", new String[]{UtilsValidate.REQUIRED,UtilsValidate.MAXLENGTH + "45"});
			respuesta += UtilsValidate.validateFields(usuario.getMaterno(),"Appellido Materno", new String[]{UtilsValidate.MAXLENGTH + "45"});
			
			respuesta += UtilsValidate.validateFields(usuario.getTipotarjeta()+"","Tipo Tarjeta", new String[]{UtilsValidate.REQUIRED,UtilsValidate.MAXLENGTH + "1"});
			respuesta += UtilsValidate.validateFields(usuario.getTerminos()+"","Terminos y Condiciones", new String[]{UtilsValidate.REQUIRED,UtilsValidate.MAXLENGTH + "1"});
			
			if(tipo){
				respuesta += UtilsValidate.validateFields(usuario.getTarjeta(),"Tarjeta", new String[]{UtilsValidate.REQUIRED,UtilsValidate.DIGITS,UtilsValidate.MAXLENGTH + "16",UtilsValidate.MINLENGTH + "15"});
				respuesta += UtilsValidate.validateFields(usuario.getVigencia(),"Vigencia", new String[]{UtilsValidate.REQUIRED,UtilsValidate.DATE + "MM/yyyy",UtilsValidate.MAXLENGTH + "7"});
			}
			
			if(StringUtils.isEmpty(respuesta)){
				respuesta = null;
			}
			
		}catch(Exception e){
			logger.error("Ocurrio un error con la validacion de los campos." , e.getMessage());
			respuesta = "Ocurrio un error con la validacion de los campos.";
		}
		
		return respuesta;
		
	}
	
	private String validateFieldsCambioPassword(LoginRequest cambioPassword){
		String respuesta = null;
		
		try{
			respuesta = UtilsValidate.validateFields(cambioPassword.getLogin(),"Usuario", new String[]{UtilsValidate.REQUIRED,UtilsValidate.MAXLENGTH + "15"});
			respuesta += UtilsValidate.validateFields(cambioPassword.getPassword(),"Password", new String[]{UtilsValidate.REQUIRED,UtilsValidate.MAXLENGTH + "16"});
			respuesta += UtilsValidate.validateFields(cambioPassword.getNewPassword(),"Nuevo Password", new String[]{UtilsValidate.REQUIRED,UtilsValidate.MAXLENGTH + "16"});
			
			if(StringUtils.isEmpty(respuesta)){
				respuesta = null;
			}
			
		}catch(Exception e){
			logger.error("Ocurrio un error con la validacion de los campos." , e.getMessage());
			respuesta = "Ocurrio un error con la validacion de los campos.";
		}
		
		return respuesta;
	}
	
	private String validateFieldsCambioTarjeta(CambioTarjeta cambioTarjeta){
		String respuesta = null;
		
		try{
			respuesta = UtilsValidate.validateFields(cambioTarjeta.getLogin(),"Usuario", new String[]{UtilsValidate.REQUIRED,UtilsValidate.MAXLENGTH + "15"});
			respuesta += UtilsValidate.validateFields(cambioTarjeta.getPassword(),"Password", new String[]{UtilsValidate.REQUIRED,UtilsValidate.MAXLENGTH + "16"});
			respuesta += UtilsValidate.validateFields(cambioTarjeta.getTarjeta(),"Nueva Tarjeta", new String[]{UtilsValidate.REQUIRED,UtilsValidate.MAXLENGTH + "16"});
			respuesta += UtilsValidate.validateFields(cambioTarjeta.getTarjetaCon(),"Nueva Tarjeta Con", new String[]{UtilsValidate.REQUIRED,UtilsValidate.MAXLENGTH + "16"});
			respuesta += UtilsValidate.validateFields(cambioTarjeta.getVigencia(),"Vigencia", new String[]{UtilsValidate.REQUIRED,UtilsValidate.MAXLENGTH + "7"});
			
			if(StringUtils.isEmpty(respuesta)){
				respuesta = null;
			}
			
		}catch(Exception e){
			logger.error("Ocurrio un error con la validacion de los campos." , e.getMessage());
			respuesta = "Ocurrio un error con la validacion de los campos.";
		}
		
		return respuesta;
	}
	
	private String validateFieldsCompra(SolicitudCompraVO compra){
		String respuesta = null;
		
		try{
//			respuesta = UtilsValidate.validateFields(String.valueOf(compra.getUsuario()),"Usuario", new String[]{UtilsValidate.REQUIRED});
//			respuesta += UtilsValidate.validateFields(String.valueOf(compra.getPassword()),"Password", new String[]{UtilsValidate.REQUIRED});
			respuesta = UtilsValidate.validateFields(String.valueOf(compra.getIdComercio()),"IdComercio", new String[]{UtilsValidate.REQUIRED_NUM});
			respuesta += UtilsValidate.validateFields(String.valueOf(compra.getToken()),"Token", new String[]{UtilsValidate.REQUIRED});
			respuesta += UtilsValidate.validateFields(compra.getCertificado(),"Certificado", new String[]{UtilsValidate.REQUIRED});
			
			if(StringUtils.isEmpty(respuesta)){
				respuesta = null;
			}
			
		}catch(Exception e){
			logger.error("Ocurrio un error con la validacion de los campos." , e.getMessage());
			respuesta = "Ocurrio un error con la validacion de los campos.";
		}
		
		return respuesta;
	}

	public String terminar(CompraVO compraVO, SolicitudCompraRespuestaVO transaccion, String jsonNotificacion) {
		String json = null;
		AbstractVO res = new AbstractVO();
		HashMap<String, String> data = null;
		try {
//			if(compraVO.getUrlConfirmacion() != null && !"".equals(compraVO.getUrlConfirmacion())){
//				data = new HashMap<String, String>();
//				data.put("idAplicacion", String.valueOf(compraVO.getIdAplicacion()));
//				data.put("respuesta", "");
//				mapper.jsontransaccion(data);
//				
//				String[] datos = data.get("respuesta").split(";");
//				if(!datos[0].equals("0")){
//					logger.error("Error al obtener claves no se puede notificar al Comercio: {}", datos[1]);
//				} else {
//					utilService.notificacionURLComercio(compraVO.getUrlComercio(), "json=" + jsonNotificacion);
//				}
//			}
		}catch (Exception pe) {
			res.setIdError(91);
			res.setMensajeError("Error General en el proceso de pago.");
			logger.error("Error General en el proceso de pago.: {}", pe.getMessage());
			
		} finally{
			json = utilService.objectToJson(res);
		}
		return json;
	}

	
	private void completarSolicitudCompra(SolicitudCompraVO soliCompra){
		try{
			soliCompra.setToken(AddcelCrypto.decrypt(UtilsConstants.KEY_TOKEN, soliCompra.getToken(), false));
			if(soliCompra.getModoPago() != 3 ){
				soliCompra.setVigencia("");
				soliCompra.setDigitoVer("");
			}
			if(soliCompra.getModoPago() == 3 && 
					(soliCompra.getReferenciaCJGS() == null || "".equals(soliCompra.getReferenciaCJGS()) || "null".equals(soliCompra.getReferenciaCJGS()))){
				soliCompra.setReferenciaCJGS(soliCompra.getNumeroControl());
			}
			
			if(soliCompra.getReferenciaCJGS() != null && "null".equals(soliCompra.getReferenciaCJGS())){
				soliCompra.setReferenciaCJGS(null);
			}
			
			if(soliCompra.getArrayMeses() != null && "null".equals(soliCompra.getArrayMeses())){
				soliCompra.setArrayMeses(null);
			}
		}catch(Exception e){
			soliCompra.setToken(null);
		}
		
	}
}
