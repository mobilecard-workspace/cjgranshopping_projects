package com.addcel.mobilecard.model.vo;

import java.util.Map;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class CompraVO extends AbstractVO{
	
	private String descripcionCompra;
	private String totalCompra;
	private Map<String, Object> productos;
	
	public String getDescripcionCompra() {
		return descripcionCompra;
	}
	public void setDescripcionCompra(String descripcionCompra) {
		this.descripcionCompra = descripcionCompra;
	}
	public String getTotalCompra() {
		return totalCompra;
	}
	public void setTotalCompra(String totalCompra) {
		this.totalCompra = totalCompra;
	}
	public Map<String, Object> getProductos() {
		return productos;
	}
	public void setProductos(Map<String, Object> productos) {
		this.productos = productos;
	}
	
}
