package com.addcel.mobilecard.model.vo.pagos;

import com.addcel.mobilecard.model.vo.PagoVO;
import com.addcel.mobilecard.model.vo.SolicitudCompraVO;

public class ProcomVO {
	//Atributos obligatorios
	private String total;
	private String currency;
	private String address;
	private String orderId;
	private String merchant;
	private String store;
	private String term;
	private String digest;
	private String urlBack;
	
	private String cc_name;
	private String cc_number;
	private String cc_type;
	private String cc_expmonth;
	private String cc_expyear;
	private String cc_cvv2;
	
		
//	public ProcomVO(String total, String currency, String address,
//			String orderId, String merchant, String store, String term,
//			String digest, String urlBack, String usuario, String idTramite, 
//			String monto, String idServicio, String campos) {
//		super();
//		this.total = total;
//		this.currency = currency;
//		this.address = address;
//		this.orderId = orderId;
//		this.merchant = merchant;
//		this.store = store;
//		this.term = term;
//		this.digest = digest;
//		this.urlBack = urlBack;
////		this.usuario = usuario;
////		this.monto = monto;
////		this.idServicio = idServicio;
//	}
	
	public ProcomVO(PagoVO pago, String total,
			String currency, String address,
			String orderId, String merchant, 
			String store, String term,
			String digest, String urlBack){
		super();
		this.total = total;
		this.currency = currency;
		this.address = address;
		this.orderId = orderId;
		this.merchant = merchant;
		this.store = store;
		this.term = term;
		this.digest = digest;
		this.urlBack = urlBack;
		
		this.cc_name = pago.getNombre();
		this.cc_number = pago.getTarjeta();
		this.cc_type = String.valueOf(pago.getTipoTarjeta());
		this.cc_expmonth = pago.getMes();
		this.cc_expyear = pago.getAnio();
		this.cc_cvv2 = pago.getCvv2();
		
	}

	public String getTotal() {
		return total;
	}

	public void setTotal(String total) {
		this.total = total;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getMerchant() {
		return merchant;
	}

	public void setMerchant(String merchant) {
		this.merchant = merchant;
	}

	public String getStore() {
		return store;
	}

	public void setStore(String store) {
		this.store = store;
	}

	public String getTerm() {
		return term;
	}

	public void setTerm(String term) {
		this.term = term;
	}

	public String getDigest() {
		return digest;
	}

	public void setDigest(String digest) {
		this.digest = digest;
	}

	public String getUrlBack() {
		return urlBack;
	}

	public void setUrlBack(String urlBack) {
		this.urlBack = urlBack;
	}

	public String getCc_name() {
		return cc_name;
	}

	public void setCc_name(String cc_name) {
		this.cc_name = cc_name;
	}

	public String getCc_number() {
		return cc_number;
	}

	public void setCc_number(String cc_number) {
		this.cc_number = cc_number;
	}

	public String getCc_type() {
		return cc_type;
	}

	public void setCc_type(String cc_type) {
		this.cc_type = cc_type;
	}

	public String getCc_expmonth() {
		return cc_expmonth;
	}

	public void setCc_expmonth(String cc_expmonth) {
		this.cc_expmonth = cc_expmonth;
	}

	public String getCc_expyear() {
		return cc_expyear;
	}

	public void setCc_expyear(String cc_expyear) {
		this.cc_expyear = cc_expyear;
	}

	public String getCc_cvv2() {
		return cc_cvv2;
	}

	public void setCc_cvv2(String cc_cvv2) {
		this.cc_cvv2 = cc_cvv2;
	}
	
//	public ProcomVO(String usuario,  String password, String total, String currency, 
//			String orderId, String merchant, 
//			String urlBack,  
//			String monto, String idServicio, //String campos,
//			String tarjeta, String vigencia, String tipo) {
//		super();
//		this.total = total;
//		this.currency = currency;
//		this.usuario = usuario;
//		this.password = password;
//		this.orderId = orderId;
//		this.merchant = merchant;
//		this.urlBack = urlBack;
//		this.monto = monto;
//		this.idServicio = idServicio;
//		this.tarjeta = tarjeta;
//		try{
//			if(vigencia .length() ==5 ){
//				this.mes = vigencia.substring(0, 2);
//				this.anio = vigencia.substring(3);
//			}
//		}catch(Exception e){
//			
//		}
//		
//		if(tipo != null && ("1".equals(tipo) || "2".equals(tipo))){
//			this.tipo = "VISA";
//		}else if(tipo != null && "3".equals(tipo)){
//			this.tipo = "MC";
//		}
//	}
	
}
