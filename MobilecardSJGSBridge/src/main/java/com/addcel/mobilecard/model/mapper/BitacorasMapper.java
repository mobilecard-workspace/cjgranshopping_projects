package com.addcel.mobilecard.model.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.addcel.mobilecard.model.vo.PaymentData;
import com.addcel.mobilecard.model.vo.pagos.TBitacoraProsaVO;
import com.addcel.mobilecard.model.vo.pagos.TBitacoraVO;

public interface BitacorasMapper {
	
	public int addBitacoraProsa(TBitacoraProsaVO b);
	
	public int addBitacora(TBitacoraVO b);
	
	public int updateBitacoraProsa(TBitacoraProsaVO b);
	
	public int updateBitacora(TBitacoraVO b);
	
	public int addJCGSDetalle(
			@Param(value="id_bitacora")long id_bitacora,
			@Param(value="id_usuario")long id_usuario,
			@Param(value="canal")int canal,
			@Param(value="modo_pago")int modo_pago,
			@Param(value="numero_control")String numero_control,
			@Param(value="email")String email,
			@Param(value="descripcion")String descripcion,
			@Param(value="nombre")String nombre,
			@Param(value="tarjeta")String tarjeta,
			@Param(value="tipoTarjeta")int tipoTarjeta,
			@Param(value="conmsi")int conmsi,
			@Param(value="plazo")int plazo,
			@Param(value="total")double total,
			@Param(value="comicion")double comicion,
			@Param(value="afiliacion")String afiliacion,
			@Param(value="referencia")String referencia,
			@Param(value="vigencia")String vigencia,
			@Param(value="digitover")String digitover);
	
	public int updateJCGSDetalle(
					@Param(value="idBitacora")long idBitacora,
					@Param(value="referencia")String referencia,
					@Param(value="status")int status,
					@Param(value="tipoTDC")String tipoTDC,
					@Param(value="numAutorizacion")String numAutorizacion,
					@Param(value="idAutorizacion")int idAutorizacion,
					@Param(value="tarjeta")String tarjeta);
	
	public List<PaymentData> buscarPagosJCGSDetalle(
			@Param(value="idBitacora")long idBitacora,
			@Param(value="numeroControl")String numeroControl,
			@Param(value="canal")int canal,
			@Param(value="modoPago")int modoPago);
	
	public int guardaTBitacoraPIN(
			@Param(value="idBitacora")long idBitacora,
			@Param(value="pin")String pin);
	
	public String buscarTBitacoraPIN(
			@Param(value="idBitacora")String idBitacora);
	
	public int borrarTBitacoraPIN(
			@Param(value="idBitacora")String idBitacora);
}
