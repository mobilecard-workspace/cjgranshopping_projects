package com.addcel.mobilecard.model.vo;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class RecurrenciaVO {

private int idRecurrencia;
	
	private long idTransaccionPago;
	
	private long idUsuario;
	
	private String fechaActivacion;
	
	private int idServicio;
	
	private String referencia;
	
	private String fechaActualizacion;
	
	private int diaPago;
	
	private int idAplicacion;
	
	private int status;
	
	private String codigoServicio;
	
	private int cuota; 
	
	private String descProducto;

	private int pagos;
	
	private int pagoActual;
	
	private int idProducto;
	
	private double monto;
	
	private int tipo;

	public int getIdRecurrencia() {
		return idRecurrencia;
	}

	public void setIdRecurrencia(int idRecurrencia) {
		this.idRecurrencia = idRecurrencia;
	}

	public long getIdTransaccionPago() {
		return idTransaccionPago;
	}

	public void setIdTransaccionPago(long idTransaccionPago) {
		this.idTransaccionPago = idTransaccionPago;
	}

	public long getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(long idUsuario) {
		this.idUsuario = idUsuario;
	}

	public String getFechaActivacion() {
		return fechaActivacion;
	}

	public void setFechaActivacion(String fechaActivacion) {
		this.fechaActivacion = fechaActivacion;
	}

	public int getIdServicio() {
		return idServicio;
	}

	public void setIdServicio(int idServicio) {
		this.idServicio = idServicio;
	}

	public String getReferencia() {
		return referencia;
	}

	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}

	public String getFechaActualizacion() {
		return fechaActualizacion;
	}

	public void setFechaActualizacion(String fechaActualizacion) {
		this.fechaActualizacion = fechaActualizacion;
	}

	public int getDiaPago() {
		return diaPago;
	}

	public void setDiaPago(int diaPago) {
		this.diaPago = diaPago;
	}

	public int getIdAplicacion() {
		return idAplicacion;
	}

	public void setIdAplicacion(int idAplicacion) {
		this.idAplicacion = idAplicacion;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getCodigoServicio() {
		return codigoServicio;
	}

	public void setCodigoServicio(String codigoServicio) {
		this.codigoServicio = codigoServicio;
	}

	public int getCuota() {
		return cuota;
	}

	public void setCuota(int cuota) {
		this.cuota = cuota;
	}

	public String getDescProducto() {
		return descProducto;
	}

	public void setDescProducto(String descProducto) {
		this.descProducto = descProducto;
	}

	public int getPagoActual() {
		return pagoActual;
	}

	public void setPagoActual(int pagoActual) {
		this.pagoActual = pagoActual;
	}

	public int getIdProducto() {
		return idProducto;
	}

	public void setIdProducto(int idProducto) {
		this.idProducto = idProducto;
	}

	public double getMonto() {
		return monto;
	}

	public void setMontoTotal(double monto) {
		this.monto = monto;
	}

	public int getPagos() {
		return pagos;
	}

	public void setPagos(int pagos) {
		this.pagos = pagos;
	}

	public void setMonto(double monto) {
		this.monto = monto;
	}

	public int getTipo() {
		return tipo;
	}

	public void setTipo(int tipo) {
		this.tipo = tipo;
	}
	
}
