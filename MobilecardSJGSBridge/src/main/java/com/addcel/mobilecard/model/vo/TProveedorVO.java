package com.addcel.mobilecard.model.vo;

public class TProveedorVO {
	private int idProveedor;
	private String nombre;
	private String usuario;
	private String password;
	private String clave3Desc;
	private int isVisa;
	private int isAmex;
	private int tipoComision;
	private double comiFija;
	private double comiPorc;
	private double montoMax;
	private double montoMin;
	
	private String afiliacion;
	private int status;
	private String secureCode;

	public int getIdProveedor() {
		return idProveedor;
	}

	public void setIdProveedor(int idProveedor) {
		this.idProveedor = idProveedor;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getClave3Desc() {
		return clave3Desc;
	}

	public void setClave3Desc(String clave3Desc) {
		this.clave3Desc = clave3Desc;
	}

	public int getIsVisa() {
		return isVisa;
	}

	public void setIsVisa(int isVisa) {
		this.isVisa = isVisa;
	}

	public int getIsAmex() {
		return isAmex;
	}

	public void setIsAmex(int isAmex) {
		this.isAmex = isAmex;
	}

	public int getTipoComision() {
		return tipoComision;
	}

	public void setTipoComision(int tipoComision) {
		this.tipoComision = tipoComision;
	}

	public double getComiFija() {
		return comiFija;
	}

	public void setComiFija(double comiFija) {
		this.comiFija = comiFija;
	}

	public double getComiPorc() {
		return comiPorc;
	}

	public void setComiPorc(double comiPorc) {
		this.comiPorc = comiPorc;
	}

	public double getMontoMax() {
		return montoMax;
	}

	public void setMontoMax(double montoMax) {
		this.montoMax = montoMax;
	}

	public double getMontoMin() {
		return montoMin;
	}

	public void setMontoMin(double montoMin) {
		this.montoMin = montoMin;
	}

	public String getAfiliacion() {
		return afiliacion;
	}

	public void setAfiliacion(String afiliacion) {
		this.afiliacion = afiliacion;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getSecureCode() {
		return secureCode;
	}

	public void setSecureCode(String secureCode) {
		this.secureCode = secureCode;
	}
	
	
}
