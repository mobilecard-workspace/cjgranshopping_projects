package com.addcel.mobilecard.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

import com.addcel.mobilecard.model.vo.CompraVO;
import com.addcel.mobilecard.model.vo.LoginRequest;
import com.addcel.mobilecard.model.vo.PagoVO;
import com.addcel.mobilecard.model.vo.SolicitudCompraRespuestaVO;
import com.addcel.mobilecard.model.vo.UsuarioRequest;
import com.addcel.mobilecard.model.vo.UsuarioVO;
import com.addcel.mobilecard.services.MobilecardPagoService;
import com.addcel.mobilecard.services.MobilecardService;
import com.addcel.mobilecard.utils.UtilsConstants;

@Controller
@SessionAttributes({"usuario","soliCompra", "soliCompraResp", "tproveedor", "pago"})
public class MobilecardWebController {
	
	private static final Logger logger = LoggerFactory.getLogger(MobilecardWebController.class);
	
	
	@Autowired
	private MobilecardPagoService mcPagosService;
	
	@Autowired
	private MobilecardService addcelService;
	
//	@RequestMapping(value = "/", method=RequestMethod.GET)
//	public ModelAndView home() {	
//		logger.info("Dentro del servicio: /");
//		ModelAndView mav = new ModelAndView("home");
//		return mav;	
//	}
	
	@RequestMapping(value = "/portal/getToken"/*, method=RequestMethod.POST*/)
	public @ResponseBody String getToken(@RequestParam("uparam") String jsonEnc, ModelMap modelo, HttpSession session) {	
		logger.debug("Dentro del servicio: /portal/getToken");
		return addcelService.getToken(jsonEnc);	
	}
	
	@RequestMapping(value = "/portal/consultaMSI/data"/*, method=RequestMethod.POST*/)
	public @ResponseBody String cambioDatosJson(@RequestBody String tarjeta, ModelMap modelo) {
		return mcPagosService.consultaMSI(tarjeta, modelo);
	}
	
	@RequestMapping(value = "/portal/pago-inicio"/*, method=RequestMethod.POST*/)
	public ModelAndView pagoInicio(@RequestParam("uparam") String jsonEnc,  ModelMap modelo, HttpSession session) {	
		logger.debug("Dentro del servicio: /portal/pago-inicio");
		return addcelService.pagoInicio(jsonEnc,  modelo, session);	
	}
	
	@RequestMapping(value = "/portal/procesa-pago"/*, method=RequestMethod.POST*/)
	public ModelAndView procesaPago(@ModelAttribute PagoVO pago, ModelMap modelo) {
		logger.debug("Dentro del servicio: /portal/procesa-pago");
		return mcPagosService.procesaPagoUsuario(pago, modelo);	
	}
	
	@RequestMapping(value = "/portal/procesa-pago-directo"/*, method=RequestMethod.POST*/)
	public ModelAndView procesaPagoDirecto(@ModelAttribute PagoVO pago, ModelMap modelo) {
		logger.debug("Dentro del servicio: /portal/procesa-pago-directo");
		return mcPagosService.procesaPagoDirecto(pago, modelo);	
	}
	
	@RequestMapping(value = "/portal/procesa-pago-meses"/*, method=RequestMethod.POST*/)
	public ModelAndView procesaPagoMeses(@ModelAttribute PagoVO pago, ModelMap modelo) {
		logger.debug("Dentro del servicio: /portal/procesa-pago-meses");
		return mcPagosService.procesaPagoMeses(pago, modelo);	
	}
	
	@RequestMapping(value = "/portal/pago-resultado"/*, method=RequestMethod.POST*/)
	public ModelAndView pagoResultado(@RequestParam("uparam") String jsonEnc, ModelMap modelo) {
		logger.debug("Dentro del servicio: /portal/pago-resultado");
		return mcPagosService.pagoResultado(modelo);	
	}
	
//	@RequestMapping(value = "/portal/pagina-prosa"/*, method = RequestMethod.POST*/)
//	public String paginaTestProsa(ModelMap modelo) {
//		logger.info("Dentro del servicio: /portal/pagina-prosa");
//		
////		SolicitudCompraRespuestaVO soliCompraResp = new SolicitudCompraRespuestaVO();
////		SolicitudCompraVO soliCompra = null;
////		PagoVO pago = null;
////		UsuarioVO usuario = null;
//
////		try{
//////			usuario = (UsuarioVO) modelo.get("usuario");
//////			soliCompra = (SolicitudCompraVO) modelo.get("soliCompra");
//////			pago = (PagoVO) modelo.get("pago");
////			soliCompraResp = new SolicitudCompraRespuestaVO();
////		}catch(Exception e){
////			
////		}
//		return "portal/pagina-prosa";	
//	}
	
	@RequestMapping(value = "/portal/comercio-con"/*, method = RequestMethod.POST*/)
	public ModelAndView respuestaProsa(@RequestParam String EM_Response,
			@RequestParam String EM_Total, @RequestParam String EM_OrderID,
			@RequestParam String EM_Merchant, @RequestParam String EM_Store,
			@RequestParam String EM_Term, @RequestParam String EM_RefNum,
			@RequestParam String EM_Auth, @RequestParam String EM_Digest, 
			@RequestParam(required = false) String cc_number, ModelMap modelo) {
		logger.info("Dentro del servicio: /portal/comercio-con");
		return mcPagosService.procesaRespuestaPago3DSecure(EM_Response, EM_Total, EM_OrderID, EM_Merchant, 
				EM_Store, EM_Term, EM_RefNum, EM_Auth, EM_Digest, cc_number, modelo);	
	}
	
	@RequestMapping(value = "/payworksRec3DRespuesta"/*, method = RequestMethod.GET*/)
	public ModelAndView payworksRec3DRespuesta(@RequestBody String cadena, ModelMap modelo) {
		logger.info("Dentro del servicio: /payworksRec3DRespuesta");
		return mcPagosService.procesaRespuesta3DPayworks(cadena, modelo);	
	}
	
	@RequestMapping(value = "/payworks2RecRespuesta"/*, method = RequestMethod.POST*/)
	public ModelAndView payworks2RecRespuesta(
			@RequestParam String NUMERO_CONTROL,
			@RequestParam(required = false) String REFERENCIA, @RequestParam String FECHA_RSP_CTE,
			@RequestParam(required = false) String CODIGO_PAYW,
			@RequestParam String TEXTO, @RequestParam String RESULTADO_PAYW,
			@RequestParam String FECHA_REQ_CTE, @RequestParam(required = false) String CODIGO_AUT,
			@RequestParam String ID_AFILIACION, ModelMap modelo) {
		logger.info("Dentro del servicio: /payworks2RecRespuesta");
		return mcPagosService.payworks2Respuesta(NUMERO_CONTROL,REFERENCIA, FECHA_RSP_CTE, 
				TEXTO, RESULTADO_PAYW, FECHA_REQ_CTE, CODIGO_AUT, CODIGO_PAYW,ID_AFILIACION, modelo);	
	}
	
	@RequestMapping(value = "/portal/login-end"/*, method=RequestMethod.POST*/)
	public ModelAndView loginEnd(@ModelAttribute("login") LoginRequest login, ModelMap modelo) {
		logger.info("Dentro del servicio: /portal/login-end");
		return addcelService.loginEnd(login, modelo);	
	}
	
	@RequestMapping(value = "/portal/pago-error"/*, method=RequestMethod.POST*/)
	public ModelAndView pagoError(ModelMap modelo) {
		logger.info("Dentro del servicio: /portal/pago-error");
		ModelAndView mav = new ModelAndView("portal/pago-error");
		mav.addObject("mensajeError", UtilsConstants.DESC_ERROR_FINSESION);
		return mav;	
	}
	
	@RequestMapping("/portal/logout")
	public ModelAndView logout(SessionStatus status, ModelMap modelo) {
		ModelAndView mav = null;
		try{
			status.setComplete();
			modelo.remove("usuario");
			modelo.remove("soliCompra");
			logger.debug("Cerrando session y redireccionando a la pagina de login." );
//			mav = new ModelAndView("portal/login", "usuario", new LoginVO());
		}catch(Exception e){
			logger.error("Ocurrio un error general al cerrar session: {}", e );
			mav = new ModelAndView("portal/pago-error");
			mav.addObject("mensaje", "Ocurrio un error general: " + e.getMessage());
		}

		return mav;
	}
	
	@RequestMapping(value = "/portal/registro"/*, method=RequestMethod.POST*/)
	public ModelAndView registro(@RequestParam("uparam") String jsonEnc, @RequestParam("ucom") String comercio, ModelMap modelo) {	
		logger.info("Dentro del servicio: /portal/registro");
		
		return addcelService.registro(jsonEnc, comercio, modelo);	
	}
	
	@RequestMapping(value = "/portal/registro-end/json"/*, method=RequestMethod.POST*/)
	public @ResponseBody String  registroFinal(@RequestBody UsuarioRequest usuario, ModelMap modelo) {	
		logger.info("Dentro del servicio: /portal/registro-end");
		
		return addcelService.registroFinal(usuario, modelo);
	}
	
//	@RequestMapping(value = "/portal/cambioPassword"/*, method=RequestMethod.POST*/)
//	public ModelAndView cambioPassword( @ModelAttribute("cambioPassword") CambioPassword cambioPassword) {
//		logger.info("Dentro del servicio: /cambioPassword");
//		ModelAndView mav = null;
//		try{
//			logger.debug("Inicia pantalla , cambioPassword." );
//			mav = new ModelAndView("cambioPassword", "cambioPassword", cambioPassword);
//		}catch(Exception e){
//			logger.error("Ocurrio un error general en el servicio /cambioPassword: {}", e );
//		}
//		return mav;	
//	}
//	
	@RequestMapping(value = "/portal/pago-cambio-pass-end"/*, method=RequestMethod.POST*/)
	public ModelAndView cambioPasswordFinal( @ModelAttribute("cambioPassword") LoginRequest cambioPassword, ModelMap modelo) {
		logger.info("Dentro del servicio: /portal/pago-cambio-pass-end");
		return addcelService.cambioPassword(cambioPassword, modelo);	
	}
	
	
	@RequestMapping(value = "/portal/pago-cambio-password"/*, method=RequestMethod.POST*/)
	public ModelAndView cambioPasswordJson(@ModelAttribute("cambPass") LoginRequest cambioPassword, ModelMap modelo) {
		logger.info("Dentro del servicio: /portal/pago-cambio-Password");
		return addcelService.cambioPasswordFinal(cambioPassword, modelo);	
	}
	
	@RequestMapping(value = "/portal/resetPassword"/*, method=RequestMethod.POST*/)
	public ModelAndView resetPasswordJson(@ModelAttribute LoginRequest cambioPassword, ModelMap modelo) {
		logger.info("Dentro del servicio: /portal/resetPassword");
		return addcelService.resetPassword(cambioPassword, modelo);
	}
	
	@RequestMapping(value = "/portal/cambioTarjeta/json"/*, method=RequestMethod.POST*/)
	public @ResponseBody String cambioTarjetaJson(@RequestBody List<Map<String, String>> data, ModelMap modelo) {
		logger.info("Dentro del servicio: /cambioTarjetaJson");
		UsuarioVO usuarioVO = null;
		String respuesta = null;
		try{
			usuarioVO = (UsuarioVO) modelo.get("usuario");
			
			Map<String, String> formInputs = new HashMap<String, String>();
			 
			for (Map<String, String> formInput : data) {
				 formInputs.put(formInput.get("name"), formInput.get("value"));
			}
			
			if(formInputs.containsKey("passwordTarj")){
				formInputs.put("password", formInputs.get("passwordTarj"));
			}
			if(formInputs.containsKey("newTarjeta")){
				formInputs.put("tarjeta", formInputs.get("newTarjeta"));
			}
			if(formInputs.containsKey("newTarjetaCon")){
				formInputs.put("tarjetaCon", formInputs.get("newTarjetaCon"));
			}
			
			respuesta = addcelService.cambioTarjetaJson(usuarioVO, formInputs);
			logger.debug("cambioTarjetaJson respuesta Json: {}", respuesta);
			
		}catch(Exception e){
			logger.error("Ocurrio un error general en /cambioTarjetaJson: {}", e);
		}
		return respuesta;	
	}
	
	@RequestMapping(value = "/portal/cambioDatos/json"/*, method=RequestMethod.POST*/)
	public @ResponseBody String cambioDatosJson(@RequestBody List<Map<String, String>> data, ModelMap modelo) {
		logger.info("Dentro del servicio: /cambioDatos/json");
		UsuarioVO usuarioVO = null;
		String respuesta = null;
		try{
			usuarioVO = (UsuarioVO) modelo.get("usuario");
			
			Map<String, String> formInputs = new HashMap<String, String>();
			 
			for (Map<String, String> formInput : data) {
				 formInputs.put(formInput.get("name"), formInput.get("value"));
			}
			
			respuesta = addcelService.cambioDatosJson(usuarioVO, formInputs);
			logger.debug("cambioDatosJson respuesta Json: {}", respuesta);
			
			
		}catch(Exception e){
			logger.error("Ocurrio un error general en /cambioDatosJson: {}", e);
		}
		return respuesta;	
	}
	
	
	
	
	@RequestMapping(value = "/terminar"/*, method=RequestMethod.POST*/)
	public String terminar(ModelMap modelo) {	
		logger.info("Dentro del servicio: /terminar");
		SolicitudCompraRespuestaVO res = null;
		CompraVO compraVO = null;
		String respuesta = null;
		String redirect = null;
		try{
			compraVO = (CompraVO) modelo.get("compra");	
//			if(compraVO.getUrlConfirmacion() != null){
//				String json = (String) modelo.get("jsonNotificacion");
//				res = (SolicitudCompraRespuesta) modelo.get("respuestaTransaccion");	
//				respuesta = addcelService.terminar(compraVO, res, json);
//				redirect = "redirect:"+compraVO.getUrlConfirmacion();
//				logger.debug("terminar respuesta Json: {}", respuesta);
//			} else {
//				redirect = "";
//			}
		}catch(Exception e){
			logger.error("Ocurrio un error general en /terminar: {}", e);
		}
		modelo.clear();
		return 	redirect;
	}
	
//	@RequestMapping(value = "/testURL"/*, method=RequestMethod.POST*/)
//	public @ResponseBody String testURL(ModelMap modelo, HttpServletRequest request) {	
//		logger.info("Dentro del servicio: /testURL");
//		CompraVO compraVO = null;
//		try{
//			compraVO = (CompraVO) modelo.get("compra");
//			Enumeration e = request.getAttributeNames();
//			while(e.hasMoreElements()){
//				logger.info(e.nextElement().toString());
//			}
//			logger.info("values:"+modelo.values());
//			logger.debug("terminar respuesta Json: {}");
//		}catch(Exception e){
//			logger.error("Ocurrio un error general en /terminar: {}", e);
//		}
//		return "OK, Lectura correcta";	
//	}
	
}