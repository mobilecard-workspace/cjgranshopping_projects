package com.addcel.cjgs.bridge.ws.servicios.security;

import java.io.IOException;

import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.UnsupportedCallbackException;

import org.apache.ws.security.WSPasswordCallback;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.addcel.cjgs.bridge.model.dao.CoreDAO;
import com.addcel.cjgs.bridge.utils.ibatis.service.AbstractService;
import com.addcel.utils.AddcelCrypto;

public class PassWordCallbackBridge extends AbstractService implements CallbackHandler  {
	
	private static final Logger logger = LoggerFactory.getLogger(PassWordCallbackBridge.class);

	public void handle(Callback[] callbacks) throws IOException,
			UnsupportedCallbackException {
		CoreDAO dao = null;
		String password = null;
//		logger.debug("********************************************");
//		logger.debug("callbacks: " + callbacks.length );
//		logger.debug("********************************************");
		for (int i = 0; i < callbacks.length; i++) {  
            //Autenticación del usuario y contraseña  
            WSPasswordCallback pwcb = (WSPasswordCallback)callbacks[i];  
            //comprobaciones de usuario y contraseña para acceso al servicio
            logger.debug("********************************************");
    		logger.debug("user: " + pwcb.getIdentifier());
    		logger.debug("pass: " + pwcb.getPassword());
    		logger.debug("********************************************");
    		
    		try {
				dao = (CoreDAO) getBean("CoreDAO");
				password =  dao.getTProveedorPassword(pwcb.getIdentifier());
				pwcb.setPassword(AddcelCrypto.encryptPassword(pwcb.getPassword()));
			} catch (Exception e) {
				logger.error("Error al obtener el Password: {}", e);
			}
    		
            if(password != null && pwcb.getPassword().equals(password)) {  
                //autenticación con éxito  
                return;  
            } else {  
                throw new UnsupportedCallbackException(callbacks[i], "The security User could not be authenticated or authorized.");  
            }  
        }  
		
	}
}