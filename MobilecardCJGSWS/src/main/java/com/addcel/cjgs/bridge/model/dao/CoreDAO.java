package com.addcel.cjgs.bridge.model.dao;

import org.apache.log4j.Logger;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.addcel.cjgs.bridge.model.vo.SmtpVO;

public class CoreDAO extends SqlMapClientDaoSupport{
		
	private static final Logger log = Logger.getLogger(CoreDAO.class);

	public String getTProveedorPassword(String token ){
		String resp = null;
		try{
			resp = (String) getSqlMapClientTemplate().queryForObject("getTProveedorPassword", token);
		}catch(Exception e){
			//resp = "Occurrio un error en la BD: " + e.getMessage();
			log.error("Ocurrio un error en la BD al generar Token. ", e);
		}
		return resp;
	}
	
	public String getTProveedor(){
		String resp = null;
		try{
			resp = (String) getSqlMapClientTemplate().queryForObject("getTProveedor", null);
		}catch(Exception e){
			log.error("Ocurrio un error en la BD al obtenre pass 3Ddesc ", e);
		}
		return resp;
	}

	public SmtpVO obtenDatosMail(int idProveedor){
		SmtpVO resp = null;
		try{
			resp = (SmtpVO) getSqlMapClientTemplate().queryForObject("obtenDatosMail", idProveedor);
		}catch(Exception e){
			log.error("Ocurrio un error en la BD al obtenDatosMail. ", e);
		}
		return resp;
	}
}
