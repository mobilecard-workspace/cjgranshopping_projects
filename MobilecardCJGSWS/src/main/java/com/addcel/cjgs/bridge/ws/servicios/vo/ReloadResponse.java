package com.addcel.cjgs.bridge.ws.servicios.vo;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.1</a>, using an XML
 * Schema.
 * $Id: ReloadResponse.java,v 1.1 2015/06/15 17:15:53 jlopez Exp $
 */

/**
 * Class ReloadResponse.
 * 
 * @version $Revision: 1.1 $ $Date: 2015/06/15 17:15:53 $
 */
@SuppressWarnings("serial")
@XmlRootElement(name = "ReloadResponse")
public class ReloadResponse implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _FOLIO.
     */
    private String _FOLIO;

    /**
     * Field _ID_TERMINAL.
     */
    private String _ID_TERMINAL;


    /**
     * Field _LOCAL_DATE.
     */
    private java.lang.String _LOCAL_DATE;

    /**
     * Field _NOAUTO.
     */
    private String _NOAUTO;

    /**
     * Field _AMOUNT.
     */
    private double _AMOUNT;


    /**
     * Field _TRX_NO.
     */
    private String _TRX_NO;


    /**
     * Field _RESPONSECODE.
     */
    private int _RESPONSECODE;

    /**
     * Field _DESCRIPTIONCODE.
     */
    private java.lang.String _DESCRIPTIONCODE;


      //----------------/
     //- Constructors -/
    //----------------/

    public ReloadResponse() {
        super();
    }


      //-----------/
     //- Methods -/
    //-----------/



    /**
     * Returns the value of field 'DESCRIPTIONCODE'.
     * 
     * @return the value of field 'DESCRIPTIONCODE'.
     */
    @XmlElement(name = "DESCRIPTIONCODE")
    public java.lang.String getDESCRIPTIONCODE(
    ) {
        return this._DESCRIPTIONCODE;
    }

    /**
     * Returns the value of field 'FOLIO'.
     * 
     * @return the value of field 'FOLIO'.
     */
    @XmlElement(name = "FOLIO")
    public String getFOLIO(
    ) {
        return this._FOLIO;
    }

    /**
     * Returns the value of field 'ID_TERMINAL'.
     * 
     * @return the value of field 'ID_TERMINAL'.
     */
    @XmlElement(name = "ID_TERMINAL")
    public String getID_TERMINAL(
    ) {
        return this._ID_TERMINAL;
    }

    /**
     * Returns the value of field 'LOCAL_DATE'.
     * 
     * @return the value of field 'LOCAL_DATE'.
     */
    @XmlElement(name = "LOCAL_DATE")
    public java.lang.String getLOCAL_DATE(
    ) {
        return this._LOCAL_DATE;
    }

    /**
     * Returns the value of field 'NOAUTO'.
     * 
     * @return the value of field 'NOAUTO'.
     */
    @XmlElement(name = "NOAUTO")
    public String getNOAUTO(
    ) {
        return this._NOAUTO;
    }

    /**
     * Returns the value of field 'RESPONSECODE'.
     * 
     * @return the value of field 'RESPONSECODE'.
     */
    @XmlElement(name = "RESPONSECODE")
    public int getRESPONSECODE(
    ) {
        return this._RESPONSECODE;
    }

    /**
     * Returns the value of field 'TRX_NO'.
     * 
     * @return the value of field 'TRX_NO'.
     */
    @XmlElement(name = "TRX_NO")
    public String getTRX_NO(
    ) {
        return this._TRX_NO;
    }

    /**
     * Sets the value of field 'AMOUNT'.
     * 
     * @param AMOUNT the value of field 'AMOUNT'.
     */
    public void setAMOUNT(
            double AMOUNT) {
        this._AMOUNT = AMOUNT;
    }

    /**
     * Sets the value of field 'DESCRIPTIONCODE'.
     * 
     * @param DESCRIPTIONCODE the value of field 'DESCRIPTIONCODE'.
     */
    public void setDESCRIPTIONCODE(
            java.lang.String DESCRIPTIONCODE) {
        this._DESCRIPTIONCODE = DESCRIPTIONCODE;
    }

    /**
     * Sets the value of field 'FOLIO'.
     * 
     * @param FOLIO the value of field 'FOLIO'.
     */
    public void setFOLIO(
    		String FOLIO) {
        this._FOLIO = FOLIO;
    }

    /**
     * Sets the value of field 'ID_TERMINAL'.
     * 
     * @param ID_TERMINAL the value of field 'ID_TERMINAL'.
     */
    public void setID_TERMINAL(
    		String ID_TERMINAL) {
        this._ID_TERMINAL = ID_TERMINAL;
    }

    /**
     * Sets the value of field 'LOCAL_DATE'.
     * 
     * @param LOCAL_DATE the value of field 'LOCAL_DATE'.
     */
    public void setLOCAL_DATE(
            java.lang.String LOCAL_DATE) {
        this._LOCAL_DATE = LOCAL_DATE;
    }

    /**
     * Sets the value of field 'NOAUTO'.
     * 
     * @param NOAUTO the value of field 'NOAUTO'.
     */
    public void setNOAUTO(
    		String NOAUTO) {
        this._NOAUTO = NOAUTO;
    }

    /**
     * Sets the value of field 'RESPONSECODE'.
     * 
     * @param RESPONSECODE the value of field 'RESPONSECODE'.
     */
    public void setRESPONSECODE(
            int RESPONSECODE) {
        this._RESPONSECODE = RESPONSECODE;
    }

    /**
     * Sets the value of field 'TRX_NO'.
     * 
     * @param TRX_NO the value of field 'TRX_NO'.
     */
    public void setTRX_NO(
    		String TRX_NO) {
        this._TRX_NO = TRX_NO;
    }

}
