package com.addcel.cjgs.bridge.utils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.security.GeneralSecurityException;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class UtilsService {
	private static final Logger logger = LoggerFactory.getLogger(UtilsService.class);
	
	
	public String notificacionURLComercio(String urlServicio, String parametros) {
		if(urlServicio.startsWith("https")){
			logger.info("Mandando notificacion de compra por https");
			return notificacionHttpsURLComercio(urlServicio, parametros, true);
		} else {
			logger.info("Mandando notificacion de compra por http...");
			return notificacionHttpURLComercio(urlServicio, parametros, true);
		}
	}
	
	public String notificacionURLComercio(String urlServicio, String parametros, boolean method) {
		if(urlServicio.startsWith("https")){
			logger.info("Mandando notificacion de compra por https");
			return notificacionHttpsURLComercio(urlServicio, parametros, method);
		} else {
			logger.info("Mandando notificacion de compra por http...");
			return notificacionHttpURLComercio(urlServicio, parametros, method);
		}
	}
	
	public String notificacionHttpURLComercio(String urlServicio, String parametros, boolean method) {
        InputStream is = null;
        OutputStream os = null;
        InputStreamReader isr = null;
        BufferedWriter writer = null;
        BufferedReader br = null;
        HttpURLConnection conn = null;
        URL url = null;
        String inputLine = null;
        StringBuffer sbf = new StringBuffer();
        try {
        	logger.debug("URL Notificacion: {}", urlServicio);
        	logger.debug("Param Notificacion: {}", parametros);
            url = new URL(urlServicio);
            conn = (HttpURLConnection) url.openConnection();
            if (conn != null) {
                conn.setConnectTimeout(20000);
                conn.setRequestMethod(method? "POST": "GET");
                conn.setDoInput(true);
                if (parametros != null) {
                    conn.setDoOutput(true);
                    os = conn.getOutputStream();
                    writer = new BufferedWriter(new OutputStreamWriter(os));
                    writer.write(parametros);
                    writer.close();
                    os.close();
                    conn.connect();
                }
                is = conn.getInputStream();
                isr = new InputStreamReader(is);
                br = new BufferedReader(isr);
                while ((inputLine = br.readLine()) != null) {
                    sbf.append(inputLine);
                }
                logger.debug("Respuesta notificacion: {}", sbf);
            }
        } catch (Exception e) {
         logger.error("Error al leer la url: {},    {} ", url, e);
        } finally {
            if (conn != null) {
                try {
                    conn.disconnect();
                } catch (Exception e) {
                 logger.error("Exception : ", e);
                }
            }
            if (br != null) {
                try {
                    br.close();
                } catch (Exception e) {
                 logger.error("Exception : ", e);
                }
            }

            if (isr != null) {
                try {
                    isr.close();
                } catch (Exception e) {
                 logger.error("Exception : ", e);
                }
            }
            if (is != null) {
                try {
                    is.close();
                } catch (Exception e) {
                 logger.error("Exception : ", e);
                }
            }
        }
        return sbf.toString();
    }
	
	public String notificacionHttpsURLComercio(String urlServicio, String parametros, boolean method) {
		InputStream is = null;
        OutputStream os = null;
        InputStreamReader isr = null;
        BufferedWriter writer = null;
        BufferedReader br = null;
        HttpURLConnection conn = null;
		URL url = null;
		String inputLine = null;
        StringBuffer sbf = new StringBuffer();
        
		HostnameVerifier hv = new HostnameVerifier() {
			public boolean verify(String urlHostName, SSLSession session) {
				logger.debug("Warning: URL Host: " + urlHostName + " vs. " + session.getPeerHost());
				return true;
			}
		};	        
	    
        try {
        	trustAllHttpsCertificates();
    	    HttpsURLConnection.setDefaultHostnameVerifier(hv);
    	    
        	logger.debug("URL Notificacion: {}", urlServicio);
        	logger.debug("Param Notificacion: {}", parametros);

            url = new URL(urlServicio);
            conn = (HttpURLConnection) url.openConnection();
            if (conn != null) {
                conn.setConnectTimeout(20000);
                conn.setRequestMethod(method? "POST": "GET");
                conn.setDoInput(true);
                if (parametros != null) {
                    conn.setDoOutput(true);
                    os = conn.getOutputStream();
                    writer = new BufferedWriter(new OutputStreamWriter(os));
                    writer.write(parametros);
                    writer.close();
                    os.close();
                    conn.connect();
                }
                is = conn.getInputStream();
                isr = new InputStreamReader(is);
                br = new BufferedReader(isr);
                while ((inputLine = br.readLine()) != null) {
                    sbf.append(inputLine);
                }
                logger.debug("Respuesta notificacion: {}", sbf);
            }
        } catch (GeneralSecurityException e) {
        	logger.error("Error Al generar certificados: {}" , e);
        }catch (MalformedURLException e) {
        	logger.error("error al conectar con la URL: {},    {}", urlServicio, e);
        }catch (ProtocolException e) {
        	logger.error("Error al enviar datos: {}" , e);
		} catch (IOException e) {
			logger.error("Error: {}" , e);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
            if (br != null) {
                try {
                    br.close();
                } catch (Exception e) {
                 logger.error("Exception : ", e);
                }
            }

            if (isr != null) {
                try {
                    isr.close();
                } catch (Exception e) {
                 logger.error("Exception : ", e);
                }
            }
            if (is != null) {
                try {
                    is.close();
                } catch (Exception e) {
                 logger.error("Exception : ", e);
                }
            }
            if (conn != null) {
                try {
                    conn.disconnect();
                } catch (Exception e) {
                 logger.error("Exception : ", e);
                }
            }
        }
        return sbf.toString(); 
	}
	
	public static class miTM implements javax.net.ssl.TrustManager,
			javax.net.ssl.X509TrustManager {
		public java.security.cert.X509Certificate[] getAcceptedIssuers() {
			return null;
		}

		public boolean isServerTrusted(
				java.security.cert.X509Certificate[] certs) {
			return true;
		}

		public boolean isClientTrusted(
				java.security.cert.X509Certificate[] certs) {
			return true;
		}

		public void checkServerTrusted(
				java.security.cert.X509Certificate[] certs, String authType)
				throws java.security.cert.CertificateException {
			return;
		}

		public void checkClientTrusted(
				java.security.cert.X509Certificate[] certs, String authType)
				throws java.security.cert.CertificateException {
			return;
		}
	}

	private static void trustAllHttpsCertificates() throws Exception {
		javax.net.ssl.TrustManager[] trustAllCerts =
		new javax.net.ssl.TrustManager[1];
		javax.net.ssl.TrustManager tm = new miTM();
		trustAllCerts[0] = tm;
		javax.net.ssl.SSLContext sc =
		javax.net.ssl.SSLContext.getInstance("SSL");
		sc.init(null, trustAllCerts, null);
		javax.net.ssl.HttpsURLConnection.setDefaultSSLSocketFactory(
		sc.getSocketFactory());
	}

}
