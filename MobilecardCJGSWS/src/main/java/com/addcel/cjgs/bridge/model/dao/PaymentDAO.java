package com.addcel.cjgs.bridge.model.dao;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.addcel.cjgs.bridge.model.vo.DetalleVO;
import com.addcel.cjgs.bridge.model.vo.pagos.TBitacoraProsaVO;
import com.addcel.cjgs.bridge.model.vo.pagos.TBitacoraVO;
import com.addcel.cjgs.bridge.ws.servicios.vo.ReloadRequest;

public class PaymentDAO extends SqlMapClientDaoSupport{
		
	private static final Logger log = LoggerFactory.getLogger(PaymentDAO.class);
	
	
	public DetalleVO selectCJGSDetalle(ReloadRequest request){
		DetalleVO resp = null;
		HashMap<String, Object> param = new HashMap<String, Object>();
		try{
			param.put("numeroControl", request.getFOLIO());
			param.put("monto", request.getAMOUNT());
			resp = (DetalleVO) getSqlMapClientTemplate().queryForObject("selectCJGSDetalle", param);
		}catch(Exception e){
//			resp = "Occurrio un error en la BD: " + e.getMessage();
			log.error("Ocurrio un error en la BD al obtener:transactionId ", e);
		}
		return resp;
	}
	
	
	public int updateCJGSDetalle(long idBitacora, int status, String numAutho) {
		HashMap<String, Object> param = new HashMap<String, Object>();
		param.put("idBitacora", idBitacora);
		param.put("status", status);
		param.put("authorizationNumber", numAutho);
		
			
		return (Integer )getSqlMapClientTemplate().update("updateCJGSDetalle", param);
	}
	
		
	public int updateTBitacora(TBitacoraVO bitacora) {
		Integer idBitacora = 0; 
		
		log.info("Datos de la Bitacora: " + bitacora);
		idBitacora = (Integer )getSqlMapClientTemplate().update("updateTBitacora", bitacora);

		return idBitacora;
	}
	
	public int updateTBitacoraProsa(TBitacoraProsaVO bitacora) {
		Integer idBitacora = 0; 
		
		log.info("Datos de la Bitacora Prosa: " + bitacora);
		idBitacora = (Integer )getSqlMapClientTemplate().update("updateTBitacoraProsa", bitacora);

		return idBitacora;
	}
	
	public void insertBitacoraReverso(int idBitacora) {
		
		log.info("Datos de la insertBitacora Reverso: " + idBitacora);
		getSqlMapClientTemplate().insert("insertBitacoraReverso", idBitacora);
		
	}
}
