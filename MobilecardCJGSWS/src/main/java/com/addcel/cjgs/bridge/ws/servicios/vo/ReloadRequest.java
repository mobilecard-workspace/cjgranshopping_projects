package com.addcel.cjgs.bridge.ws.servicios.vo;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
/*
 * This class was automatically generated with 
 * <a href="http://www.castor.org">Castor 1.3.1</a>, using an XML
 * Schema.
 * $Id: ReloadRequest.java,v 1.2 2015/06/25 18:14:49 jlopez Exp $
 */

/**
 * Class ReloadRequest.
 * 
 * @version $Revision: 1.2 $ $Date: 2015/06/25 18:14:49 $
 */
@SuppressWarnings("serial")
@XmlRootElement(name = "ReloadRequest")
public class ReloadRequest implements java.io.Serializable {


      //--------------------------/
     //- Class/Member Variables -/
    //--------------------------/

    /**
     * Field _FOLIO.
     */
    private String _FOLIO;


    /**
     * Field _ID_TERMINAL.
     */
    private String _ID_TERMINAL;


    /**
     * Field _LOCAL_DATE.
     */
    private java.lang.String _LOCAL_DATE;

    /**
     * Field _AMOUNT.
     */
    private double _AMOUNT;

    /**
     * Field _TRX_NO.
     */
    private String _TRX_NO;

    /**
     * Field _NOAUTO.
     */
    private String _NOAUTO;



      //----------------/
     //- Constructors -/
    //----------------/

    public ReloadRequest() {
        super();
    }


      //-----------/
     //- Methods -/
    //-----------/


    /**
     * Returns the value of field 'AMOUNT'.
     * 
     * @return the value of field 'AMOUNT'.
     */
    public double getAMOUNT(
    ) {
        return this._AMOUNT;
    }

    /**
     * Returns the value of field 'FOLIO'.
     * 
     * @return the value of field 'FOLIO'.
     */
    public String getFOLIO(
    ) {
        return this._FOLIO;
    }

    /**
     * Returns the value of field 'ID_TERMINAL'.
     * 
     * @return the value of field 'ID_TERMINAL'.
     */
    public String getID_TERMINAL(
    ) {
        return this._ID_TERMINAL;
    }

    /**
     * Returns the value of field 'LOCAL_DATE'.
     * 
     * @return the value of field 'LOCAL_DATE'.
     */
    public java.lang.String getLOCAL_DATE(
    ) {
        return this._LOCAL_DATE;
    }

    /**
     * Returns the value of field 'TRX_NO'.
     * 
     * @return the value of field 'TRX_NO'.
     */
    public String getTRX_NO(
    ) {
        return this._TRX_NO;
    }
    
    /**
     * Returns the value of field '_NOAUTO'.
     * 
     * @return the value of field '_NOAUTO'.
     */
    public String getNOAUTO() {
		return _NOAUTO;
	}

    /**
     * Sets the value of field 'AMOUNT'.
     * 
     * @param AMOUNT the value of field 'AMOUNT'.
     */
    @XmlElement(name = "AMOUNT")
    public void setAMOUNT(
            double AMOUNT) {
        this._AMOUNT = AMOUNT;
    }

    /**
     * Sets the value of field 'FOLIO'.
     * 
     * @param FOLIO the value of field 'FOLIO'.
     */
    @XmlElement(name = "FOLIO")
    public void setFOLIO(
            String FOLIO) {
        this._FOLIO = FOLIO;
    }

    /**
     * Sets the value of field 'ID_TERMINAL'.
     * 
     * @param ID_TERMINAL the value of field 'ID_TERMINAL'.
     */
    @XmlElement(name = "ID_TERMINAL")
    public void setID_TERMINAL(
            String ID_TERMINAL) {
        this._ID_TERMINAL = ID_TERMINAL;
    }

    /**
     * Sets the value of field 'LOCAL_DATE'.
     * 
     * @param LOCAL_DATE the value of field 'LOCAL_DATE'.
     */
    @XmlElement(name = "LOCAL_DATE")
    public void setLOCAL_DATE(
            java.lang.String LOCAL_DATE) {
        this._LOCAL_DATE = LOCAL_DATE;
    }

    /**
     * Sets the value of field 'TRX_NO'.
     * 
     * @param TRX_NO the value of field 'TRX_NO'.
     */
    @XmlElement(name = "TRX_NO")
    public void setTRX_NO(
            String TRX_NO) {
        this._TRX_NO = TRX_NO;
    }



	/**
     * Sets the value of field '_NOAUTO'.
     * 
     * @param TRX_NO the value of field '_NOAUTO'.
     */
    @XmlElement(name = "_NOAUTO")
	public void setNOAUTO(String _NOAUTO) {
		this._NOAUTO = _NOAUTO;
	}
    
    @Override
    public String toString(){
    	return "_FOLIO: " + _FOLIO + ", _AMOUNT:" + _AMOUNT + ", _ID_TERMINAL:" + _ID_TERMINAL + ", _TRX_NO:" + _TRX_NO + ", _NOAUTO:" + _NOAUTO ;
    }
    
}
