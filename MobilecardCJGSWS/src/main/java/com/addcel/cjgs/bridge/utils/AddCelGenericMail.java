package com.addcel.cjgs.bridge.utils;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.addcel.cjgs.bridge.model.vo.AbstractVO;
import com.addcel.cjgs.bridge.model.vo.CorreoVO;
import com.addcel.cjgs.bridge.model.vo.DetalleVO;
import com.addcel.cjgs.bridge.utils.ibatis.service.AbstractService;
import com.google.gson.Gson;

public class AddCelGenericMail extends AbstractService{
	private static final Logger logger = LoggerFactory.getLogger(AddCelGenericMail.class);
	private Gson gson = new Gson();
	
	private static final String HTML_DOBY_REF = 
			"<!DOCTYPE html> <html> <head> <meta http-equiv=\"content-type\" content=\"text/html; charset=UTF-8\"> <style type=\"text/css\"> * { padding: 0; margin: 0; }  body { font-size: 62.5%; background-color: rgb(255, 255, 255); font-family: verdana, arial, sans-serif; }  a:link { color: #000000; font-weight: bold; }  a:active { color: #000000; font-weight: bold; }  a:visited { color: #000000; font-weight: bold; }  a:hover { color: #FF0000; font-weight: bold; }  .page-container-add { width: 600px; margin: 0px auto; margin-top: 10px; margin-bottom: 10px; border: solid 1px rgb(150, 150, 150); font-size: 1.0em; }  .header-add { width: 600px; height: 82px; font-family: \"trebuchet ms\", arial, sans-serif; overflow: visible !important /*Firefox*/; overflow: hidden /*IE6*/; }  .main-content-add { display: inline; /*Fix IE floating margin bug*/; float: left; width: 540px; margin: 20px 30px 10px 30px; overflow: visible !important /*Firefox*/; overflow: hidden /*IE6*/; }  .main-content-add h1.pagetitle-add { margin: 0 0 0.4em 0; padding: 0 0 2px 0; font-family: Verdana, Geneva, sans-serif; color: #000000; font-weight: bold; font-size: 180%; }  .main-content-add p { margin: 0 0 1.0em 0; line-height: 150%; font-size: 120%; text-align: justify; color: #000000; }  .main-content-add table { clear: both; width: 100%; margin: 0 0 0 0; table-layout: fixed; border-collapse: collapse; empty-cells: show; background-color: rgb(233, 232, 244); text-align: justify; }  .main-content-add table td { height: 2.5em; padding: 2px 5px 2px 5px; border-left: solid 2px rgb(255, 255, 255); border-right: solid 2px rgb(255, 255, 255); border-top: solid 2px rgb(255, 255, 255); border-bottom: solid 2px rgb(255, 255, 255); background-color: rgb(225, 225, 225); font-weight: normal; color: #000000; font-size: 11px; }  .footer-add { clear: both; width: 600px; padding: 5px 0 0 0; background: rgb(225, 225, 225); font-size: 1.0em; overflow: visible !important /*Firefox*/; overflow: hidden /*IE6*/; }  .footer-add p { line-height: 150%; text-align: center; color: rgb(125, 125, 125); font-weight: bold; font-size: 104%; } </style> </head> <body> <div class=\"page-container-add\"> <div class=\"header-add\"> <img src=\"cid:identifierCID00\"> </div> <br> <div class=\"main-content-add\"> <h1 class=\"pagetitle-add\">Estimado(a): @NOMBRE</h1> <p> Se ha realizado su pago de <strong>@EMPRESA</strong> en <strong>PAYNET</strong>, acontinuaci&oacute;n encontrara los datos de la transacci&oacute;n . </p> <p>&nbsp;</p> <p> <font size=+1>Confirmaci&oacute;n de generacion de Referencia. </font> </p> <h3>&nbsp;</h3> <table align=\"center\" cellpadding=\"2\"> <tbody> <tr> <td>Fecha y Hora:</td> <td>@FECHA</td> </tr> <tr> <td>Monto:</td> <td>$ @MONTO MXN</td> </tr> @COMISION <tr> <td>Cargo aplicado:</td> <td>$ @CARGO MXN</td> </tr> <tr> <td>Numero Referencia MC:</td> <td>@REFERENCIAMC</td> </tr> <tr> <td>Numero Referencia @EMPRESA:</td> <td>@REFERENCIA</td> </tr> <tr> <td>Autorizaci&oacute;n:</td> <td>@AUTORIZACION</td> </tr> <tr> <td>Paynet ID Terminal:</td> <td>@IDTERMINAL</td> </tr> <tr> <td>Paynet ID Transaccion:</td> <td>@IDTRANX</td> </tr> </tbody> </table> <p>&nbsp;</p> <p align=center> <font size=-1>¡Gracias por realizar su pago <img src=\"cid:identifierCID01\"></font> </p> <h3>&nbsp;</h3> </div> <div class=\"footer-add\"> <p>Nota. Este correo es de car&aacute;ter informativo, no es necesario que responda al mismo.</p> <br /> <p>MobileCard - ABC Capital &copy; 2015 | Todos los derechos reservados.</p> <p>Powered by Addcel.</p> </div> </div> </body> </html>";
	
	public AbstractVO generatedMail(DetalleVO detalle, String numAutho, String idTerminal, String idTranx){
//		CoreDAO daoCore = null;
		CorreoVO correo = new CorreoVO();
//		SmtpVO smtp = null;
		AbstractVO resp = null;
		try{
//			daoCore = (CoreDAO) getBean("CoreDAO");
//			smtp = daoCore.obtenDatosMail(detalle.getIdCompany());
//			
//			if(smtp == null){
//				resp = new AbstractVO(20, "Does not exist e-mail settings");
//			}else{
				String body = HTML_DOBY_REF;
				body = body.replaceAll("@EMPRESA", "CJ Grand Shopping");
				body = body.replaceAll("@NOMBRE", "Usuario");
				body = body.replaceAll("@FECHA", detalle.getFechaTransaccion());
				body = body.replaceAll("@MONTO", detalle.getTotal() != 0? detalle.getTotal() + "": "0.00");
				body = body.replaceAll("@COMISION", (detalle.getComicion() != 0? 
							"<tr> <td>Comisi&oacute;n:</td> <td>\\$  " + detalle.getComicion() + " MXN</td> </tr>": ""));
				body = body.replaceAll("@CARGO", (detalle.getComicion() + detalle.getTotal()) + "");
				body = body.replaceAll("@REFERENCIAMC", detalle.getIdBitacora() + "");
				body = body.replaceAll("@REFERENCIA", detalle.getNumeroControl());
				body = body.replaceAll("@AUTORIZACION", numAutho);
				body = body.replaceAll("@IDTERMINAL", idTerminal);
				body = body.replaceAll("@IDTRANX", idTranx);
				
				String from = "no-reply@addcel.com";
//				String subject = "Acuse Pago - Referencia MC: " + detalle.getIdBitacora() + " QA ENV";
				String subject = "Acuse Pago - Referencia MC: " + detalle.getIdBitacora();
				String[] to = {detalle.getEmail()};
//				String[] to = {"jesus.lopez@addcel.com"};
				String[] cid = {
						"/usr/java/resources/images/Addcel/MobileCard_header_600.PNG",
						"/usr/java/resources/images/Paynet/paynet_logo_35.png"
						};
		
		//		correo.setAttachments(attachments);
				correo.setBcc(new String[]{"abraham.dominguez@cjgrandshopping.com"});
				correo.setCc(new String[]{});
//				correo.setCc(new String[]{"jesus.lopez@addcel.com","abraham.dominguez@cjgrandshopping.com"});
//				correo.setCc(new String[]{"jesus.lopez@addcel.com","carlos@addcel.com"});
				correo.setCid(cid);
				correo.setBody(body);
				correo.setFrom(from);
				correo.setSubject(subject);
				correo.setTo(to);
				
				sendMail(gson.toJson(correo));
//			}
		}catch(Exception e){
			resp = new AbstractVO(21, "An error has occurred when trying to send mail"); 
			logger.error("Ocurrio un error al enviar el email ", e);
		}
		return resp;
	}
	

	public void sendMail(String data) {
		String line = null;
		StringBuilder sb = new StringBuilder();
		try {
			logger.info("Iniciando proceso de envio email. ");
			//logger.debug("Mail Data: {}", data);

			URL url = new URL(ConstantesCJGS.URL_MAIL_SENDER);
			logger.info("Conectando con " + ConstantesCJGS.URL_MAIL_SENDER);
			HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

			urlConnection.setDoOutput(true);
			urlConnection.setRequestProperty("Content-Type", "application/json");
			urlConnection.setRequestProperty("Accept", "application/json");
			urlConnection.setRequestMethod("POST");

			OutputStreamWriter writter = new OutputStreamWriter(urlConnection.getOutputStream());
			writter.write(data);
			writter.flush();

//			logger.info("Datos enviados, esperando respuesta");

			BufferedReader reader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));

			while ((line = reader.readLine()) != null) {
				sb.append(line);
			}

//			logger.info("Respuesta del servidor: " + sb.toString());
		} catch (Exception ex) {
			logger.error("Error en: sendMail, al enviar el email ", ex);
		}
	}
}
