package com.addcel.cjgs.bridge.model.vo.prosa;


public class ProsaDetalleVO {
	private int idBitacora;
	private int idCompany;
	private int status;
	private String numAutorizacion;
	private int idTransaction;
	private String numAutorizacionCancel;
	private int idTransactionCancel;
	
	private String company;
	private String cardNumber;
	private String total;
	private String fecha;
	private String moneda;
	
	public int getIdBitacora() {
		return idBitacora;
	}
	public void setIdBitacora(int idBitacora) {
		this.idBitacora = idBitacora;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getNumAutorizacion() {
		return numAutorizacion;
	}
	public void setNumAutorizacion(String numAutorizacion) {
		this.numAutorizacion = numAutorizacion;
	}
	public int getIdTransaction() {
		return idTransaction;
	}
	public void setIdTransaction(int idTransaction) {
		this.idTransaction = idTransaction;
	}
	public String getNumAutorizacionCancel() {
		return numAutorizacionCancel;
	}
	public void setNumAutorizacionCancel(String numAutorizacionCancel) {
		this.numAutorizacionCancel = numAutorizacionCancel;
	}
	public int getIdTransactionCancel() {
		return idTransactionCancel;
	}
	public void setIdTransactionCancel(int idTransactionCancel) {
		this.idTransactionCancel = idTransactionCancel;
	}
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	public String getCardNumber() {
		return cardNumber;
	}
	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}
	public String getTotal() {
		return total;
	}
	public void setTotal(String total) {
		this.total = total;
	}
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	public int getIdCompany() {
		return idCompany;
	}
	public void setIdCompany(int idCompany) {
		this.idCompany = idCompany;
	}
	public String getMoneda() {
		return moneda;
	}
	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}
	
		
}
