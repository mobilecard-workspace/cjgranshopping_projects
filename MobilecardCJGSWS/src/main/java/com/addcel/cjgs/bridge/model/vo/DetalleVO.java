package com.addcel.cjgs.bridge.model.vo;

public class DetalleVO {
	private long idBitacora;
	private String idUsuario;
	private String numeroControl;
	private String nombre;
	private int canal;
	private int modoPago;
	private String email;
	private String descripcion;
	private String tipoTarjeta;
	private String conMSI;
	private int plazo;
	private double total;
	private double comicion;
	private String referencia;
	private String referenciaPaynet;
	private String vigencia;
	private String digitover;
	private int status;
	
	private String fechaTransaccion;
	
	public long getIdBitacora() {
		return idBitacora;
	}
	public void setIdBitacora(long idBitacora) {
		this.idBitacora = idBitacora;
	}
	public String getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(String idUsuario) {
		this.idUsuario = idUsuario;
	}
	public String getNumeroControl() {
		return numeroControl;
	}
	public void setNumeroControl(String numeroControl) {
		this.numeroControl = numeroControl;
	}
	public int getCanal() {
		return canal;
	}
	public void setCanal(int canal) {
		this.canal = canal;
	}
	public int getModoPago() {
		return modoPago;
	}
	public void setModoPago(int modoPago) {
		this.modoPago = modoPago;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getTipoTarjeta() {
		return tipoTarjeta;
	}
	public void setTipoTarjeta(String tipoTarjeta) {
		this.tipoTarjeta = tipoTarjeta;
	}
	public String getConMSI() {
		return conMSI;
	}
	public void setConMSI(String conMSI) {
		this.conMSI = conMSI;
	}
	public int getPlazo() {
		return plazo;
	}
	public void setPlazo(int plazo) {
		this.plazo = plazo;
	}
	public double getTotal() {
		return total;
	}
	public void setTotal(double total) {
		this.total = total;
	}
	public double getComicion() {
		return comicion;
	}
	public void setComicion(double comicion) {
		this.comicion = comicion;
	}
	public String getReferencia() {
		return referencia;
	}
	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}
	public String getReferenciaPaynet() {
		return referenciaPaynet;
	}
	public void setReferenciaPaynet(String referenciaPaynet) {
		this.referenciaPaynet = referenciaPaynet;
	}
	public String getVigencia() {
		return vigencia;
	}
	public void setVigencia(String vigencia) {
		this.vigencia = vigencia;
	}
	public String getDigitover() {
		return digitover;
	}
	public void setDigitover(String digitover) {
		this.digitover = digitover;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getFechaTransaccion() {
		return fechaTransaccion;
	}
	public void setFechaTransaccion(String fechaTransaccion) {
		this.fechaTransaccion = fechaTransaccion;
	}

		
}
