package com.addcel.cjgs.bridge.ws.servicios;

import com.addcel.cjgs.bridge.services.PagoExpressService;

public class NotificaPagoExpressWS {
//	private static final Logger logger = LoggerFactory.getLogger(NotificaPagoExpressWS.class);

	public String reload(String sXML){
		return new PagoExpressService().reloadProcesses(sXML);
	}
	
	public String reverseReload(String sXML){
		return new PagoExpressService().reverseReload(sXML);
	}

}
