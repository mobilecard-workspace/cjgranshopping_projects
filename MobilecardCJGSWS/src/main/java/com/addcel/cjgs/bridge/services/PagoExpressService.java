package com.addcel.cjgs.bridge.services;

import java.io.StringReader;
import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.addcel.cjgs.bridge.model.dao.CoreDAO;
import com.addcel.cjgs.bridge.model.dao.PaymentDAO;
import com.addcel.cjgs.bridge.model.vo.DetalleVO;
import com.addcel.cjgs.bridge.model.vo.pagos.TBitacoraVO;
import com.addcel.cjgs.bridge.utils.AddCelGenericMail;
import com.addcel.cjgs.bridge.utils.ibatis.service.AbstractService;
import com.addcel.cjgs.bridge.ws.servicios.vo.ReloadRequest;
import com.addcel.cjgs.bridge.ws.servicios.vo.ReloadResponse;
import com.addcel.utils.AddcelCrypto;
import com.addcel.utils.Utilerias;

@Service
public class PagoExpressService extends AbstractService{
	private static final Logger logger = LoggerFactory.getLogger(PagoExpressService.class);
		
//	private static final String patronCom = "yyyy-MM-dd";
//	private static final SimpleDateFormat formatoCom = new SimpleDateFormat(patronCom);

	
	public String reloadProcesses(String sXML){
		ReloadResponse response = new ReloadResponse();
		ReloadRequest request  = null;
		DetalleVO detalle = null;
		String resp = null;
		String clave = null;
		String numAutho = null;
		
		try {
			
			if(sXML != null && !"".equals(sXML.trim())){
				request = unmarshalRequest(sXML);
				
				if(request != null){
					detalle = getDetalleVO(request);
					
					if(detalle == null){
						response.setRESPONSECODE(100);
						response.setDESCRIPTIONCODE("No existe la transaccion.");
					}else if (detalle.getStatus() == 1){
						response.setRESPONSECODE(101);
						response.setDESCRIPTIONCODE("Referencia ya pagada.");
					}else if (detalle.getStatus() == 3){
						response.setRESPONSECODE(98);
						response.setDESCRIPTIONCODE("Reversa Duplicada");
					}else if((detalle.getTotal() + detalle.getComicion()) != request.getAMOUNT()) {
						response.setRESPONSECODE(88);
						response.setDESCRIPTIONCODE("Monto Invalido");
					}else{
						clave = getClaveTripleDesc();
						if(clave == null){
							response.setRESPONSECODE(93);
							response.setDESCRIPTIONCODE("Adquiriente Inválido");
						}else{
							numAutho = generaNumAutho();
							response.setRESPONSECODE(0);
							response.setDESCRIPTIONCODE("Success");
							response.setNOAUTO(numAutho);
							
//							respuestaCJGS = new UtilsService().notificacionURLComercio(ConstantesCJGS.URL_NOTIFICA_CJGS, "?param=" + notificacionComercio(request, response, detalle, clave));
							//validar respuesta de CJGS
							detalle.setFechaTransaccion(Utilerias.getFullFechaActual());
							
							new AddCelGenericMail().generatedMail(detalle, numAutho, request.getID_TERMINAL(), request.getTRX_NO());
							
							updateBitacoras(request, response, detalle, 1, numAutho);
						}
					}
				}else{
					response.setRESPONSECODE(96);
					response.setDESCRIPTIONCODE("Error de sistema");
				}
			}else{
				response.setRESPONSECODE(96);
				response.setDESCRIPTIONCODE("Error de sistema");
			}
		}catch(Exception e){
			logger.error("An error occurred general RELOAD: {}", e);
			response.setRESPONSECODE(96);
			response.setDESCRIPTIONCODE("Error de sistema");
		}finally{
			response.setLOCAL_DATE(Utilerias.getFullFechaActual());
			response.setAMOUNT(request.getAMOUNT());
			response.setID_TERMINAL(request.getID_TERMINAL());
			response.setTRX_NO(request.getTRX_NO());
			if(response.getNOAUTO() == null){
				response.setNOAUTO("");
			}
			resp = marshalResponse(response);
		}
		
		logger.debug("Respuesta: " + resp);
		return resp;
	}
	
	public String reverseReload(String sXML){
		ReloadResponse response = new ReloadResponse();
		ReloadRequest request  = null;
		DetalleVO detalle = null;
		String resp = null;
		String clave = null;
		
		try {
			
			if(sXML != null && !"".equals(sXML.trim())){
				request = unmarshalRequest(sXML);
				
				if(request != null){
					detalle = getDetalleVO(request);
					
					if(detalle == null){
						response.setRESPONSECODE(100);
						response.setDESCRIPTIONCODE("No existe Original.");
//					}else if (detalle.getStatus() == 1){
//						response.setRESPONSECODE(101);
//						response.setDESCRIPTIONCODE("Referencia ya pagada.");
					}else if (detalle.getStatus() == 3){
						response.setRESPONSECODE(98);
						response.setDESCRIPTIONCODE("Reversa Duplicada");
					}else if((detalle.getTotal() + detalle.getComicion()) != request.getAMOUNT()) {
						response.setRESPONSECODE(88);
						response.setDESCRIPTIONCODE("Monto Invalido");
					}else{
						clave = getClaveTripleDesc();
						if(clave == null){
							response.setRESPONSECODE(93);
							response.setDESCRIPTIONCODE("Adquiriente Inválido");
						}else{
							response.setRESPONSECODE(0);
							response.setDESCRIPTIONCODE("Success");
							response.setNOAUTO(detalle.getIdBitacora() + "");
							
//							respuestaCJGS = new UtilsService().notificacionURLComercio(ConstantesCJGS.URL_NOTIFICA_CJGS, "?param=" + notificacionComercio(request, response, detalle, clave));
							//validar respuesta de CJGS
							
							updateBitacoras(request, response, detalle, -1, null);
						}
					}
				}else{
					response.setRESPONSECODE(96);
					response.setDESCRIPTIONCODE("Error de sistema");
				}
			}else{
				response.setRESPONSECODE(96);
				response.setDESCRIPTIONCODE("Error de sistema");
			}
		}catch(Exception e){
			response.setRESPONSECODE(96);
			response.setDESCRIPTIONCODE("Error de sistema");
			logger.error("An error occurred general RELOAD: {}", e);
		}finally{
			response.setLOCAL_DATE(Utilerias.getFullFechaActual());
			if(request != null){
				response.setAMOUNT(request.getAMOUNT());
				response.setID_TERMINAL(request.getID_TERMINAL());
				response.setTRX_NO(request.getTRX_NO());
			}
			
			if(response.getNOAUTO() == null){
				response.setNOAUTO("");
			}
			resp = marshalResponse(response);
		}
		
		logger.debug("Respuesta: " + resp);
		return resp;
	}
	
	private String notificacionComercio(ReloadRequest request, ReloadResponse response, DetalleVO detalle, String clave){
		String notifiacionJson = null;
		StringBuilder data = new StringBuilder();
		try{
			
			data.append("{\"idError\":").append(response.getRESPONSECODE())
			.append(",\"mensajeError\":\"").append(response.getDESCRIPTIONCODE())
			.append("\",\"numeroControl\":\"").append(detalle.getNumeroControl())
			.append("\",\"fechaTransaccion\":\"").append(request.getLOCAL_DATE())
			.append("\",\"autorizacion\":\"").append("")
			.append("\",\"transaccionMC\":").append(detalle.getIdBitacora())
			.append(",\"referencia\":\"").append(detalle.getReferenciaPaynet())
			.append("\",\"tarjeta\":\"").append("")
			.append("\",\"plazo\":").append("1")
			.append(",\"pago\":").append(true)
			.append(",\"canal\":").append(detalle.getCanal())
			.append(",\"modoPago\":").append(detalle.getModoPago())
			.append("}");
			
			notifiacionJson = AddcelCrypto.encryptTripleDes(clave, data.toString());
		}catch(Exception e){
			logger.error("Error al genaral al notificar al Comercio: {}", e);
		}
		return notifiacionJson;
	}
	
	private String getClaveTripleDesc(){
		String clave = null;
		CoreDAO dao = null;
		try{
			dao = (CoreDAO) getBean("CoreDAO");
			clave = dao.getTProveedor();
			
		}catch(Exception e){
			logger.error("Error al genaral al notificar al Comercio: {}", e);
		}
		return clave;
	}

	
	private DetalleVO getDetalleVO(ReloadRequest request){		
		DetalleVO detalle = null;
		PaymentDAO dao = null;
		try{
			dao = (PaymentDAO) getBean("PaymentDAO");
			logger.debug("Busqueda del registro: ");
			logger.debug("Request: " + request.toString());
			detalle = dao.selectCJGSDetalle(request);
			
			if(detalle != null){
				logger.debug("ReferenciaPaynet: " + detalle.getReferenciaPaynet());
				logger.debug("Total: " + detalle.getTotal());
				logger.debug("NumeroControl: " + detalle.getNumeroControl());
			}
			
		}catch(Exception e){
			logger.error("Error general al buscar en CJGS_detalle: {}", e);
		}
		
		return detalle;
	}
	
	private void updateBitacoras(ReloadRequest request, ReloadResponse response, DetalleVO detalle, int status, String numAutho){
		
		try{
			PaymentDAO dao = (PaymentDAO) getBean("PaymentDAO");
			
			TBitacoraVO tbitacora = new TBitacoraVO();
			tbitacora.setIdBitacora(detalle.getIdBitacora());
			tbitacora.setBitStatus(status);
			if(status == 0){
				tbitacora.setBitConcepto("ERROR PAGO ID_TERMINAL: " + request.getID_TERMINAL()  + ", TRX_NO: " + request.getID_TERMINAL());
			}else if(status == 1){
				tbitacora.setBitConcepto("PAGO EXITO ID_TERMINAL: " + request.getID_TERMINAL()  + ", TRX_NO: " + request.getID_TERMINAL());
			}else if(status == 3){
				tbitacora.setBitConcepto("PAGO CENCELADO ID_TERMINAL: " + request.getID_TERMINAL()  + ", TRX_NO: " + request.getID_TERMINAL() );
			}
			
//			tbitacora.setBitNoAutorizacion(request.getID_TERMINAL()+"");
			
			logger.debug("Actualizando t_bitacora...");
			dao.updateTBitacora(tbitacora);
			
			logger.debug("Actualizando GPO_RICHARD_detalle...");
			dao.updateCJGSDetalle(detalle.getIdBitacora(), status, numAutho);
		}catch(Exception e){
			logger.error("Error general al actualizar en bitacoras: {}", e);
		}
	}
	
	public ReloadRequest unmarshalRequest(String sXML){
		StreamSource data = null;
		JAXBContext jaxbContext = null;
		Unmarshaller jaxbUnmarshaller = null;
		ReloadRequest request  = null;
		
		try {
			sXML = sXML.trim();
			logger.debug("Cadena XML: " + sXML);
			
			if(sXML != null){
				data = new StreamSource(new StringReader(sXML));
				jaxbContext = JAXBContext.newInstance(ReloadRequest.class);
				jaxbUnmarshaller = jaxbContext.createUnmarshaller();
				
				request = jaxbUnmarshaller.unmarshal(data, ReloadRequest.class).getValue();
				
				logger.debug("FOLIO: " + request.getFOLIO());
				logger.debug("AMOUNT: " + request.getAMOUNT());
				logger.debug("ID_TERMINAL: " + request.getID_TERMINAL());
				logger.debug("TRX_NO: " + request.getTRX_NO());
				
			}
		}catch(Exception e){
			logger.error("An error occurred general durante el pago: {}", e);
		}finally{
		}
		return request;
	}
	
	public String marshalResponse(ReloadResponse response){
		StringWriter sw = new StringWriter();
		JAXBContext jaxbContext = null;
		Marshaller jaxbMarshaller = null;
		try {
			if(response != null){
				jaxbContext = JAXBContext.newInstance(ReloadResponse.class);
				jaxbMarshaller = jaxbContext.createMarshaller();
				jaxbMarshaller.setProperty( Marshaller.JAXB_FORMATTED_OUTPUT, true );
				
				jaxbMarshaller.marshal(response, sw);
			}
		}catch(Exception e){
			logger.error("An error occurred general durante el pago: {}", e);
		}finally{
		}
		return sw != null? sw.toString(): null;
	}
	
	public String generaNumAutho(){
		String numAutho = null;
		try{
			numAutho = "00000" + ((long) (Math.random() * 999999));
		}catch(Exception e){
			numAutho = "000000";
		}
		
		return numAutho != null? numAutho.substring(numAutho.length() - 6):"00000";
	}
}

